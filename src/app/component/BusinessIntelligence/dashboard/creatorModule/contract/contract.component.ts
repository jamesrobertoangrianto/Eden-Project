import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faExpandAlt, faDownload } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.scss']
})
export class ContractComponent implements OnInit {
  faChevronDown = faChevronDown
  faDownload=faDownload
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  faExpandAlt=faExpandAlt
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];
  contract: any;

  params: string;
  sortListItems: { name: string; items: any; }[];
  contract_status: { id: string; label: string; value: string; name: string; }[];
  selected_item: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    private serializer: UrlSerializer,

  ) { }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(queryParams => {
     
      this.params = this.serializer.serialize(this.router.createUrlTree([''],
      { queryParams: this.route.snapshot.queryParams}))      
      this.getContract()

    })

    this.contract_status = [
      {
        'id' : 'status',
        'label' : 'draft',
        'value' : 'draft',
        'name' : 'draft',
      
      },
      {
        'id' : 'status',
        'label' : 'Not Signed',
        'value' : 'not-signed',
        'name' : 'not-signed',
      },
      {
        'id' : 'status',
        'label' : 'signed',
        'value' : 'signed',
        'name' : 'signed',
      },
      {
        'id' : 'status',
        'label' : 'terminated',
        'value' : 'terminated',
        'name' : 'terminated',

      }
    ]


    this.sortListItems = [
      
      {
       
        'name' : 'status',
        'items' : this.contract_status

      },
        
    ]



  }




  async getContract(){
    
    
    try {
    
      let response = await this.webService.getContract(this.params)
      this.contract = response.data

     
    console.log(this.contract)
     
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
  
    }
  }

  download(link){
    window.open(link, '_blank');
  }

  async onChange(event: any,id) {
    const file: File = event.target.files[0];

    if (file) {
     

      const formData = new FormData();
  
      formData.append('file', file);

     
      
      try {
        this.appService.showLoading()
        let res = await this.webService.uploadPhoto(formData)
        console.log(res)
        if(res.data){
          let form = {
            'id' : 'legal_doc_url',
            'value' : res.data
          }

        
          this.updateContract(form,id)

        }
        
      } catch (error) {
        this.appService.openToast(error)
      }
      finally{
       
        this.appService.hideLoading()
      
      }

    }
  }

  getPlaceholderName(item) {
    if(item.first_name && item.last_name){
      var first_name =item.first_name.charAt(0);
      var last_name =item.last_name.charAt(0);
      return first_name + last_name
    }
  
  }

  toggleShow(item){
    this.selected_item = item
    this.openModal('contractModal')
  }
  async updateContract(item,id){
    console.log(id)
    let form = {}
    form[item.id] = item.value
    try {
    
    let response = await this.webService.updateContract(form,id)
      if(response.data){
        this.appService.openToast('Updated!')
      }
     
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
   
      this.ngOnInit()
    }
  }
  toJson(item){
    return JSON.parse(item)
  }

  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }

  toggleColumnsMenu(){
    this.show_columns = !this.show_columns
  }

  _navigateTo(page) {
    this.router.navigate(
      ['/creator/contract/view/'+page]
    );
  }

  changeColumnsView(item) {
    this.router.navigate(
      [],
      { queryParams: { columns_view: item.id},
      queryParamsHandling: 'merge' }
    );
    this.selected_columns = item
    this.toggleColumnsMenu()
   
  }
  
}
