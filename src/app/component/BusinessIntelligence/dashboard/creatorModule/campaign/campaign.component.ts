import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faCalendarDay, faChevronCircleRight, faChevronDown, faChevronRight, faDotCircle, faEllipsisH, faEnvelope, faFileAlt, faFileContract, faMapPin, faPhone } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.scss']
})
export class CampaignComponent implements OnInit {
  faEllipsisH = faEllipsisH
  faChevronCircleRight= faChevronRight
 

  faPhone=faPhone
  faEnvelope= faEnvelope
  faMapPin =faMapPin
  faChevronCircleDown = faChevronDown
  faDotCircle=faDotCircle
  faFileContract = faFileContract
  faCalendarTimes = faCalendarDay
  faFileAlt =faFileAlt
  showModal: any;

  createCampaignForm = new FormGroup({
    campaign_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    start_date: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    end_date: new FormControl(null),
    status: new FormControl(0),


    

    budget: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    creator_count: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    owner_id: new FormControl(null),

    
    
    
    
  })
  showClientMenu: boolean;
  searchClientKey: any;
  show_location_menu: boolean;
  search_location_key: any;
  mini_menu: string;
  search_client_key: any;
  search_key: any;
  client_filter_list :any;
  client_list: string;
  modal_sheets: any;
  selected_client: any;
  is_end_date: boolean;
  tab_menu_list: { id: string; label: string; count: number; class: any; }[];
  tier = [
    {
      'id' : 'Nano',
      'value' : '( 1000 - 9999 )'
    },
    {
      'id' : 'Micro',
      'value' : '( 10000 - 99999 )'
    },
    {
      'id' : 'Macro',
      'value' : '( 100000 - 999999 )'
    },
    {
      'id' : 'Mega',
      'value' : '( >= 1000000 )'
    }

  ]

  campaign: any;
  overview: { label: string; value: string; }[];
  creator: any;
  params: string;
  sortListItems: { name: string; items: any; }[];
  total_invited: any;
  page: any;
  
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    private serializer: UrlSerializer,
  ) { }

  ngOnInit(): void {
    this.is_end_date =false
  // this.playSound('https://assets.mixkit.co/active_storage/sfx/2870/2870-preview.mp3')
   this.getClient()
    this.route.queryParamMap.subscribe(queryParams => {
     
      this.params = this.serializer.serialize(this.router.createUrlTree([''],
      { queryParams: this.route.snapshot.queryParams}))      
      this.getCampaign()
    })

    this.sortListItems = []
   
    this.setComponentData()
    
  }

      
    
  async getClient(){
    this.client_filter_list = []
   
    
    try {
      this.appService.showLoading()
      let response = await this.webService.getClient('brand')
 

      this.client_filter_list = response.data.map(item => {
        return {
          id: item.id,
          label: item.client_name,
          name: item.client_name
        };
      });

      this.sortListItems.push(
        {
       
          'name' : 'client',
          'items' : this.client_filter_list
        },
      )


    console.log(response)
    


    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
      this.appService.hideLoading()
    }
  }


  async getCampaign(){
    this.page = 1
    
    try {
      this.appService.showLoading()
      let response = await this.webService.getCampaign(this.params)
    console.log(response)
      this.campaign = response.data
    
      
  
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
      this.appService.hideLoading()
    }
  }

  async loadMore() {
    this.page = this.page+1
    this.params = this.params+'&page='+this.page

    
    try {
      this.appService.showLoading()
      let response = await this.webService.getCampaign(this.params)
      if(response.data){
      

      this.campaign = this.campaign.concat(response.data)
      
    }
    
      
  
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
      this.appService.hideLoading()
    }
  }


  getPlaceholderName(item) {
    if(item.first_name && item.last_name){
      var first_name =item.first_name.charAt(0);
      var last_name =item.last_name.charAt(0);
      return first_name + last_name
    }
  
  }



   playSound(url) {
    const audio = new Audio(url);
    audio.play();
  }


  setComponentData(){
    this.tab_menu_list = [
      {
        'id' : 'active_campaign',
        'label' : 'Active Campaign',
        'count' : 10,
        'class' : null,
      },
      {
        'id' : 'in_active',
        'label' : 'In Active',
        'count' : 10,
        'class' : null,
      }
     
    ]
    
  }





  
  
  _navigateTo(page) {
    this.router.navigate(
      ['/creator/campaign/view/'+page]
    );
  }
  toggleEndDate(){
    this.is_end_date  = !this.is_end_date
  }
  async addCampaign(){
    this.createCampaignForm.get('owner_id').setValue(this.webService.account_id)
    if(this.createCampaignForm.valid){
      try {
        this.appService.showLoading()
          let response = await this.webService.addCampaign(this.createCampaignForm.value)
          
         this._navigateTo(response.data.id)
  
        } catch (e) {
          
          this.appService.openToast(e)
          
          console.log(e)
        } finally {
         
          this.appService.hideLoading()
          this.closeModal()
          this.appService.openToast('New Campaign added!')
        
          
        }
    }
   

  }

  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }

  searchClient(){
   
    if(this.search_key){
      this.client_list  = 'true'
    }
  }

  toggleSheets(menu?){
    if(!this.modal_sheets){
      this.modal_sheets = menu
    }else{
      this.modal_sheets = null
    }
   
  }

  searchLocation(){
   
  
  }

  selectClient(val){
    this.toggleSheets()
    this.selected_client = val
  }

  selectService(val){
  
  }
}
