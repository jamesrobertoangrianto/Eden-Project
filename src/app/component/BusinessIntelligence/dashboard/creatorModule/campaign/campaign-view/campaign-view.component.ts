import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faCalendarAlt, faChevronDown, faChevronLeft, faChevronRight, faMapMarker, faTrash, faTrashAlt, faUserPlus } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-campaign-view',
  templateUrl: './campaign-view.component.html',
  styleUrls: ['./campaign-view.component.scss']
})
export class CampaignViewComponent implements OnInit {
  faChevronDown = faChevronDown
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus 
  faTrashAlt=faTrashAlt
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  showModal: any;
  tab_view: string;
  right_tab_view: string;
  insight: { title: string; Description: string; link: string; fn: string; icon: string; }[];
  creator: any;
  creator_id: string;
  isEdit: string;
  isRateEdit: boolean;
  
  creator_rate = {
    'platform_name' : null,
    'rate_name' : null,
    'rate_price' : null,
    'creator_id' : null,
  }

  overview: { label: string; value: string; }[];
  social_metric: { platform_name: string; metric_name: string; metric_value: string; creator_id: string; }[];
  performance_form: { performance_name: any; performance_score: any; creator_id: any; }[];
  campaign_id: string;
  campaign: any;
  invited_creator: any;
  selected_creator: any;
  contract: any
  selected_contract: any;


  option = [
    {
      'id':'id',
      'value':'value',
      'label':'label'
    },
    {
      'id':'id',
      'value':'value',
      'label':'label'
    }
  ]
  client_list: any[];
  tab_menu_list: { id: string; label: string; }[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

  ngOnInit(): void {
    
    this.route.paramMap.subscribe(params=>{
      this.campaign_id = params.get("id")
      this.getCampaignById(this.campaign_id)
    })

    this.setComponentData()
  
  
  
  }

  async deleteInvitedCreator(item){
  
    try {
    
      let response = await this.webService.deleteInvitedCreator(item.id)
      console.log(response)
      
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      let index = this.campaign.invited_creator.indexOf(item)
      this.campaign.invited_creator.splice(index, 1)
    
    }
  }


  download(link){
    window.open(link, '_blank');
  }
  inviteCreator(){
   
    this.openModal('addCreatorModal')
  }
  

  _getPerformance(creator){
    let total  = 0
      creator.performance.forEach(item => {
          total += item.performance_score / 3
      });

  
      if( total > 67 && total < 100){
        return 'best'
      }
      else if( total > 37 && total < 67){
        return 'good'
      }
      else if( total >0 && total< 37){
        return 'bad' 
      }
      else{
        return 'not set'
      }
     
      
    
  }

  
  updateSocialMetric(item){

  }

  
  async searchClient(e){
    if(e.length>1){
      let a  = '?search='+e
      this.client_list = []
      try {
        let response = await this.webService.getClient('brand',a)
  
        this.client_list = response.data.map(item => {
          return {
            id: 'client_id',
            label: item.client_name,
            value: item.id
          };
        });
  
  
      
      } catch (error) {
        
      }
    }
   
  }




  selectClient($event){
  console.log($event)
  
  }
  setComponentData(){
    this.tab_menu_list = [
      {
        'id' : 'instagram',
        'label' : 'Instagram',
       
       
      },
      {
        'id' : 'assigned_task',
        'label' : 'Tiktok',
       
       
      },
    
    ]

    

  }



  

    
  async getCampaignById(id){
    
    
    try {
    
      let response = await this.webService.getCampaignById(id)
    
      this.campaign = response.data
    
      this.setOverview()
      this.campaign.invited_creator.forEach(item => {
        item.performance = this._getPerformance(item.creator)
        item.persona = JSON.parse(item.creator.persona)
       //console.log(JSON.parse(item.creator.persona))
     
      });

      console.log(this.campaign)
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {

    }
  }

  

  async getCreator(e){
    
  let a  = '?search='+e
    try {
    
      let response = await this.webService.getCreator(a)
    
      this.creator = response.data
      this.creator.forEach(item => {
    
        item.isAdded = this.isCreatorAdded(item.id)
      });
     
   
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
  
    }
  }

  isCreatorAdded(id){

    if(this.campaign.invited_creator.some(creator => creator.creator_id === id)){
    return true
    } else{
       return false
    }
   
  }
  createContract(item){
    if(this.campaign.client_id){
      this.selected_creator = item

        this.contract = {
        
          'campaign_id' : this.selected_creator.campaign_id,
          'creator_id' : this.selected_creator.creator.id,
          'client_id' : this.campaign.client_id,
          'status' : 'draft',
          'budget' : '',
          'gross_up' : '',
          'term_of_payment' : '',
          'term_of_payment_1' : '',
          'scope' : [
            {
              'platform_name' : null,
              'creator_campaign_id' : this.selected_creator.campaign_id,
              'type' : 'campaign',
              'name' : null
            }
          ]
        }

        console.log(this.contract)

        this.openModal('addContractModal')
    }
    else{
      this.appService.openToast('Please add client to this campaign to continue')
    }
    
  }

  addMoreScope(){
    this.contract.scope.push( {
      'platform_name' : null,
      'creator_campaign_id' : this.selected_creator.campaign_id,
      'type' : 'campaign',
      'name' : null
    })
  }

  async addContract(){
  console.log(this.contract)
    if(this.selected_creator){

      try {
        this.appService.showLoading()
        let response = await this.webService.addContract(this.contract,this.selected_creator.id)
     
        this.selected_creator.contract_id = response.data.id
  
        this.appService.openToast('Updated!')
      
      } catch (e) {
        
        this.appService.openToast(e)
        
        console.log(e)
      } finally {
        
        this.contract = null
        
        this.appService.hideLoading()
        this.closeModal()
      }
    }

  


  }
    setOverview(){
      this.overview = [
        {
          'label' : 'Date',
          'value' : this.campaign.start_date
        },
        {
          'label' : 'Budget',
          'value' : this.campaign.budget
        },
        {
          'label' : 'Creator Need',
          'value' : this.campaign.creator_count
        },
       
      ]
    }

    

  toggleRateEdit(item){
  item.edit = !item.edit
  }


  viewContract(item){
    this.selected_contract = item.contracts
    console.log(this.selected_contract)
    this.openModal('contractModal')
  }

  
  async updateStatus(item){
   
    let form = {}
    form['status'] = !item.status
      console.log(form)
      if(this.campaign.id){
        try {
      
          let response = await this.webService.updateCampaign(form,this.campaign.id)
          
        console.log(response)
          this.appService.openToast('Updated!')
        
        } catch (e) {
          
          this.appService.openToast(e)
          
          console.log(e)
        } finally {
        
      
        }
      }
  }


  async addToCampaign(item){
    
    let form = {
      'creator_id' : item.id,
      'campaign_id' : this.campaign.id
    }

      if(this.campaign.id){
        try {
      
          let response = await this.webService.addInvitedCreator(form)
          let a = response.data
          a.creator = item
          item.isAdded = true
         this.campaign.invited_creator.unshift(a)


          this.appService.openToast('Updated!')
        
        } catch (e) {
          
          this.appService.openToast(e)
          
          console.log(e)
        } finally {
        
      
        }
      }
  }

  async updateCampaign(data){
    
    let form = {}
    form[data.id] = data.value
      console.log(form)
      if(this.campaign.id){
        try {
      
          let response = await this.webService.updateCampaign(form,this.campaign.id)
          if(data.id == 'client_id'){
            this.campaign.client_id = data.value
          }
   
          this.appService.openToast('Updated!')
        
        } catch (e) {
          
          this.appService.openToast(e)
          
          console.log(e)
        } finally {
        
      
        }
      }
  
    }

  async deleteCreatorRate(item){
  // console.log(item)
  
    try {
    
      let response = await this.webService.deleteCreatorRate(item.id)
      console.log(response)
      this.appService.openToast('Ratecard Removed!')
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      let index = this.creator.rate.indexOf(item)
      this.creator.rate.splice(index, 1)
    
    }
  }


  createNewRate(){
    let rate = {
      "creator_id" : this.creator.id,
      "platform_name": null,
      "platform_id": null,
      "rate_name": null,
      "rate_price": null,
      "rate_price_per": null,
      "isNew" : true,
      "edit": true
  }
    this.creator.rate.unshift(rate)
  }



  getPlaceholderName(item) {
    if(item.first_name && item.last_name){
      var first_name =item.first_name.charAt(0);
      var last_name =item.last_name.charAt(0);
      return first_name + last_name
    }
  
  }



  async addNewRateCart(){


    
    try {
      this.creator_rate.creator_id = this.creator.id
      let response = await this.webService.addCreatorRate(this.creator_rate,this.creator.id)
      console.log(response)
      response.data.new = true
      this.appService.openToast('New Rate Added')
      this.creator.rate.unshift(response.data)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.creator_rate = null
      this.closeModal()
    }
  }

  searchCreator(e){
    console.log(e)
  }


  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }

  _navigateToTabView(tab_view) {
    this.router.navigate(
      [],
      { queryParams: { tab_view: tab_view},
      queryParamsHandling: 'merge' }
    );
  }
  _navigateToRightTabView(tab_view) {
    this.router.navigate(
      [],
      { queryParams: { right_tab_view: tab_view},
      queryParamsHandling: 'merge' }
    );
  }
}
