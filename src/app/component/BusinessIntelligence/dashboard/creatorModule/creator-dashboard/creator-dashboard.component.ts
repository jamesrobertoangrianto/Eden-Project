import { Component, OnInit } from '@angular/core';
import { faProjectDiagram, faUsers, faVideo } from '@fortawesome/free-solid-svg-icons';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-creator-dashboard',
  templateUrl: './creator-dashboard.component.html',
  styleUrls: ['./creator-dashboard.component.scss']
})
export class CreatorDashboardComponent implements OnInit {
  total_creator: any;
  total_management: any;
  total_campaign: any;
  creator_tier: any;
  creator_location: any;
  creator_campaign: any;
  creator_age: any;

  faUsers=faUsers
  faProjectDiagram=faVideo
  creator_management: any;
  creator_persona: any;

  constructor(
    private webService : ManualOrderService,
  ) { }

  ngOnInit(): void {
    this.getTotalCreator()
    this.getTotalManagement()
    this.getTotalCampaign()
    this.getCreatorTier()
    this.getCreatorLocation()
    this.getCreatorCampaign()
    this.getCreatorDemographic()
    this.getManagement()
    this.getCreatorPersona()
  }

  _getPercentage(total,count){
    return Math.round((count/total)*100)
  }



  getPlaceholderName(item) {
    if(item.first_name && item.last_name){
      var first_name =item.first_name.charAt(0);
      var last_name =item.last_name.charAt(0);
      return first_name + last_name
    }
  
  }

  async getCreatorTier(){
    
    
    try {
    
      let response = await this.webService.getCreatorDashboard('creator_tier')
      if(response.data){
     
        this.creator_tier = response.data
       
       

      }
   

    

    } catch (e) {
      
     
    } finally {
    
  
    }
  }

  async getCreatorLocation(){
    
    
    try {
    
      let response = await this.webService.getCreatorDashboard('creator_location')
      if(response.data){
     
        this.creator_location = response.data
       
       

      }
   

    

    } catch (e) {
      
     
    } finally {
    
  
    }
  }

  async getCreatorCampaign(){
    
    
    try {
    
      let response = await this.webService.getCreatorDashboard('creator_campaign')
      if(response.data){
     console.log(response.data)
        this.creator_campaign = response.data
       
       

      }
   

    

    } catch (e) {
      
     
    } finally {
    
  
    }
  }

  async getCreatorDemographic(){
    
    
    try {
    
      let response = await this.webService.getCreatorDashboard('creator_age')
      if(response.data){
     console.log(response.data)
        this.creator_age = response.data
       
       

      }
   

    

    } catch (e) {
      
     
    } finally {
    
  
    }
  }

  async getManagement(){
    
    
    try {
    
      let response = await this.webService.getCreatorDashboard('creator_management')
      if(response.data){
        console.log('here')
     console.log(response.data)
        this.creator_management = response.data
       
       

      }
   

    

    } catch (e) {
      
     
    } finally {
    
  
    }
  }

  async getCreatorPersona(){
    
    
    try {
    
      let response = await this.webService.getCreatorDashboard('creator_persona')
      this.creator_persona = response.data
      if(this.creator_persona){
        this.creator_persona = Object.entries(this.creator_persona).map(([k, v]) => {
          return {
            name: k,
            value: v
          
          };
        });

      }
   

    

    } catch (e) {
      
     
    } finally {
    
  
    }
  }


  async getTotalCreator(){
    
    
    try {
    
      let response = await this.webService.getCreatorDashboard('total_creator')
      this.total_creator = response.data?response.data:'0'
     
    } catch (e) {
      
     
    } finally {
    
  
    }
  }


  async getTotalManagement(){
    
    
    try {
    
      let response = await this.webService.getCreatorDashboard('total_management')
      
      this.total_management = response.data?response.data:'0'
     console.log(this.total_management)
    } catch (e) {
      
     
    } finally {
    
  
    }
  }



  async getTotalCampaign(){
    
    
    try {
    
      let response = await this.webService.getCreatorDashboard('total_campaign')
      
      this.total_campaign = response.data?response.data:'0'
    
    } catch (e) {
      
     
    } finally {
    
  
    }
  }

}
