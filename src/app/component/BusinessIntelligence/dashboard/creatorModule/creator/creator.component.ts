import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faPhone, faEnvelope, faMapPin, faChevronDown, faDotCircle, faChevronCircleRight, faChevronRight, faColumns, faExpandAlt, faExpandArrowsAlt, faExpand, faKey, faTag, faListAlt, faTrash, faUserTag } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-creator',
  templateUrl: './creator.component.html',
  styleUrls: ['./creator.component.scss']
})
export class CreatorComponent implements OnInit {

  faKey=faKey
  faUserTag=faUserTag
  faListAlt=faListAlt
  faTrash=faTrash
  platform_list = [
    {
      'id':'platform_name',
      'label': 'instagram',
      'value': 'instagram'
    },
    {
      'id':'platform_name',
      'label': 'tiktok',
      'value': 'tiktok'
    }
  ]
  creator_rate = {
    'platform_name' : null,
    'rate_name' : null,
    'rate_price' : null,
    'creator_id' : null,
  }

  tier = [
    {
      'id' : 'Nano',
      'value' : '( 1000 - 9999 )'
    },
    {
      'id' : 'Micro',
      'value' : '( 10000 - 99999 )'
    },
    {
      'id' : 'Macro',
      'value' : '( 100000 - 999999 )'
    },
    {
      'id' : 'Mega',
      'value' : '( >= 1000000 )'
    }

  ]
  partnership = [
    {
      'id' : '⁠exclusive',
      'value' : '⁠Exclusive'
    },
    {
      'id' : 'non_⁠exclusive',
      'value' : 'Non ⁠Exclusive'
    },
   

  ]

  tab_menu_list = [{
    'id': 'creator',
    'label': 'Creator',

  },
  {
    'id': 'management',
    'label': 'Management',

    
  },








]


  company_type = [
    {
      'name' : 'Management',
    
    },
    {
      'name' : 'Personal',
    
    },

  ]


  data = [
    {
      "first_name": "anya",
      "last_name": "geraldine",
      "tier": "macro"
    },
    {
      "first_name": "Rosy",
      "last_name": "Posy",
      "tier": "Micro"
    },
    {
      "first_name": "dr.",
      "last_name": "Andhika",
      "tier": "Macro"
    },
    {
      "first_name": "Amelia",
      "last_name": "Gonta",
      "tier": "Micro"
    },
    {
      "first_name": "Adrian",
      "last_name": "Setiaji",
      "tier": "Macro"
    }
  ]


  createContactForm = new FormGroup({
    first_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    last_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    alias: new FormControl(null),

    dob: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    location: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    persona: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),



    tier: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    company_type: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    company_name: new FormControl(null),

    create_by: new FormControl(null),

    creator_management_id: new FormControl(null),
    


    
  })

  accessForm = new FormGroup({
    username: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    password: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    creator_id: new FormControl(null),

    creator_management_id: new FormControl(null),
    


    
  })
  faExpandAlt= faExpandAlt
  faPhone=faPhone
  faEnvelope= faEnvelope
  faMapPin =faMapPin
  faChevronDown = faChevronDown
  faDotCircle=faDotCircle
  faChevronRight=faChevronRight
  faColumns=faColumns
  show_columns: boolean;
  columns_view: string;
  selected_columns: any;
  showModal: any;
  showMiniModal: any;
  alias: boolean;
  creator: any;
  selected_creator: any;
  selected_access: any;
  error_message: string;
  search: string;
  params: any;

  persona: { name: string; }[];
  city_list: any[];
  city_filter: any;
  tab_view: string;
  management: any;
  management_list: any[];
  selected_management: any;

  persona_list: { name: string; }[];
  status: string;
  file: File;
  sortListItems: ({ name: string; items: { name: string; }[]; label?: undefined; } | { name: string; items: { name: string; label: string; }[]; label?: undefined; } | { name: string; label: string; items: any[]; })[];
  page: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    private serializer: UrlSerializer,
    private http: HttpClient
  ) { }



  ngOnInit(): void {
   
    this.persona = this.appService.persona
    this.management_list = []


    this.route.queryParamMap.subscribe(queryParams => {
      this.tab_view = queryParams.get("tab_view")

      this.params = this.serializer.serialize(this.router.createUrlTree([''],
      { queryParams: this.route.snapshot.queryParams}))      

      if(this.tab_view == 'creator'){
        this.getCreator()



      }

      if(this.tab_view == 'management'){
        this.getManagement()

      }

    })


    this.sortListItems = [
      
      {
       
        'name' : 'persona',
        'items' : this.persona

      },
      {
       
        'name' : 'tier',
        'items' :[
          {
            'name' :'nano',
            'label' :'nano'
          },
          {
            'name' :'micro',
            'label' :'micro'
          },
          {
            'name' :'macro',
            'label' :'macro',

          },
          {
            'name' :'mega',
            'label' :'mega',

          }
        ]

      },
      {
       
        'name' : 'partnership',
        'items' :[
          {
            'name' :'exclusive',
            'label' :'Exclusive'
          },
          {
            'name' :'non_exclusive',
            'label' :'Non Exclusive'
          },
          
        ]

      },
      {
       
        'name' : 'min_age',
        'label' : 'Min Age',
        'items' : this._getAgeRange()

      },
      {
        'name' : 'max_age',
        'label' : 'Max Age',
        'items' : this._getAgeRange()

      }
    ]

    this.getCityFilter()

  
  
  }
  test(){
    console.log('shown')
  }


  async onChange(event: any) {
    const file: File = event.target.files[0];

    if (file) {
      this.status = "initial";
      this.file = file;

      const formData = new FormData();
  
      formData.append('file', this.file);

      // let form ={
      //   'file' : this.file
      // }
      console.log(formData)
      
      // try {
     
      //   let res = await this.webService.uploadPhoto(form)
      //   console.log(res)
      //   this.status = 'success';
      // } catch (error) {
      //   this.appService.openToast(error)
      // }
      // finally{
      //   this.appService.hideLoading()
      
      // }

    }
  }


  _getPersona(){
    this.persona_list = this.persona
    console.log(this.appService.persona)
  }

  getPlaceholderName(item) {
    if(item.first_name && item.last_name){
      var first_name =item.first_name.charAt(0);
      var last_name =item.last_name.charAt(0);
      return first_name + last_name
    }
  
  }



  _getAge(dob){
    if(dob){
      var today = new Date()
      var receive = new Date(dob)
      let time = Math.round((today.getTime() - receive.getTime())/(1000*60*60*24*365));
     return time
    }
   
  }


  _getAgeRange(){
    var foo = [];

    for (var i = 10; i <= 80; i++) {
      foo.push({
        'id' : i,
        'name' : i,
        'label' : i.toString() + ' Years'
      });
    }
    return foo
  }

  async getCityFilter(){
    
    this.city_list=[]
    try {
      this.appService.showLoading()
      let res = await this.webService.getCityList('','')
      
     console.log(res.data)

     if(res.data){
      this.city_filter = res.data.map(item => {
        return {
          id: item.city_id,
          label: item.city_name,
          name: item.city_name
        };
      });

      console.log(this.city_filter)

      this.sortListItems.push(
        {
       
          'name' : 'location',
          'items' : this.city_filter
        },
      )

    }
   
     
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
    
    
  }

  viewManagement(item){
    this.selected_management = item

    this.selected_management.creator.forEach(creator => {
      creator.performance = this._getPerformance(creator.performance_score)
      creator.persona = JSON.parse(creator.persona)
    });
  }

  async deleteManagement(id){
   

    try {
      this.appService.showLoading()
      let res = await this.webService.deleteManagement(id)
      
      if(res.data){
        this.ngOnInit()
      }
     


     
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }

  }

  async updateManagement(id,e){
    console.log(e)

    let form = {}
    form[e.id] = e.value

    try {
      this.appService.showLoading()
      let res = await this.webService.updateManagement(form,id)
      console.log(res)
      if(res.data){
        this.appService.openToast('Updated!')
        this.ngOnInit()
      }

     
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }

  }
  async searchCity($event){
    console.log($event)
    this.city_list=[]
    try {
      this.appService.showLoading()
      let res = await this.webService.getCityList('10',$event)
      
      if(res.data){
        this.city_list = res.data.map(item => {
          return {
            id: item.city_id,
            label: item.city_name,
            name: item.city_name
          };
        });
  
      }
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
    
    
  }

  selectManagement(e){
    console.log(e)
    this.createContactForm.get('creator_management_id').setValue(e.id)
  }

  async searchManagement(e){
    console.log(e)
    let search = '?search='+e
    try {
      this.appService.showLoading()
      let res = await this.webService.getManagement(search)
      console.log(res)

      if(res.data){
        this.management_list = res.data.map(item => {
          return {
            id: item.id,
            label: item.name,
            name: item.name
          };
        });
      }
      
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.hideLoading()
  
    }

  }

  selectCity(e){
 
    this.createContactForm.get('location').setValue(e.name)

  }

  async deleteCreatorRate(item){
    // console.log(item)
    
      try {
      
        let response = await this.webService.deleteCreatorRate(item.id)
        console.log(response)
        this.appService.openToast('Ratecard Removed!')
      
      } catch (e) {
        
        this.appService.openToast(e)
        
        console.log(e)
      } finally {
        let index = this.selected_creator.rate.indexOf(item)
        this.selected_creator.rate.splice(index, 1)
      
      }
    }

    
  viewRate(item){
    this.selected_creator = item
    this.openModal('rateModal')
  }

  viewProject(item){
    this.selected_creator = item
    this.openModal('projectModal')
  }

 selectPersona(e){
  console.log(e)
  this.createContactForm.get('persona').setValue(JSON.stringify(e))
 }


  viewSosmed(item){
    this.selected_creator = item
  }

  viewAccess(access){
    console.log(access)
    this.selected_access = access
    this.openModal('accessModal')
  }

  viewContact(item){
    this.selected_creator = item
    this.openModal('contactModal')
  }



  
  async getCreator(){
    this.page = 1
    try {
      this.appService.showLoading()
      let response = await this.webService.getCreator(this.params)
    
      this.creator = response.data
      this.creator.forEach(creator => {
        creator.performance = this._getPerformance(creator.performance_score)
        creator.persona = JSON.parse(creator.persona)
      
      });
      console.log(response)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.hideLoading()
  
    }
  }




  async loadMore() {
    this.page = this.page+1
    this.params = this.params+'&page='+this.page

    console.log(this.params)
    

    try {
      this.appService.showLoading()
      let response = await this.webService.getCreator(this.params)
      if(response.data){
        response.data.forEach(item => {
          item.performance = this._getPerformance(item.performance_score)
          item.persona = JSON.parse(item.persona)
        
        });

      this.creator = this.creator.concat(response.data)
      
    }
     // this.creator = this.creator.contact(response.data)
     
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.hideLoading()
  
    }
  }



  async getManagement(){
    console.log('get mana')
    
    try {
      this.appService.showLoading()
      let response = await this.webService.getManagement(this.params)
      console.log(response)
      this.management = response.data
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.hideLoading()
  
    }
  }



  
  async addAccess(){
      if(this.tab_view =='creator'){
        this.accessForm.get('creator_id').setValue(this.selected_access.id)

      }
      if(this.tab_view=='management'){
        this.accessForm.get('creator_management_id').setValue(this.selected_access.id)

      }

    try {
      console.log(this.accessForm.value)

   
      let response = await this.webService.addClientAuth(this.accessForm.value)
      console.log(response)
      if(response.data){
        this.closeModal()
        this.appService.openToast('Success!')
     
      }
      
    } catch (e) {
      
      this.appService.openToast(e)
      this.appService.openToast('username exist, please use another username')
      this.error_message = 'username exist, please use another username'
      console.log(e)
    } finally {
     this.accessForm.reset
      // this.appService.hideLoading()
      // this.appService.openToast('New creator added!')
      this.getCreator()
      
    }
  }


  _getPerformance(total){
    

  
      if( total > 67 && total < 100){
        return 'best'
      }
      else if( total > 37 && total < 67){
        return 'good'
      }
      else if( total >0 && total< 37){
        return 'bad' 
      }
      else{
        return null;
      }
      
     
      
     
  }

  async addCreator(isView){
    
    console.log(this.createContactForm.value)

    this.createContactForm.get('create_by').setValue(this.webService.account_id)
    try {
    this.appService.showLoading()
      let response = await this.webService.addCreator(this.createContactForm.value)
      
      if(isView){
        this._navigateTo(response.data.id)
      }else{
        this.getCreator()
        window.location.reload();
        
      }


     
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.persona_list = []
      this.createContactForm.reset()
      this.appService.hideLoading()
      this.closeModal()
      this.appService.openToast('New creator added!')
    //  this.ngOnInit()
  
    }
  }


  async addCreatorManagement(){
    
   let form = {
    
   }
    try {
    this.appService.showLoading()
      let response = await this.webService.addCreatorManagement(form)
      console.log(response)
      if(response.data){
        this.ngOnInit()
      }
      
     console.log(response)

    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
      this.appService.openToast(' added!')
    
      
    }
  }

  
  async updateRate(item,id){
    let form = {}
    form[item.id] = item.value
    console.log(item)
    try {
    
     let response = await this.webService.updateCreatorRate(form,id)
      
      this.appService.openToast('Updated!')
      
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
      this.toggleRateEdit(item)
    }
  }
  
  toggleRateEdit(item){
    item.edit = !item.edit
    }


  async addNewRateCart(){


  
    try {
        this.creator_rate.creator_id = this.selected_creator.id
      let response = await this.webService.addCreatorRate(this.creator_rate,this.selected_creator.id)
      console.log(response.data)
      this.appService.openToast('New Rate Added')
      this.selected_creator.rate.unshift(response.data)
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.closeMiniModal()
    }
  }

  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }


 


  _navigateTo(page) {
    this.router.navigate(
      ['/creator/creator/view/'+page]
    );
  }



  openMiniModal(modal){
    this.showMiniModal = modal
  }
  closeMiniModal(){
    this.showMiniModal = null
  }
  toggleAlias(){
    this.alias = !this.alias
  }

}
