import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faAddressBook, faCalendarAlt, faChevronDown, faChevronLeft, faChevronRight, faCloudUploadAlt, faMapMarker, faPhotoVideo, faTrash, faUpload, faUserPlus } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-creator-view',
  templateUrl: './creator-view.component.html',
  styleUrls: ['./creator-view.component.scss']
})
export class CreatorViewComponent implements OnInit {
  faAddressBook=faAddressBook
  faChevronDown = faChevronDown
  faCloudUploadAlt=faCloudUploadAlt
  faTrash=faTrash
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  showModal: any;
  tab_view: string;
  right_tab_view: string;
  insight: { title: string; Description: string; link: string; fn: string; icon: string; }[];
  tab_menu_list: { id: string; label: string; count: number; class: any; }[];
  creator: any;
  creator_id: string;
  isEdit: string;
  isRateEdit: boolean;
  
  creator_rate = {
    'platform_name' : null,
    'rate_name' : null,
    'rate_price' : null,
    'creator_id' : null,
  }


  creatorRateForm = new FormGroup({
    platform_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    rate_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    rate_price: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    creator_id: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    
    
  })


  overview: { label: string; value: string; }[];
  social_metric: { platform_name: string; metric_name: string; metric_value: string; creator_id: string; }[];
  performance_form: { performance_name: any; performance_score: any; creator_id: any; }[];
  selected_contract: any;

  platform_list = [
    {
      'id':'platform_name',
      'label': 'instagram',
      'value': 'instagram'
    },
    {
      'id':'platform_name',
      'label': 'tiktok',
      'value': 'tiktok'
    }
  ]


  contactForm = new FormGroup({
    
    name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    work: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    mobile: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    position: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    creator_id: new FormControl(null),
  
  })

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

  ngOnInit(): void {
    
    this.route.paramMap.subscribe(params=>{
      this.creator_id = params.get("id")
      this.getCreatorById(this.creator_id)
    })

    this.setComponentData()
    
    
  
  
  }

  updateSocialMetric(item){

  }
  setComponentData(){
    this.tab_menu_list = [
      {
        'id' : 'instagram',
        'label' : 'Instagram',
        'count' : 10,
        'class' : null,
      },
      {
        'id' : 'assigned_task',
        'label' : 'Tiktok',
        'count' : 10,
        'class' : null,
      },
    
    ]

    

  }


  viewContract(item){
    this.selected_contract = item.contract
    this.openModal('contractModal')
  }
  

  download(link){
    window.open(link, '_blank');
  }
  
  async onChange(event: any,id) {
    const file: File = event.target.files[0];

    if (file) {
     

      const formData = new FormData();
  
      formData.append('file', file);

     
      
      try {
        this.appService.showLoading()
        let res = await this.webService.uploadPhoto(formData)
        console.log(res)
        if(res.data){
          let form = {
            'id' : id,
            'value' : res.data
          }

        
          this.updateData(form)

        }
        
      } catch (error) {
        this.appService.openToast(error)
      }
      finally{
       
        this.appService.hideLoading()
      
      }

    }
  }

  
  async getCreator(e){
   
    try {
    
      let response = await this.webService.getCreator(e)
    
      this.creator = response.data
      this.creator.persona = JSON.parse(this.creator.persona)
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      
      
    }
  }


    
  async getCreatorById(id){
    
    
    try {
    
      let response = await this.webService.getCreatorById(id)
    
      this.creator = response.data
      this.creator.persona = JSON.parse(this.creator.persona)
      if(this.creator.performance.length == 0){
       this.generatePerformance()
      }

      // this.creator.campaign.forEach(item => {
      //   item.contract.scope_of_work = JSON.parse(item.contract?.scope_of_work)
      // });

      console.log(this.creator)
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.setOverview()
  
    }
  }

  async addContact(){
    this.contactForm.get('creator_id').setValue(this.creator_id)


    try {
    this.appService.showLoading()
    let response = await this.webService.addContact(this.contactForm.value)
      this.creator.contact.push(response.data)
    console.log(response)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.hideLoading()
    
      this.closeModal()

    }
}


async deleteContact(id){
  


  try {
  this.appService.showLoading()
  let response = await this.webService.deleteContact(id)
 
  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
    this.appService.hideLoading()
  this.ngOnInit()

  }
}





async updateContact(id,contact){
   
  
  let form = {}
  form[contact.id] = contact.value

  console.log(form)
try {

 let response = await this.webService.updateContact(id,form)
console.log(response)

} catch (e) {
  
  this.appService.openToast(e)
  
  console.log(e)
} finally {
 

}
}


  async updateData(data){
    
  let form = {}
  form[data.id] = data.value
    console.log(form)
    if(this.creator.id){
      try {
    
        let response = await this.webService.updateCreator(form,this.creator.id)
        console.log(response)
        this.appService.openToast('Updated!')
      
      } catch (e) {
        
        this.appService.openToast(e)
        
        console.log(e)
      } finally {
        this.ngOnInit()
    
      }
    }

  }

  generatePerformance(){
    let form = [
      {
        'performance_name' :'Communication',
        'performance_score' : null,
        'creator_id' : this.creator.id,
        'create_by' : this.webService.account_id
      },
      {
        'performance_name' :'Delivery',
        'performance_score' : null,
        'creator_id' : this.creator.id,
        'create_by' : this.webService.account_id
      },
      {
        'performance_name' :'Work Ethic',
        'performance_score' : null,
        'creator_id' : this.creator.id,
        'create_by' : this.webService.account_id
      }
    ]
    form.forEach(element => {
      this.addPerformance(element)
    });  
}
async updatePerformance(item){
  console.log(item)
    let form = {
     
      'performance_score' : item.value,
      'create_by' : this.webService.account_id
      
    }

      try {
        this.appService.showLoading()
       let response = await this.webService.updatePerformance(form,item.id)
       console.log(response)
       this.setOverview()
       
  
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      
      this.appService.hideLoading()
    }
}
async addPerformance(form){
   

      try {
        this.appService.showLoading()
       let response = await this.webService.addPerformance(form)
       console.log(response)
       this.creator.performance.push(response.data)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.hideLoading()
    }
    
  }

    setOverview(){
      if(this.creator){
        console.log('dasdas')
        this.overview = [
          {
            'label' : 'Campaign',
            'value' : this.creator.campaign.length + 'Campaign'
          },
          {
            'label' : 'Company Type',
            'value' : this.creator.company_type
          },
          {
            'label' : 'Performance',
            'value' : this._getPerformance()
          },
          {
            'label' : 'Ratecard',
            'value' : this.creator.rate.length + ' Services'
          }
        ]
      }
    }

  toggleRateEdit(item){
  item.edit = !item.edit
  }

  _getPerformance(){
    let total  = 0
      this.creator.performance.forEach(item => {
           total += item.performance_score / 3
      });

      if( total > 67 && total < 100){
        return 'best'
      }
      else if( total > 37 && total < 67){
        return 'good'
      }
      else if( total >0 && total< 37){
        return 'bad' 
      }
      else{
        return 'not set'
      }
     
  }

  async updateCreatorRate(item){
    console.log(item)
  
    let form = {
      'rate_name' : item.rate_name,
      'rate_price' : item.rate_price,
    }
    try {
    
      let response = await this.webService.updateCreatorRate(form,item.id)
      
      this.appService.openToast('Updated!')
      
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
      this.toggleRateEdit(item)
    }
  }

  async updateRate(item,id){
    let form = {}
    form[item.id] = item.value
    console.log(item)
    try {
    
     let response = await this.webService.updateCreatorRate(form,id)
      
      this.appService.openToast('Updated!')
      
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
      this.toggleRateEdit(item)
    }
  }


  async deleteCreatorRate(item){
  // console.log(item)
  
    try {
    
      let response = await this.webService.deleteCreatorRate(item.id)
      console.log(response)
      this.appService.openToast('Ratecard Removed!')
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      let index = this.creator.rate.indexOf(item)
      this.creator.rate.splice(index, 1)
    
    }
  }


  createNewRate(){
    let rate = {
      "creator_id" : this.creator.id,
      "platform_name": null,
      "platform_id": null,
      "rate_name": null,
      "rate_price": null,
      "rate_price_per": null,
      "isNew" : true,
      "edit": true
  }
    this.creator.rate.unshift(rate)
  }



  getPlaceholderName(item) {
    if(item.first_name && item.last_name){
      var first_name =item.first_name.charAt(0);
      var last_name =item.last_name.charAt(0);
      return first_name + last_name
    }
  
  }



  async addNewRateCart(){

    this.creatorRateForm.get('creator_id').setValue(this.creator_id)

    try {
      this.appService.showLoading()
      let response = await this.webService.addCreatorRate(this.creatorRateForm.value,this.creator.id)
    
      response.data.isNew = true
      this.appService.openToast('New Rate Added')
      this.creator.rate.unshift(response.data)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.creatorRateForm.reset()
      this.appService.hideLoading()
      this.closeModal()
    }
    
  }




  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }

  _navigateToTabView(tab_view) {
    this.router.navigate(
      [],
      { queryParams: { tab_view: tab_view},
      queryParamsHandling: 'merge' }
    );
  }
  _navigateToRightTabView(tab_view) {
    this.router.navigate(
      [],
      { queryParams: { right_tab_view: tab_view},
      queryParamsHandling: 'merge' }
    );
  }

}
