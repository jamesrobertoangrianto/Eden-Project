import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faCoins, faUsers, faDownload, faUser, faGift, faBullhorn, faInbox, faCheckCircle, faCircle, faCircleNotch, faHandHolding, faHandHoldingUsd, faHandHoldingHeart, faUserEdit } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-community-account',
  templateUrl: './community-account.component.html',
  // styleUrls: ['./community-account.component.scss']
  styleUrls: ['../client-login/client-login.component.scss']
})
export class CommunityAccountComponent implements OnInit {
  faUser =faUserEdit
  faHandHolding=faHandHoldingHeart
  faDownload= faDownload
  faCircle=faCircle
  faCircleNotch=faCircleNotch
  faCheckCircle=faCheckCircle
  faCoins=faCoins
  faGift=faGift
  faChevronDown = faChevronDown
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];
  contract_status: { id: string; label: string; class: string; }[];
  booking_status: { id: string; label: string; class: string; }[];
  tab_view: string;

  main_menu =[
    {
      'id':'personal',
      'label':'Personal Information',
      'icon' : faUser
      
    },
    {
      'id':'campaign',
      'label':'Campaign',
      'icon' : faBullhorn
    },
    {
      'id':'reward',
      'label':'Reward',
      'icon' : faGift 
    },
    {
      'id':'points',
      'label':'Points Activity',
      'icon' : faCoins
    },
    
    {
      'id':'ticket',
      'label':'Message',
      'icon' : faEnvelope
    },
    
  ]

  small_main_menu =[
   
    {
      'id':'campaign',
      'label':'Campaign',
      'icon' : faBullhorn
    },
    {
      'id':'reward',
      'label':'Reward',
      'icon' : faGift 
    },
    {
      'id':'points',
      'label':'Points',
      'icon' : faCoins
    },
    
    {
      'id':'ticket',
      'label':'Message',
      'icon' : faEnvelope
    },
    
  ]

  registerForm = new FormGroup({
    first_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    location: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    city: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),



    


    dob: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),



    working_status: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    number_of_kids: new FormControl(null),

    is_breast_feed: new FormControl(null),

    field: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

      
  
   
    mothly_expense: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

     what_to_expect: new FormControl(null),


   

    instagram_account: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    instagram_account_link: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    instagram_follower: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    phone: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


   
   
  })

  domicile_list = [
    'Jakarta','Bogor','Depok','Tangerang - Banten', 'Bekasi','Jawa Barat','Jawa Tengah','Jawa Timur','Bali, NTT, NTB','Sumatra, Aceh, Riau','Kalimantan','Sulawesi','Papua & Lainnya'
  ]
  working_status = [
   
    {
      value : 'Pegawai Swasta',
      label:'Pegawai Swasta',
    
    },
    {
      value : 'PNS',
      label:'PNS',
     
    },
    {
      value : 'Wirausaha',
      label:'Wirausaha'
    },
    
    {
      value : 'Freelancer',
      label:'Freelancer'
    },
    {
      value : 'Mahasiswa',
      label:'Mahasiswa'
    },
    
    {
      value : 'Lainnya',
      label: 'Lainnya'
    },
    
   
  ]


  number_of_kids = [
   
    {
      value : 'Calon Ibu',
      label:'Calon Ibu'
    },
    {
      value : '1 - 2 Anak',
      label:'1 - 2 Anak'
    },
    {
      value : 'Lebih dari 2 Anak',
      label:'Lebih dari 2 Anak'
    },
    
   
  ]


  is_breast_feed = [
   
    {
      value : 'Yes',
      label:'Yes'
    },
    {
      value : 'No',
      label:'No',
     
    },
    
    
   
  ]

  field = [
    
    {
      value : 'Fitness',
      label:'Fitness'
    },
    {
      value : 'Health & Beauty',
      label:'Health & Beauty',
      selected : true
    },
    {
      value : 'Digital',
      label:'Digital'
    },
    {
      value : 'Home Decor',
      label:'Home Decor'
    },
    {
      value : 'Memasak/Kuliner',
      label:'Memasak/Kuliner'
    },
    {
      value : 'Lainnya',
      label: 'Lainnya'
    },
  
   
  ]

  information_source = [
    
    {
      value : 'Social Media',
      label:'Social Media'
    },
    {
      value : 'Teman',
      label:'Teman',
      selected : true
    },
    {
      value : 'WhatsApp Group',
      label:'WhatsApp Group'
    },
    {
      value : 'Komunitas',
      label:'Komunitas'
    },
    {
      value : 'Lainnya',
      label:'Lainnya'
    },
    
  
   
  ]

  mothly_expense = [
    
    {
      value : '>5 Juta',
      label:'>5 Juta',
      //selected : '>5 Juta'
      
    },
    {
      value : '3-5 Juta',
      label:'3-5 Juta',
      
    },
    {
      value : '1-2 Juta',
      label:'1-2 Juta'
    },
    {
      value : '< 1 Juta',
      label:'< 1 Juta'
    },
    
  
   
  ]
  


  member: any;
  member_id: string;
  ticket: any;
  selected_ticket: any;
  message: any;
  selected_vendor: any;
  client_session: any;
  tab_menu_list: { id: string; label: string; }[];
  tab_menu_list_campaign: { id: string; label: string; }[];
  reward_collection: any;
  page_view: string;
  is_update: any;
  campaign: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }


  ticketForm = new FormGroup({
    title: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    topic: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    message: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    status: new FormControl('new_topic'),
    community_member_id: new FormControl(null),
    
    
    
    
  })


  productForm = new FormGroup({
    name: new FormControl(null, {
      validators: [Validators.required, Validators.nullValidator]
    }),
    type: new FormControl('channel'),
    vendor_id: new FormControl(null),

    category_id: new FormControl(null),

    price: new FormControl(null),


    stock: new FormControl(null),

  })

   
  ngOnInit(): void {
    this.client_session = this.webService.getClientSession()
   
   
    this.route.queryParamMap.subscribe(queryParams => {
      this.tab_view = queryParams.get("tab_view")
      this.page_view = queryParams.get("page_view")

      if(!this.page_view){
        this.page_view = 'campaign'
      }


    this.route.paramMap.subscribe(params=>{
      this.member_id = params.get("id")
      this.getCommunityMemberById(this.member_id)
      this.getTicketByMemberId(this.member_id)
      this.getReward()
    })

    })

    
    
   

    this.setComponentData()
  }


  selectOption(item,array,id){
    console.log(item)

    array.map(item => {
      item.selected =  null
    });

    item.selected = item.value
    this.registerForm.get(id).setValue(item.value)

    
  }

  async redeem(item){
   let form ={
    'status' : 'redeem',
    'community_member_id' :this.member_id,
    'community_reward_id': item.id

   }
    try {
      this.appService.showLoading()
      let response = await this.webService.addMemberRedeem(item.id,this.member_id,form)
      console.log(response)
    if(response.data){
      this.appService.openToast('Redeem Success')
      this.ngOnInit()
    }
    
  
    } catch (e) {
      this.appService.openToast(e)
      console.log(e)
    
    }
    finally{
      this.appService.hideLoading()
    
      this.closeModal()
    }
  }



  addCustomData(e,id){
    this.registerForm.get(id).setValue(e.target.value)
    console.log(e.target.value)
  }


  async getReward(){
    try {
      this.appService.showLoading()
      let res = await this.webService.getReward()
      this.reward_collection = res.data
      console.log(res)
   console.log('res')
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }

  setComponentData(){
    this.tab_menu_list = [
      {
        'id' : 'collection',
        'label' : 'Collection',
     
      },
      {
        'id' : 'my_reward',
        'label' : 'My Reward',
       
      },
      

    
    
     
     
    ]
    this.tab_menu_list_campaign = [
      {
        'id' : 'collection',
        'label' : 'Open Registration',
       
      },
      {
        'id' : 'my_campaign',
        'label' : 'My Campaign',
       
      },
     
    

    
    
     
     
    ]
    
  }


  viewTicket(item){
  //  this.selected_ticket = item
    this.getCommunityTicketById(item.id)

  }



  async getCommunityCampaign(){
    this.campaign =[]
  
    if(this.member?.community_id){
     if(this.tab_view){
      try {
        if(this.tab_view == 'my_campaign'){
        var param = '?community_id='+this.member.community_id

        }else if(this.tab_view == 'collection'){
        var param = '?status=open_registration'

        }

        this.appService.showLoading()
        let res = await this.webService.getCommunityCampaign(param)
        this.campaign = res.data
        this.campaign.forEach(item => {
          item.is_apply =  this._checkParticipant(item.participant)
        });
        console.log(res)
     
      
      } catch (error) {
        this.appService.openToast(error)
      }
      finally{
        this.appService.hideLoading()
      
      }
     }
    }
    
  }

  _checkParticipant(array){
    if(array.some(item =>
      item.community_member_id === this.member.id
    
    )){
      return true
      } else{
         return false
      }
     
  }


  public numbersOnlyValidator(event: any) {
    const pattern = /^[0-9\-]*$/;
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^0-9\-]/g, "");
    }
  }

  async updateCommunityMember(){
    console.log(this.registerForm.value)
    if(this.member_id){
      try {
     
        let res = await this.webService.updateCommunityMember(this.registerForm.value,this.member_id)
        if(res.data){
          this.toggleUpdate()
            this.appService.openToast('Updated!')
            this.ngOnInit()
        }
        console.log(res)
      
      } catch (error) {
        this.appService.openToast(error)
      }
      finally{
     
      }
    }
    
   
  }

  toggleUpdate(){
    this.is_update = !this.is_update
  }

  
  async getCommunityTicketById(id){
    try {
     
      this.appService.showLoading()
      let res = await this.webService.getCommunityTicketById(id)
      this.selected_ticket = res.data

      this.openModal('ticketModal')
      console.log(res)
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }


  async addToCampaign(item){
    
    let form = {
      'community_campaign_id' : item.id,
      'community_member_id' : this.member_id,
      'status' : 'request'
    }

    console.log(item)

      if(this.member.id){
        try {
      
          let response = await this.webService.addCommunityCampaignMember(item.id,form)
        //   let a = response.data
        //   a.member = item
        //   item.isAdded = true
        //  this.campaign.participant.unshift(a)
if(response.data){
  this.appService.openToast('Terimakasih telah mendaftar! Kamu dapat mengikuti campaign ini setelah status kamu di setujui')
}

       
        
        } catch (e) {
          
          this.appService.openToast(e)
          
          console.log(e)
        } finally {
        this.ngOnInit()
      
        }
      }
  }

  async addTicketDiscussion(){
    
    let form = {
      'community_member_id' : this.selected_ticket.community_member_id,
      'community_ticket_id' : this.selected_ticket.id,
      'message' : this.message
    }

    try {
      
      let response = await this.webService.addTicketDiscussion(form)
   
      console.log(response)
      this.appService.openToast('Send!')
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.message = null
      this.getCommunityTicketById(this.selected_ticket.id)
  
    }
  }


  async getTicketByMemberId(id){
    try {
      this.appService.showLoading()
      let res = await this.webService.getTicketByMemberId(id)
      this.ticket = res.data
      console.log('wo')
      console.log(this.ticket)
   

    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }



  async getCommunityMemberById(id){ 
    
    try {
      this.appService.showLoading()
      let res = await this.webService.getCommunityMemberById(id)
      this.member = res.data
      this.registerForm.get('first_name').patchValue(this.member.first_name)
       this.registerForm.get('location').patchValue(this.member.location)
       this.registerForm.get('city').patchValue(this.member.city)
       this.registerForm.get('dob').patchValue(this.member.dob)
       this.registerForm.get('phone').patchValue(this.member.phone)
       this.registerForm.get('instagram_account').patchValue(this.member.instagram_account)
       this.registerForm.get('instagram_follower').patchValue(this.member.instagram_follower)
       this.registerForm.get('instagram_account_link').patchValue(this.member.instagram_account_link)



      
      if(this.client_session.community_member_id == this.member.id){
    
        if(this.member.auth.password !== this.client_session?.password){
         
          this.router.navigate( ['client/login'] );

        }
      }else{
        this.router.navigate( ['client/login'] );

      }
      
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
      this.getCommunityCampaign()
      
    }
  }


  async addVendorProduct(item ? ) {
    if (item) {
      this.selected_vendor = item
    }
    console.log(item)
    this.productForm.get('vendor_id').setValue(this.selected_vendor.id)
    console.log(this.productForm.value)
    try {

      let res = await this.webService.addVendorProduct(this.productForm.value)
      if (res.data) {
        res.data.isNew = true
        this.selected_vendor.product_count = this.selected_vendor.product_count + 1
        this.selected_vendor.product.unshift(res.data)
        this.appService.openToast('New Product Added!')
      }



    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.productForm.reset()
      this.closeModal()
    }


  }

  
  async addTicket(){
    
    this.ticketForm.get('community_member_id').setValue(this.member_id)
    try {
      console.log(this.ticketForm.value)
      let response = await this.webService.addTicket(this.ticketForm.value)
   
      console.log(response)
      this.appService.openToast('Send!')
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.ticketForm.reset()
      this.closeModal()
      this.getTicketByMemberId(this.member_id)
    }
  }





  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }



  _navigateTo(page) {
    this.router.navigate(
      [], { queryParams: 
        { 
          page_view: page,
        
        },
       
      }
    );
  }




}
