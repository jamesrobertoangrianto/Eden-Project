import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faCoins, faUsers, faDownload, faTrash, faBullhorn, faGift, faUser, faTag, faUserTag, faAddressBook } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
@Component({
  selector: 'app-creator-account',
  templateUrl: './creator-account.component.html',
  styleUrls: ['./creator-account.component.scss']
})
export class CreatorAccountComponent implements OnInit {
  faTrash=faTrash
  faUsers =faUsers
  faDownload= faDownload
  faCoins=faCoins
  faChevronDown = faChevronDown
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];
  contract_status: { id: string; label: string; class: string; }[];
  booking_status: { id: string; label: string; class: string; }[];
  tab_view: string;

  tab_menu_list_campaign: { id: string; label: string; count: number; class: any; }[];
  tab_menu_list: { id: string; label: string; class: any; }[];
  creator: any;
  creator_id: string;


  contactForm = new FormGroup({
    
    name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    work: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    mobile: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    position: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    creator_id: new FormControl(null),
  
  })

  main_menu =[
    {
      'id':'rate',
      'label':'Ratecard',
      'icon' : faUserTag 
    },
    {
      'id':'contact',
      'label':'Contact',
      'icon' : faAddressBook
      
    },
    {
      'id':'campaign',
      'label':'Campaign',
      'icon' : faBullhorn
    },
    
    
    
  ]

  platform_list = [
    {
      'id':'platform_name',
      'label': 'instagram',
      'value': 'instagram'
    },
    {
      'id':'platform_name',
      'label': 'tiktok',
      'value': 'tiktok'
    }
  ]
  creatorRateForm = new FormGroup({
    platform_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    rate_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    rate_price: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    creator_id: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    
    
  })
  selected_contract: any;
  client_session: any;
  page_view: string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

   
  ngOnInit(): void {
    this.client_session = this.webService.getClientSession()
    this.route.queryParamMap.subscribe(queryParams => {
      this.tab_view = queryParams.get("tab_view")

      this.page_view = queryParams.get("page_view")
      console.log(this.page_view)
      if(!this.page_view){
        this.page_view = 'rate'
      }
    })

    this.route.paramMap.subscribe(params=>{
      this.creator_id = params.get("id")

      
      this.getCreatorById(this.creator_id)
    })
    
    this.setComponentData()
    
  
  }

  async updateContact(id,contact){
   
  
    let form = {}
    form[contact.id] = contact.value
  
    console.log(form)
  try {
  
   let response = await this.webService.updateContact(id,form)
  console.log(response)
  
  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
   
  
  }
  }

  async deleteContact(id){
  


  try {
  this.appService.showLoading()
  let response = await this.webService.deleteContact(id)
 this.ngOnInit()
  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
    this.appService.hideLoading()
  
    this.closeModal()

  }
}

  
async addContact(){
  this.contactForm.get('creator_id').setValue(this.creator_id)


  try {
  this.appService.showLoading()
  let response = await this.webService.addContact(this.contactForm.value)
    this.creator.contact.push(response.data)
  console.log(response)
  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
    this.appService.hideLoading()
  
    this.closeModal()

  }
}


  async getCreatorById(id){
    
    
    try {
    
      let response = await this.webService.getCreatorById(id)
    
      this.creator = response.data
      this.creator.persona = JSON.parse(this.creator.persona)
     
      


      if(this.client_session.creator_id == this.creator.id){
        if(this.creator.auth.password !== this.client_session?.password){
          this.router.navigate(
            ['/login']
          );
        }
      }else{
        this.router.navigate(
          ['/login']
        );
      }

      console.log(this.creator)
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      
    }
  }


  async deleteCreatorRate(item){
    // console.log(item)
    
      try {
      
        let response = await this.webService.deleteCreatorRate(item.id)
        console.log(response)
        this.appService.openToast('Ratecard Removed!')
      
      } catch (e) {
        
        this.appService.openToast(e)
        
        console.log(e)
      } finally {
        let index = this.creator.rate.indexOf(item)
        this.creator.rate.splice(index, 1)
      
      }
    }
  

    async addNewRateCart(){

      this.creatorRateForm.get('creator_id').setValue(this.creator_id)
  
      try {
        this.appService.showLoading()
        let response = await this.webService.addCreatorRate(this.creatorRateForm.value,this.creator.id)
      
        response.data.isNew = true
        this.appService.openToast('New Rate Added')
        this.creator.rate.unshift(response.data)
      } catch (e) {
        
        this.appService.openToast(e)
        
        console.log(e)
      } finally {
        this.creatorRateForm.reset()
        this.appService.hideLoading()
        this.closeModal()
      }
      
    }

  getPlaceholderName(item) {
    if(item.first_name && item.last_name){
      var first_name =item.first_name.charAt(0);
      var last_name =item.last_name.charAt(0);
      return first_name + last_name
    }
  
  }



  viewContract(item){
    this.selected_contract = item.contract
    this.openModal('contractModal')
  }
  

  async updateRate(item,id){
    let form = {}
    form[item.id] = item.value
    console.log(item)
    try {
    
     let response = await this.webService.updateCreatorRate(form,id)
      
      this.appService.openToast('Updated!')
      
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
      this.toggleRateEdit(item)
    }
  }


  toggleRateEdit(item){
    item.edit = !item.edit
    }

    
  setComponentData(){
    this.tab_menu_list = [
      {
        'id' : 'rate',
        'label' : 'Rate Cart',
        'class' : null,
      },
      {
        'id' : 'contract',
        'label' : 'Social Metric',
        
        'class' : null,
      },
   
      

    
    
     
     
    ]
    this.tab_menu_list_campaign = [
      {
        'id' : 'up_coming_campaign',
        'label' : 'Up Coming',
        'count' : 10,
        'class' : null,
      },
      {
        'id' : 'Up Comming Campaign',
        'label' : 'Past Campaign',
        'count' : 10,
        'class' : null,
      },
     
    

    
    
     
     
    ]
    
  }


  _navigateToTabView(tab_view) {
    this.router.navigate(
      [],
      { queryParams: { tab_view: tab_view},
      queryParamsHandling: 'merge' }
    );
  }

  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }

  toggleColumnsMenu(){
    this.show_columns = !this.show_columns
  }

  _navigateTo(page) {
    this.router.navigate(
      [], { queryParams: { page_view: page} }
    );
  }

  changeColumnsView(item) {
    this.router.navigate(
      [],
      { queryParams: { columns_view: item.id},
      queryParamsHandling: 'merge' }
    );
    this.selected_columns = item
    this.toggleColumnsMenu()
   
  }


}
