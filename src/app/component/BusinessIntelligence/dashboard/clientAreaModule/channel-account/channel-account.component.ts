import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faCoins, faUsers, faDownload, faTrash, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
@Component({
  selector: 'app-channel-account',
  templateUrl: './channel-account.component.html',
  styleUrls: ['./channel-account.component.scss']
})
export class ChannelAccountComponent implements OnInit {
  faTrashAlt=faTrashAlt

  faUsers =faUsers
  faDownload= faDownload
  faCoins=faCoins
  faChevronDown = faChevronDown
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];
  contract_status: { id: string; label: string; class: string; }[];
  booking_status: { id: string; label: string; class: string; }[];
  tab_view: string;
  tab_menu_list: { id: string; label: string; count: number; class: any; }[];
  tab_menu_list_campaign: { id: string; label: string; count: number; class: any; }[];
  vendor_id: string;
  vendor: any;
  category: any;
  category_option: any[];

  productForm = new FormGroup({
    name: new FormControl(null, {
      validators: [Validators.required, Validators.nullValidator]
    }),
    type: new FormControl('channel'),
    vendor_id: new FormControl(null),

    category_id: new FormControl(null),

    price: new FormControl(null),


    stock: new FormControl(null),

  })
  client_session: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

   
  ngOnInit(): void {
    this.client_session = this.webService.getClientSession()

    this.route.queryParamMap.subscribe(queryParams => {
      this.tab_view = queryParams.get("tab_view")
    })

    this.getVendorProductCategory()
    
    this.route.paramMap.subscribe(params=>{
      this.vendor_id = params.get("id")

      this.getVendorById(this.vendor_id)
    
    })
    
    this.setComponentData()
  }



  async searchCategory(e) {
    this.category_option = []
    this.category.forEach(item => {
      this.category_option.push({
        'id': 'category_id',
        'label': item.name,
        'value': item.id
      })
    });


  }

  async getVendorProductCategory() {
    try {
      this.appService.showLoading()
      let res = await this.webService.getVendorProductCategory('channel','')
      this.category = res.data
      console.log(res)

    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.appService.hideLoading()
    }
  }

  async updateProduct(e, id) {

    let form = {}
    form[e.id] = e.value
    console.log(form)
    try {

      let res = await this.webService.updateVendorProduct(form, id)

      if (res.data) {
        this.appService.openToast('Updated!')
      }
    } catch (error) {
      this.appService.openToast(error)
    } finally {


    }
  }
  async getVendorById(id){
    try {
      this.appService.showLoading()
      let res = await this.webService.getVendorById(id)
      this.vendor = res.data


     

      if(this.client_session.vendor_id == this.vendor.id){
        if(this.vendor.auth.password !== this.client_session?.password){
          this.router.navigate(
            ['/login']
          );
        }
      }else{
        this.router.navigate(
          ['/login']
        );
      }
      
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }



  setComponentData(){
    this.tab_menu_list = [
      {
        'id' : 'activity',
        'label' : 'Product / Services',
        'count' : 10,
        'class' : null,
      },
      
    
    
    
     
     
    ]
    this.tab_menu_list_campaign = [
      {
        'id' : 'active_campaign',
        'label' : 'Active Campaign',
        'count' : 10,
        'class' : null,
      },
      {
        'id' : 'participate_campaign',
        'label' : 'Participated Campaign',
        'count' : 10,
        'class' : null,
      },
     
    

    
    
     
     
    ]
    
  }


  async deleteVendorProduct(item) {

    try {
      this.appService.showLoading()
      let res = await this.webService.deleteVendorProduct(item.id)
      if (res.data) {
        let index = this.vendor.product.indexOf(item)
        this.vendor.product.splice(index, 1)
        this.appService.openToast('updated!')
      }

      console.log(res)

    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.appService.hideLoading()
    }
  }

  async updateVendor(e, id) {

    let form = {}
    form[e.id] = e.value
    console.log(form)
    try {

      let res = await this.webService.updateVendor(form, id)
      if (res.data) {
        this.appService.openToast('Updated!')
      }

    } catch (error) {
      this.appService.openToast(error)
    } finally {


    }
  }


  async addVendorProduct() {

    this.productForm.get('vendor_id').setValue(this.vendor_id)
    console.log(this.productForm.value)
    try {

      let res = await this.webService.addVendorProduct(this.productForm.value)
      if (res.data) {
        res.data.isNew = true
        this.vendor.product_count = this.vendor.product_count + 1
        this.vendor.product.unshift(res.data)
        this.appService.openToast('New Product Added!')
      }



    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.productForm.reset()
      this.closeModal()
    }


  }

  _navigateToTabView(tab_view) {
    this.router.navigate(
      [],
      { queryParams: { tab_view: tab_view},
      queryParamsHandling: 'merge' }
    );
  }

  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }

  toggleColumnsMenu(){
    this.show_columns = !this.show_columns
  }

  _navigateTo(page) {
    this.router.navigate(
      ['/creator/contract/view/'+page]
    );
  }

  changeColumnsView(item) {
    this.router.navigate(
      [],
      { queryParams: { columns_view: item.id},
      queryParamsHandling: 'merge' }
    );
    this.selected_columns = item
    this.toggleColumnsMenu()
   
  }

}
