import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faCoins, faUsers, faDownload, faExpandAlt, faTrash } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-management-account',
  templateUrl: './management-account.component.html',
  styleUrls: ['./management-account.component.scss']
})



export class ManagementAccountComponent implements OnInit {
  faExpandAlt=faExpandAlt
  faUsers =faUsers
  faTrash=faTrash
  faDownload= faDownload
  faCoins=faCoins
  faChevronDown = faChevronDown
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];
  contract_status: { id: string; label: string; class: string; }[];
  booking_status: { id: string; label: string; class: string; }[];
  tab_view: string;

  tab_menu_list_campaign: { id: string; label: string; count: number; class: any; }[];
  tab_menu_list: { id: string; label: string; class: any; }[];
  creator: any;
  creator_id: string;
  platform_list = [
    {
      'id':'platform_name',
      'label': 'instagram',
      'value': 'instagram'
    },
    {
      'id':'platform_name',
      'label': 'tiktok',
      'value': 'tiktok'
    }
  ]

  creator_rate = {
    'platform_name' : null,
    'rate_name' : null,
    'rate_price' : null,
    'creator_id' : null,
  }

  selected_contract: any;
  client_session: any;
  management: any;
  selected_creator: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

   
  ngOnInit(): void {
    
    this.client_session = this.webService.getClientSession()
    console.log(this.client_session)
    this.route.queryParamMap.subscribe(queryParams => {
      this.tab_view = queryParams.get("tab_view")
    })

    this.route.paramMap.subscribe(params=>{
      this.creator_id = params.get("id")
      this.getCreatorById(this.creator_id)
    })
    
    this.setComponentData()
    
  
  }

  viewRate(item){
    this.selected_creator = item
    this.openModal('rateModal')
  }


  async updateManagement(e){
    console.log(e)

    let form = {}
    form[e.id] = e.value

    try {
      this.appService.showLoading()
      let res = await this.webService.updateManagement(form,this.management.id)
      console.log(res)
      if(res.data){
        this.appService.openToast('Updated!')
        this.ngOnInit()
      }

     
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }

  }

  async getCreatorById(id){
    
    
    try {
    
      let response = await this.webService.getManagementById(id)
      this.management = response.data
      // this.creator.persona = JSON.parse(this.creator.persona)
     console.log(this.management)
      
     this.management.creator.forEach(creator => {
      //creator.performance = this._getPerformance(creator.performance_score)
      creator.persona = JSON.parse(creator.persona)
    });

      if(this.client_session.creator_management_id == this.management.id){
       
       console.log('1')
        if(this.management.auth.password !== this.client_session?.password){
          this.router.navigate(
            ['/client/login']
          );
        }
      }else{
       console.log('2')

        this.router.navigate(
          ['/client/login']
        );
      }

      console.log(this.creator)
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      
    }
  }


  async deleteCreatorRate(item){
    // console.log(item)
    
      try {
      
        let response = await this.webService.deleteCreatorRate(item.id)
        console.log(response)
        this.appService.openToast('Ratecard Removed!')
      
      } catch (e) {
        
        this.appService.openToast(e)
        
        console.log(e)
      } finally {
        let index = this.selected_creator.rate.indexOf(item)
        this.selected_creator.rate.splice(index, 1)
      
      }
    }
  



    async addNewRateCart(){


  
      try {
          this.creator_rate.creator_id = this.selected_creator.id
        let response = await this.webService.addCreatorRate(this.creator_rate,this.selected_creator.id)
        console.log(response.data)
        this.appService.openToast('New Rate Added')
        this.selected_creator.rate.unshift(response.data)
      
      } catch (e) {
        
        this.appService.openToast(e)
        
        console.log(e)
      } finally {
        //this.closeMiniModal()
      }
    }
  
  

  getPlaceholderName(item) {
    if(item.first_name && item.last_name){
      var first_name =item.first_name.charAt(0);
      var last_name =item.last_name.charAt(0);
      return first_name + last_name
    }
  
  }



  viewContract(item){
    this.selected_contract = item.contract
    this.openModal('contractModal')
  }
  

  async updateRate(item,id){
    let form = {}
    form[item.id] = item.value
    console.log(item)
    try {
    
     let response = await this.webService.updateCreatorRate(form,id)
      
      this.appService.openToast('Updated!')
      
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
      this.toggleRateEdit(item)
    }
  }


  toggleRateEdit(item){
    item.edit = !item.edit
    }

    
  setComponentData(){
    this.tab_menu_list = [
      {
        'id' : 'rate',
        'label' : 'Rate Cart',
        'class' : null,
      },
      {
        'id' : 'contract',
        'label' : 'Social Metric',
        
        'class' : null,
      },
   
      

    
    
     
     
    ]
    this.tab_menu_list_campaign = [
      {
        'id' : 'up_coming_campaign',
        'label' : 'Up Coming',
        'count' : 10,
        'class' : null,
      },
      {
        'id' : 'Up Comming Campaign',
        'label' : 'Past Campaign',
        'count' : 10,
        'class' : null,
      },
     
    

    
    
     
     
    ]
    
  }


  _navigateToTabView(tab_view) {
    this.router.navigate(
      [],
      { queryParams: { tab_view: tab_view},
      queryParamsHandling: 'merge' }
    );
  }

  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }

  toggleColumnsMenu(){
    this.show_columns = !this.show_columns
  }

  _navigateTo(page) {
    this.router.navigate(
      ['/creator/contract/view/'+page]
    );
  }

  changeColumnsView(item) {
    this.router.navigate(
      [],
      { queryParams: { columns_view: item.id},
      queryParamsHandling: 'merge' }
    );
    this.selected_columns = item
    this.toggleColumnsMenu()
   
  }


}
