import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faBookmark, faThumbsDown, faThumbsUp, faComment, faComments, faHeart } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';


@Component({
  selector: 'app-newsfeed-backend',
  templateUrl: './newsfeed-backend.component.html',
  styleUrls: ['./newsfeed-backend.component.scss']
})
export class NewsfeedBackendComponent implements OnInit {
  faChevronLeft=faChevronLeft
  faThumbsUp =faThumbsUp
  faThumbsDown = faThumbsDown
  faBookmark = faBookmark
  faComments=faComments
  faHeart=faHeart

  postForm = new FormGroup({
    title: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    description: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    status: new FormControl('draft'),


    

    account_id: new FormControl(null),

    


    
  })
  faChevronDown = faChevronDown
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus

  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];
  contract_status: { id: string; label: string; class: string; }[];
  booking_status: { id: string; label: string; class: string; }[];
  tab_view: string;
  
  post: any;
  params: any;
  category: Promise<any>;
  post_status: { id: string; label: string; value: string; }[];
  keyword: any;
  account: any;
  contact: any;
  tab_menu_list: { id: string; label: string; name: string; }[];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    private serializer: UrlSerializer,

  ) { }

   
  ngOnInit(): void {
   
    this.setComponentData()

    this.route.queryParamMap.subscribe(queryParams => {
      this.params = this.serializer.serialize(this.router.createUrlTree([''],
        { queryParams: this.route.snapshot.queryParams}))      

        
      this.tab_view = queryParams.get("tab_view")
      if (this.tab_view == 'post') {
        this.getPost()
      } else if (this.tab_view == 'category' || this.tab_view == 'brand' ) {
         this.getPostCategory()
      } else if (this.tab_view == 'keyword_filtering') {
         this.getPostKeywordFilter()
      }
      else if (this.tab_view == 'account') {
        this.getAccount()
     }
     else if (this.tab_view == 'contact') {
      this.getNewsfeedContact()
   }

    })

    
   
  }


  setComponentData(){

    this.post_status = [
      {
        'id' : 'status',
        'label' : 'draft',
        'value' : 'draft',

      },
      {
        'id' : 'status',
        'label' : 'publish',
        'value' : 'publish',
     
      },
      {
        'id' : 'status',
        'label' : 'archive',
        'value' : 'archive',
     
      },
   

      
    ]
    
    this.tab_menu_list = [
      {
        'id' : 'post',
        'label' : 'Post',
        'name' : 'Post',
     
      },
      {
        'id' : 'category',
        'label' : 'Category',
        'name' : 'Category',
       
      },
      {
        'id' : 'brand',
        'label' : 'Brand',
        'name' : 'Brand',
       
      },
      {
        'id' : 'keyword_filtering',
        'label' : 'Keyword Filtering',
        'name' : 'Keyword Filtering',
       
      },
      {
        'id' : 'account',
        'label' : 'Account',
        'name' : 'Account',
       
      },
      {
        'id' : 'contact',
        'label' : 'Contact',
        'name' : 'Contact',
       
      },
     
    ]
    
  }



  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }



  async getPost(){
      
    console.log('getpost')
    try {
      this.appService.showLoading()
      let response = await this.webService.getPost(this.params)  
    
      this.post = response.data
    
      console.log(this.post)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
      this.appService.hideLoading()
    }
  }


  async getPostCategory(){
    console.log('get cat')
   
   try {
     this.appService.showLoading()
     let response = await this.webService.getPostCategory(this.tab_view,this.params)  
   
     this.category = response.data
   
     console.log(this.category)
   } catch (e) {
     
     this.appService.openToast(e)
     
     console.log(e)
   } finally {
   
     this.appService.hideLoading()
   }
 }

 async getPostKeywordFilter(){
      
  try {
    this.appService.showLoading()
    let response = await this.webService.getPostKeywordFilter(this.params)  
  
    this.keyword = response.data
  
    console.log(this.keyword) 
  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
  
    this.appService.hideLoading()
  }
}


async getAccount(){
      
  console.log('account')
  try {
    this.appService.showLoading()
    let response = await this.webService.getNewsfeedAccount(this.params)  
  
    this.account = response.data
  
    console.log(this.account) 
  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
  
    this.appService.hideLoading()
  }
}

async getNewsfeedContact(){
      
  console.log('account')
  try {
    this.appService.showLoading()
    let response = await this.webService.getNewsfeedContact(this.params)  
  
    this.contact = response.data
  
    console.log(this.contact) 
  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
  
    this.appService.hideLoading()
  }
}

  
async addPost(){
  this.post = null
  this.postForm.get('account_id').setValue(this.webService.account_id)
  
  try {
  
    let response = await this.webService.addPost(this.postForm.value)

  this.router.navigate(
    ['/client/newsfeed/view/'+response.data.id]
  );
  
  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
    this.postForm.reset()
  }

}


}
