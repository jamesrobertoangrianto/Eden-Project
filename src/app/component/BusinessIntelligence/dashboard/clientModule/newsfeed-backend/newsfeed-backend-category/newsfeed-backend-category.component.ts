import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faTrash } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-newsfeed-backend-category',
  templateUrl: './newsfeed-backend-category.component.html',
  styleUrls: ['./newsfeed-backend-category.component.scss']
})
export class NewsfeedBackendCategoryComponent implements OnInit {
  faTrash=faTrash
  params: string;
  tab_view: string;
  @Input() category: any
  @Output() onChange = new EventEmitter()


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    private serializer: UrlSerializer,
  ) { }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(queryParams => {
      
      this.tab_view = queryParams.get("tab_view")

      })
   
  }



  removePostCategory(item){
    console.log(item)
  }
  async addPostCategory(){
      let form = {
        'type' : this.tab_view
      }
    
    try {
      this.appService.showLoading()
      let response = await this.webService.addPostCategory(form)  
    this.category.unshift(response.data)
    //  this.category = response.data
    this.appService.openToast('Added!')
    
      console.log(response)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
      this.appService.hideLoading()
    }
  }


  async deletePostCategory(id){
   // let form = {}
  
  try {
    this.appService.showLoading()
    let response = await this.webService.deletePostCategory(id)  
    if(response.data){
      this.appService.openToast('Deleted!')
    }
  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
    
    this.appService.hideLoading()
    this.onChange.emit(true)
  }
}

  async updatePostCategory(id,item){
    console.log(id)
    console.log(item)
    let form = {
      
    }
    form[item.id]=item.value
  
  try {
    this.appService.showLoading()
   let response = await this.webService.updatePostCategory(id,form)  
    if(response.data){
      this.appService.openToast('Updated!')
    }
  
   console.log(response)
  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
  
    this.appService.hideLoading()
  }
}


}
