import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faTrash } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-newsfeed-backend-filtering',
  templateUrl: './newsfeed-backend-filtering.component.html',
  styleUrls: ['./newsfeed-backend-filtering.component.scss']
})
export class NewsfeedBackendFilteringComponent implements OnInit {
  params: string;
  faTrash=faTrash

  @Input() keyword: any
  @Output() onChange = new EventEmitter()


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    private serializer: UrlSerializer,
  ) { }

  ngOnInit(): void {
    // this.route.queryParamMap.subscribe(queryParams => {
      
    
    //   this.params = this.serializer.serialize(this.router.createUrlTree([''],
    //   { queryParams: this.route.snapshot.queryParams}))      
    //     this.getPostKeywordFilter()
    //   })
  }
  





  async addPostKeywordFilter(){
      let form={}
    try {
      this.appService.showLoading()
      let response = await this.webService.addPostKeywordFilter(form)  
     if(response.data){
      this.keyword.unshift(response.data)
     }
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
      this.appService.hideLoading()
      this.ngOnInit()
    }
  }

  async updateKeywordFilter(id,e){
   

    let form = {
      
    }
    form[e.id]=e.value

  try {
    this.appService.showLoading()
   let response = await this.webService.updateKeywordFilter(id,form)  
    if(response.data){
      this.appService.openToast('Updated!')
    }
  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
  
    this.appService.hideLoading()
    this.ngOnInit()
  }
}


async deleteKeywordFilter(id){
   


try {
  this.appService.showLoading()
 let response = await this.webService.deleteKeywordFilter(id)  
  
} catch (e) {
  
  this.appService.openToast(e)
  
  console.log(e)
} finally {
  this.onChange.emit(true)
  this.appService.hideLoading()
  this.ngOnInit()
}
}
}
