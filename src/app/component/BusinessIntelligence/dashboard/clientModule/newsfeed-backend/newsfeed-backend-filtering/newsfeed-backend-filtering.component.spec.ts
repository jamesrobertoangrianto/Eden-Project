import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsfeedBackendFilteringComponent } from './newsfeed-backend-filtering.component';

describe('NewsfeedBackendFilteringComponent', () => {
  let component: NewsfeedBackendFilteringComponent;
  let fixture: ComponentFixture<NewsfeedBackendFilteringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsfeedBackendFilteringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsfeedBackendFilteringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
