import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faBookmark, faThumbsDown, faThumbsUp, faComment, faComments, faHeart } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';


@Component({
  selector: 'app-newsfeed-backend-post',
  templateUrl: './newsfeed-backend-post.component.html',
  styleUrls: ['./newsfeed-backend-post.component.scss']
})
export class NewsfeedBackendPostComponent implements OnInit {
  faChevronLeft=faChevronLeft
  faThumbsUp =faThumbsUp
  faThumbsDown = faThumbsDown
  faBookmark = faBookmark
  faComments=faComments
  faHeart=faHeart
  @Input() post: any
  @Output() onChange = new EventEmitter()
  postForm = new FormGroup({
    title: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    description: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    status: new FormControl('draft'),


    

    account_id: new FormControl(null),

    


    
  })
  show_columns: boolean;
  showModal: null;
  params: string;
  post_status: { id: string; label: string; value: string; }[];
  tab_view: string;
  sortListItems: { name: string; items: any; }[];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    private serializer: UrlSerializer,

  ) { }

  ngOnInit(): void {
    this.setComponentData()
    

  }


  setComponentData(){

    this.sortListItems = [
      {
       
        'name' : 'status',
        'items' : [
          {
           
            'name' :'draft',

            'label' :'draft',

          },
          {
           
            'name' :'publish',
            'label' :'publish',

          },
          {
           
            'name' :'archive',
            'label' :'archive',


          }
        ]

      }
    ]

    
    this.post_status = [
      {
        'id' : 'status',
        'label' : 'draft',
        'value' : 'draft',

      },
      {
        'id' : 'status',
        'label' : 'publish',
        'value' : 'publish',
     
      },
      {
        'id' : 'status',
        'label' : 'archive',
        'value' : 'archive',
     
      },
   

      
    ]
    

  }


    

  async updatePost(id,e){
    console.log(e)
    let form ={}
    form[e.id]=e.value
  
    try {
     
      let response = await this.webService.updatePost(id,form)  
    if(response.data){
      this.appService.openToast('Updated!')
      this.onChange.emit(true)
    }
     console.log(response.data)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
     
    }
  
  
  }

  async updateFeatured(id, e){
    console.log(e)
    let form ={}
    form['is_featured']= !e.is_featured
  
    try {
     
      let response = await this.webService.updatePost(id,form)  
    if(response.data){
      this.appService.openToast('Updated!')
    }
    console.log(response.data)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
     
    }
  
  
  }
  



  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }

  toggleColumnsMenu(){
    this.show_columns = !this.show_columns
  }

  _navigateTo(page) {
    this.router.navigate(
      ['/creator/contract/view/'+page]
    );
  }


}
