import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsfeedBackendPostComponent } from './newsfeed-backend-post.component';

describe('NewsfeedBackendPostComponent', () => {
  let component: NewsfeedBackendPostComponent;
  let fixture: ComponentFixture<NewsfeedBackendPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsfeedBackendPostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsfeedBackendPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
