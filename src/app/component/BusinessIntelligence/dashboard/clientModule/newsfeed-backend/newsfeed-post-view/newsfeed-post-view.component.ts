import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faThumbsUp, faThumbsDown, faMarker, faBookmark, faEllipsisV, faHeart } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-newsfeed-post-view',
  templateUrl: './newsfeed-post-view.component.html',
  styleUrls: ['./newsfeed-post-view.component.scss']
})
export class NewsfeedPostViewComponent implements OnInit {
  faChevronLeft=faChevronLeft
  faThumbsUp =faThumbsUp
  faThumbsDown = faThumbsDown
  faBookmark = faBookmark
  faHeart=faHeart

  faEllipsisV=faEllipsisV

  post_id: string;
  post: any;
  category: Promise<any>;
  showModal: any;
  brand: any;
  term: any;
  termTitle: any;
  term_id: any;
  isShowoption: boolean;
  isShowOption: boolean;
  term_name: any;
  discussion_message: any;
  post_status: { id: string; label: string; value: string; }[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params=>{
      this.post_id = params.get("id")
      this.getPostById(this.post_id)

    

    })

    this.post_status = [
      {
        'id' : 'status',
        'label' : 'draft',
        'value' : 'draft',

      },
      {
        'id' : 'status',
        'label' : 'publish',
        'value' : 'publish',
     
      },
      {
        'id' : 'status',
        'label' : 'archive',
        'value' : 'archive',
     
      },
   

      
    ]

  }
   addTerm(id){
    this.term_name = null
    this.term = null
    this.term_id = id
    this.isShowOption = false
    this.searchTerm('')
    this.openModal('termModal')

  }

  
 
  async searchTerm(e){
    console.log(e)
    var search = '?search='+e
    let response = await this.webService.getPostCategory(this.term_id, search)  
    this.isShowOption = false
    if(e && response.data?.length == 0){
      this.isShowOption = true
      this.term_name = e
      
    }
    
    this.term = response.data
    this.term.forEach(item => {
      item.isAdded = this._checkTerm(item.id)
    });


  }

  _checkTerm(id){

    var array = this.post[this.term_id]

    if(array.some(item => item.newsfeed_category_id === id)){
      return true
      } else{
         return false
      }
     
   
  }
  
   async selectTerm(term_id,item){
    
    let form = {
      'newsfeed_post_id' : this.post_id,
      'newsfeed_category_id' : item.id,
      'type' : term_id
    }


    try {
    
      let response = await this.webService.addPostTerm(form)
    if(response.data){
      item.isAdded = true
    }
     // this.post = response.data
     //this.appService.openToast('Added!')
    
     
      console.log(response.data)
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.ngOnInit()
     // this.closeModal()
    
    }
  }

  async removeTerm(term_id,item){
   

    try {
    
      let response = await this.webService.removePostTerm(item.id)
    
      if(response.data){
        item.isAdded = false
      }

      console.log(response.data)
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.ngOnInit()
     // this.closeModal()
    
    }
  }
  async getPostById(id){
    
    
    try {
    
      let response = await this.webService.getPostById(id)
    
      this.post = response.data
    
      console.log(this.post)
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
    }
  }

  async addDiscussion(){
    let form={
      'message' : this.discussion_message,
      'account_id' : this.webService.account_id,
      'newsfeed_post_id' : this.post_id
    }
    try {
      this.appService.showLoading()
      let response = await this.webService.addPostDiscussion(form)  
      console.log(response)
      this.discussion_message = null
     
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {

      this.appService.hideLoading()
      this.ngOnInit()
    }
  }
  async addPostTerm(term_id,name){
    let form = {
      'type' : term_id,
      'name' : name
    }
  
  try {
    this.appService.showLoading()
    let response = await this.webService.addPostCategory(form)  
  

    this.selectTerm(term_id,response.data)
    
    setTimeout(() => {
      this.searchTerm(name)
      this.appService.hideLoading()
    }, 300);


   this.isShowOption =false
   
  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
  
   
  }

 
}


async addReaction(id){
  console.log(id)

  let form ={
    'name' : id,
    'newsfeed_post_id' : this.post_id
    
  }

  try {
   
    let response = await this.webService.addReaction(form)  
  
   console.log(response.data)
  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
  
   
  }
}

 async updatePost(e){
  console.log(e)
  let form ={}
  form[e.id]=e.value

  try {
   
    let response = await this.webService.updatePost(this.post_id,form)  
  if(response.data){
    this.appService.openToast('Updated!')
  }
   console.log(response.data)
  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
  
   
  }


}

  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }


}
