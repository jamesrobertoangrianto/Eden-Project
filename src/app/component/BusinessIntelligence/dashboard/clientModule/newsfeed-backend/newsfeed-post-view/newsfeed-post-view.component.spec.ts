import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsfeedPostViewComponent } from './newsfeed-post-view.component';

describe('NewsfeedPostViewComponent', () => {
  let component: NewsfeedPostViewComponent;
  let fixture: ComponentFixture<NewsfeedPostViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsfeedPostViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsfeedPostViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
