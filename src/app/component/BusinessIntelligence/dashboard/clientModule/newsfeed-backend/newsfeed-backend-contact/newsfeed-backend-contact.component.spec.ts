import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsfeedBackendContactComponent } from './newsfeed-backend-contact.component';

describe('NewsfeedBackendContactComponent', () => {
  let component: NewsfeedBackendContactComponent;
  let fixture: ComponentFixture<NewsfeedBackendContactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsfeedBackendContactComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsfeedBackendContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
