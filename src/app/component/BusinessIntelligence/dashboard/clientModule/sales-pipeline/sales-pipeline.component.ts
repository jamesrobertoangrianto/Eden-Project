import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faExpandArrowsAlt, faExpand, faExpandAlt } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-sales-pipeline',
  templateUrl: './sales-pipeline.component.html',
  styleUrls: ['./sales-pipeline.component.scss']
})
export class SalesPipelineComponent implements OnInit {
  faExpandArrowsAlt=faExpandAlt
  faChevronDown = faChevronDown
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];

  clientForm = new FormGroup({
    aquisition_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    client_id: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    deal_stage: new FormControl('initiate_contact'),
    owner_id: new FormControl(null),

    

    


    
  })
  contract_status: { id: string; label: string; value: string; }[];
  client_list: any[];
  aquisition: any;
  overview: { label: string; value: any; }[];
  params: string;
  total_aquisition_value: any;
  client_filter_list: any[];
  page: number;
  sortListItems: ({ name: string; key: string; type: string; items: any[]; } | { name: string; items: { label: string; name: string; }[]; key?: undefined; type?: undefined; })[];



  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    private serializer: UrlSerializer,
  ) { }

  ngOnInit(): void {
    this.getClient()
   
    this.route.queryParamMap.subscribe(queryParams => {
     
      this.params = this.serializer.serialize(this.router.createUrlTree([''],
      { queryParams: this.route.snapshot.queryParams}))      
      this.getAquisition()
    })
   
    this.sortListItems = [
      {
       
        'name' : 'Periode Start',
        'key' : 'periode',
        'type' : 'date',
        'items' : [   
        ]

      },
      {
       
        'name' : 'Periode End',
        'key' : 'periode_end',
        'type' : 'date',
        'items' : [   
        ]

      },
     
      {
       
        'name' : 'stages',
        'items' : [
          {
            
            'label' : 'Initiate Contact',
            'name' : 'initiate_contact',
          },
          {
          
            'label' : 'Introductory Meeting',
            'name' : 'introductory_meeting',
          },
          {
           
            'label' : 'Proposal Sent',
            'name' : 'proposal_sent',
          },
          {
           
            'label' : 'Contract Sent',
            'name' : 'contract_sent',
          },
          {
           
            'label' : 'Won',
            'name' : 'won',
          },
    
          {
          
            'label' : 'Lost',
            'name' : 'lost',
          },
        
        
          
        ]

      },
      
    ]

    this.contract_status = [
      {
        'id' : 'deal_stage',
        'label' : 'Initiate Contact',
        'value' : 'initiate_contact',
      },
      {
        'id' : 'deal_stage',
        'label' : 'Introductory Meeting',
        'value' : 'introductory_meeting',
      },
      {
        'id' : 'deal_stage',
        'label' : 'Proposal Sent',
        'value' : 'proposal_sent',
      },
      {
        'id' : 'deal_stage',
        'label' : 'Contract Sent',
        'value' : 'contract_sent',
      },
      {
        'id' : 'deal_stage',
        'label' : 'Won',
        'value' : 'won',
      },

      {
        'id' : 'deal_stage',
        'label' : 'Lost',
        'value' : 'lost',
      },
    
    
    
    ]

  }


  async getClient(){
    this.client_filter_list = []
   
    
    try {
      this.appService.showLoading()
      let response = await this.webService.getClient('brand')
 

      this.client_filter_list = response.data.map(item => {
        return {
          id: item.id,
          label: item.client_name,
          name: item.client_name
        };
      });

      this.sortListItems.push(
        {
       
          'name' : 'client',
          'items' : this.client_filter_list
        },
      )




    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
      this.appService.hideLoading()
    }
  }

  selectClient(client){
    //console.log(client)
    this.clientForm.get('client_id').setValue(client.value)
  }
  async searchClient(e){
    this.client_list = []
    try {
      let response = await this.webService.getClient('brand')
      

      if(response.data){
        this.client_list = response.data.map(client => {
          return {
            name: 'parrent_id',
            label: client.client_name,
            value: client.id
          };
        });
      
      }

    
     
    } catch (error) {
      
    }
  }


  async getAquisition(){
      
      this.page = 1
    try {
      this.appService.showLoading()
      let response = await this.webService.getAquisition(this.params)

      console.log(response.data)
        this.aquisition = response.data
          this.total_aquisition_value = 0
      
        this.aquisition.forEach(item => {
          this.total_aquisition_value += parseFloat(item.budget?item.budget:0)
        });
      
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.hideLoading()
  
    }
  }


  async loadMore() {
    console.log('load')
    this.page = this.page+1
    this.params = '?/'+this.params+'&page='+this.page
   
    
    try {
      this.appService.showLoading()
    


      let res = await this.webService.getAquisition(this.params)

      console.log(res)
    if(res.data){
    
      res.data.forEach(item => {
        this.total_aquisition_value += parseFloat(item.budget?item.budget:0)
      });

      this.aquisition = this.aquisition.concat(res.data)

    }
     
     
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.hideLoading()
  
    }
  }
  
  async addAquisition(){
    this.clientForm.get('owner_id').setValue(this.webService.account_id)
    console.log(this.clientForm.value)
    try {
      this.appService.showLoading()
        let response = await this.webService.addAquisition(this.clientForm.value)
      
      this._navigateTo(response.data.id)
      } catch (e) {
        
        this.appService.openToast(e)
        
        console.log(e)
      } finally {
        this.appService.hideLoading()
      
        this.closeModal()
    
      }
  }



  async updateAquisition(item,id){
    let form = {}
    form[item.id] = item.value
  
      try {
        let response = await this.webService.updateAquisition(id,form)
      
       console.log(response)
       this.ngOnInit()
      } catch (error) {

      }
    }

    

  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }



  _navigateTo(page) {
    this.router.navigate(
      ['/client/aquisition/view/'+page]
    );
  }

  

}
