import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesPipelineViewComponent } from './sales-pipeline-view.component';

describe('SalesPipelineViewComponent', () => {
  let component: SalesPipelineViewComponent;
  let fixture: ComponentFixture<SalesPipelineViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesPipelineViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesPipelineViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
