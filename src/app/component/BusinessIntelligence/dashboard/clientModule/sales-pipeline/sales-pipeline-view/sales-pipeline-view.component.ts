import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faAddressBook, faCalendar, faCalendarAlt, faChevronDown, faChevronLeft, faEdit, faMapMarked, faMapMarker, faUserEdit, faUserPlus } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
@Component({
  selector: 'app-sales-pipeline-view',
  templateUrl: './sales-pipeline-view.component.html',
  styleUrls: ['./sales-pipeline-view.component.scss']
})
export class SalesPipelineViewComponent implements OnInit {
  faAddressBook=faAddressBook
  faChevronDown = faChevronDown
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  showModal: any;
  insight: { title: string; Description: string; link: string; fn: string; icon: string; }[];
  tab_view: string;
  overview: { label: string; value: string; }[];
  aquisition: any;
  aquisition_id: string;
  contract_status: { id: string; label: string; value: string; }[];



  contactForm = new FormGroup({
    
    name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    mobile: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    work: new FormControl(null),
    position: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    client_id: new FormControl(null),
  
  })



  scopeForm = new FormGroup({
    
    name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    description: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
 
  
    client_aquisition_id: new FormControl(null),
  
  })

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

 
  ngOnInit(): void {
   
    this.route.paramMap.subscribe(params=>{
      this.aquisition_id = params.get("id")
      this.getAquisitionById(this.aquisition_id)
     
    })

    this.contract_status = [
      {
        'id' : 'deal_stage',
        'label' : 'initiate_contact',
        'value' : 'initiate_contact',
      },
      {
        'id' : 'deal_stage',
        'label' : 'introductori_meeting',
        'value' : 'introductori_meeting',
      },
      {
        'id' : 'deal_stage',
        'label' : 'proposal_sent',
        'value' : 'proposal_sent',
      },
      {
        'id' : 'deal_stage',
        'label' : 'contract_sent',
        'value' : 'contract_sent',
      },
      {
        'id' : 'deal_stage',
        'label' : 'won',
        'value' : 'won',
      },

      {
        'id' : 'deal_stage',
        'label' : 'lost',
        'value' : 'lost',
      },
    
    
    
    ]

  }

  _navigateBack(){

    this.router.navigate(
      ['/client/aquisition/'],
      { queryParams: { animation: 'tab_view'},
      queryParamsHandling: 'merge' }
    );
   
  
  }


  async updateAquisition(item,id){
 
    let form = {}
    form[item.id] = item.value
    
    
      try {
        let response = await this.webService.updateAquisition(id,form)
        if(item.id == 'budget'){
          this.aquisition.budget = item.value
        }
        this.appService.openToast('Update')

    
      } catch (error) {
        
      }
    }


  async updateScope(item,id){
    let form = {}
    form[item.id] = item.value
  
    console.log(form)
    console.log(id)
      try {
        let response = await this.webService.updateScope(id,form)
      
       console.log(response)
       
      } catch (error) {
        
      }
    }



  async addContact(){
    this.contactForm.get('client_id').setValue(this.aquisition.client.id)


    try {
    this.appService.showLoading()
    let response = await this.webService.addContact(this.contactForm.value)
      this.aquisition.client.contact.unshift(response.data)
    console.log(response)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.hideLoading()
    
      this.closeModal()

    }
}


async addScope(){
  this.scopeForm.get('client_aquisition_id').setValue(this.aquisition.id)
console.log(this.scopeForm.value)

  try {
  this.appService.showLoading()
  let response = await this.webService.addScope(this.scopeForm.value)
 this.aquisition.scope.unshift(response.data)
  console.log(response)
  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
    this.appService.hideLoading()
    this.scopeForm.reset()
    this.closeModal()

  }
}



  setOverview(){
   if(this.aquisition){
    this.overview = [
      {
        'label' : 'Create Date',
        'value' : this.aquisition.created_at
      },
      {
        'label' : 'Last Update',
        'value' : this.aquisition.updated_at
      },
      {
        'label' : 'Stage',
        'value' : this.aquisition.deal_stage
      },
      {
        'label' : 'SOW',
        'value' : '2 Services'
      }
    ]
   }
  }


async updateContact(id,contact){
   
  
  let form = {}
  form[contact.id] = contact.value

  console.log(form)
try {

 let response = await this.webService.updateContact(id,form)
console.log(response)

} catch (e) {
  
  this.appService.openToast(e)
  
  console.log(e)
} finally {
 this.appService.openToast('Update')

}
}

  async getAquisitionById(id){
      
      
    try {
      this.appService.showLoading()
      let response = await this.webService.getAquisitionById(id)
    this.aquisition = response.data
   console.log(response)
   
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.setOverview()
      this.appService.hideLoading()
    }
  }
  

  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }


}
