import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-news-footer',
  templateUrl: './news-footer.component.html',
  styleUrls: ['./news-footer.component.scss']
})
export class NewsFooterComponent implements OnInit {

  constructor() { }

  top_link =[
    {
      'id':'',
      'label':'About',
      'link':'/newsfeed/about'
    },
    {
      'id':'',
      'label':'Term and Conditions',
      'link':'/newsfeed/term-conditions'
    },
    {
      'id':'',
      'label':'Privacy Policy',
      'link':'/newsfeed/privacy'
    }
  ]
  ngOnInit(): void {
  }

}
