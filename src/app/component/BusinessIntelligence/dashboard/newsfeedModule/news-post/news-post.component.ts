import { Component, Input, OnInit } from '@angular/core';
import { faChevronLeft, faThumbsUp, faThumbsDown, faBookmark, faEllipsisV, faHeart, faShareAlt } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

import { Location } from '@angular/common';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
@Component({
  selector: 'app-news-post',
  templateUrl: './news-post.component.html',
  styleUrls: ['./news-post.component.scss']
})
export class NewsPostComponent implements OnInit {
  @Input() post_id: any
  post: any;

  faEllipsisV=faEllipsisV
  faChevronLeft=faChevronLeft
  faThumbsUp =faThumbsUp
  faShareAlt=faShareAlt
  faHeart=faHeart
  

  faThumbsDown = faThumbsDown
  faBookmark = faBookmark

  discussion_message: any;
  routerSubscription: any;
  isLoading: boolean;
  backLink: string;
  author_account: any;

  constructor(
    private webService : ManualOrderService,
    private appService : BusinessIntelligenceService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute,
    


  ) { }

  ngOnInit(): void {

    this.getPostById(this.post_id)

    this.webService.authorSessionSubject.subscribe(
      (res)=>{
       
         console.log(res)
        if(res){
          this.author_account = res
         
         
          
        }
       
      }
    )

            
  }

  ngOnDestroy(){
    if(this.routerSubscription) this.routerSubscription.unsubscribe()
      this.post = null
  }


  ngAfterViewInit() {
   console.log(this.webService.author_id)

    this.routerSubscription=this.router.events.subscribe(
      (res)=>{
        if(res instanceof NavigationEnd){
          // this.post = null
          // this.getPostById(this.post_id)
         this.isLoading = true
          setTimeout(()=>{     
            
            this.getPostById(this.post_id)
        }, 100);
         
        }
      }
    )



  
    
  }

  async getPostById(id){
    
    
    try {
      this.isLoading = true
    
      let response = await this.webService.getPostById(id)
    
      this.post = response.data
      this.post.is_like = this._checklike(this.post.reaction,'like')
      this.post.is_pinned = this._checklike(this.post.reaction,'pinned')
      console.log(this.post)
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    this.isLoading = false
    }
  }



  share(item){
    item.isShowShare = !item.isShowShare
    console.log('lsls')
  }

  onBlur(event,item){
    if (!event.currentTarget.contains(event.relatedTarget)) {
     this.share(item)
    }
  }

  shareTo(source,item){
   
  switch(source) { 
    case 'whatsapp': { 
      window.location.href='https://api.whatsapp.com/send?text='+window.location.href;
       break; 
    } 

    case 'facebook': { 
      window.location.href='https://www.facebook.com/sharer/sharer.php?u='+window.location.href;
       break; 
    } 

    case 'twitter': { 
      window.location.href='https://twitter.com/intent/tweet?url='+window.location.href;
       break; 
    } 

    
   
    default: { 
       //statements; 
       break; 
    } 
 } }

  async addDiscussion(){
    console.log(this.webService.author_id)

    let form={
      'message' : this.discussion_message,
      'account_id' : this.webService.account_id,
      'newsfeed_account_id' : this.webService.author_id,

      'newsfeed_post_id' : this.post_id
    }
    try {
      this.appService.showLoading()
      let response = await this.webService.addPostDiscussion(form)  
      console.log(response)
      this.discussion_message = null
     
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {

      this.appService.hideLoading()
      this.ngOnInit()
    }
  }

async addReaction(id){
  console.log(this.webService.author_id)

  if(this.webService.author_id){
  this.appService.showLoading()

    let form ={
      'name' : id,
      'newsfeed_account_id' : this.webService.author_id,
      'newsfeed_post_id' : this.post_id
      
    }

  try {
   
    let response = await this.webService.addReaction(form)  
  
   console.log(response.data)
  } catch (e) {
    
    this.appService.openToast(e)
   console.log(e)
    
    console.log(e)
  } finally {
    this.appService.hideLoading()
  this.ngOnInit()
   
  }
  }
  

  else{
    this.appService.openToast('Please login to ' + id)
  }
}


_checklike(reaction,type){
  if(reaction.some(item =>
    item.newsfeed_account_id === this.webService.author_id &&
    item.name === type
  
  )){
    return true
    } else{
       return false
    }
   
 
}

}
