import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsPrivacyComponent } from './news-privacy.component';

describe('NewsPrivacyComponent', () => {
  let component: NewsPrivacyComponent;
  let fixture: ComponentFixture<NewsPrivacyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsPrivacyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsPrivacyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
