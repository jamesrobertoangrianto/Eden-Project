import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faBookmark, faThumbsDown, faThumbsUp, faComment, faComments, faEllipsisH, faEllipsisV, faHeart, faShareAlt } from '@fortawesome/free-solid-svg-icons';
import { title } from 'process';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';



@Component({
  selector: 'app-news-homepage',
  templateUrl: './news-homepage.component.html',
  styleUrls: ['./news-homepage.component.scss']
})
export class NewsHomepageComponent implements OnInit {
  tab_menu_list: {}[];
  tab_view: string;
  params: string;
  post: any;
  faShareAlt=faShareAlt
  faEllipsisV=faEllipsisV
  faChevronLeft=faChevronLeft
  faThumbsUp =faThumbsUp
  faThumbsDown = faThumbsDown
  faBookmark = faBookmark
  faComment = faComments
  faHeart=faHeart
  category: any;
  selected_category: string;
  recommended_post: any;
  category_option: any[];
  postBody: any
  is_post_view: boolean;
  post_id: string;
  isLoading: boolean;
  author_account: any;
  isShowForm: boolean;
  keyword_filter: any;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    private serializer: UrlSerializer,
  ) { }

  _checkKeyword(title,description){

    const str1 = title + description;
    const conditions = ["anjing", "babi"];

    const res = this.keyword_filter.some(item => str1.includes(item.name));
    console.log(res)
    return res
  }

  ngOnInit(): void {
    
    


    this.is_post_view = false
   //this.appService.openToast('fsjhfjhsjhs')
    this.getPostCategory()
    this.getPostKeywordFilter()

    this.route.paramMap.subscribe(params=>{
      this.post_id = params.get("id")
      if(this.post_id){
        this.is_post_view = true
      }
   

    })

    if(!this.is_post_view){
      this.route.queryParamMap.subscribe(queryParams => {
        this.tab_view = queryParams.get("tab_view")
        
        this.selected_category = queryParams.get("category")
       
        this.params = this.serializer.serialize(this.router.createUrlTree([''],
        { queryParams: this.route.snapshot.queryParams}))    
       
  
        if(this.tab_view == 'pinned' || this.tab_view == 'my_post'){
            setTimeout(()=>{                           // <<<---using ()=> syntax
            this.getPost()
          }, 200);  
        }else{
          this.getPost()
          
        }
     
  
       
        })
      
    }

   
      



  
    this.setForm()

    this.webService.authorSessionSubject.subscribe(
      (res)=>{
       
         console.log(res)
        if(res){
          this.author_account = res
          this.tab_menu_list = [
            {
              'id' : 'newest',
              'label' : 'Newest',
          
            },
            {
              'id' : 'trending',
              'label' : 'Trending',
             
            },
            {
              'id' : 'pinned',
              'label' : 'Pinned',
             
            },
            {
              'id' : 'my_post',
              'label' : 'My Post',
             
            },
        
           
          ]
         this.getRecommendedPost()
         
          
        }
        else{
          this.tab_menu_list = [
            {
              'id' : 'newest',
              'label' : 'Newest',
          
            },
            {
              'id' : 'trending',
              'label' : 'Trending',
             
            },
          
           
          ]
         this.getRecommendedPost()

        }
      }
    )
  }

  setForm(){
    this.postBody = {
      'author_id' : null,
      'title' : null,
      'description':null,
      'status' : 'publish',
      'category':[]
    }
  }

  share(item){
    item.isShowShare = !item.isShowShare
    console.log('lsls')
  }

  onBlur(event,item){
    if (!event.currentTarget.contains(event.relatedTarget)) {
     this.share(item)
    }
  }

  shareTo(source,item){
   
  switch(source) { 
    case 'whatsapp': { 
      window.location.href='https://api.whatsapp.com/send?text='+window.location.href;
       break; 
    } 

    case 'facebook': { 
      window.location.href='https://www.facebook.com/sharer/sharer.php?u='+window.location.href;
       break; 
    } 

    case 'twitter': { 
      window.location.href='https://twitter.com/intent/tweet?url='+window.location.href;
       break; 
    } 

    
   
    default: { 
       //statements; 
       break; 
    } 
 } }

  async getPost(){
      
    this.isLoading = true
    if(this.tab_view =='pinned'){
     this.params = this.params+'&pinned_id='+this.webService.author_id
    }
    if(this.tab_view =='my_post'){
      this.params = this.params+'&author_id='+this.webService.author_id
     }
    try {
     
      let response = await this.webService.getPost(this.params,'publish')  
    
      this.post = response.data
    
      this.post.forEach(item => {
        item.is_like = this._checklike(item.reaction,'like')
        item.is_pinned = this._checklike(item.reaction,'pinned')
      });
      console.log(this.post)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    this.isLoading = false
    }
  }


  async getRecommendedPost(){
      
    
    try {
     
      let response = await this.webService.getRecommendedPost(this.params)  
    console.log('rec')
      this.recommended_post = response.data
      // this.recommended_post.forEach(item => {
      //   item.is_like = this._checklike(item.reaction,'like')
      //   item.is_pinned = this._checklike(item.reaction,'pinned')
      // });
    
      console.log(this.recommended_post)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
    
    }
  }

  async getPersonalizedPost(){
      
    
    try {
     
      let response = await this.webService.getPost(this.params)  
    
      this.recommended_post = response.data
      // this.recommended_post.forEach(item => {
      //   item.is_like = this._checklike(item.reaction,'like')
      //   item.is_pinned = this._checklike(item.reaction,'pinned')
      // });
    
      console.log(this.post)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
    
    }
  }


  async getPostCategory(){
     
    this.category_option = []
    
    try {
      
      let response = await this.webService.getPostCategory('category')  
    
      this.category = response.data
      this.category.forEach(item => {
        this.category_option.push({
          'id' : item.id,
          'label' : item.name
        })
      });
    
      console.log(this.category)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
   
    }
  }

  async getPostKeywordFilter(){
     
    this.category_option = []
    
    try {
      
      let response = await this.webService.getPostKeywordFilter('')  
      this.keyword_filter = response.data
      console.log('leyword')
    
      console.log(response)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
   
    }
  }


  navigateTo(id){
    console.log(id)
    this.router.navigate(
      ['/newsfeed/post/'+id]
    );
    
  }

  navigateToCat(id){
   

    this.router.navigate(
      [],
      { queryParams: { category: id},
      queryParamsHandling: 'merge' }
    );
    
  }


  selectCategory(e){
    console.log(e)
    this.postBody.category = e
    console.log(this.postBody)
  }
  togglePostForm(){
    this.isShowForm = !this.isShowForm
  }

  async addPost(){
    //this.category_option = []
    this.postBody.author_id = this.webService.author_id

    if(!this.webService.author_id && this.getGuestCount() >= 3){
      this.appService.openToast('Daily post limit exceeded')
    }else{
      if(this._checkKeyword(this.postBody.title,this.postBody.description)){
        this.appService.openToast('Your post contains bad word')

      }else{
        try {
          this.appService.showLoading()
        
          let response = await this.webService.addPost(this.postBody)
    
          if(response.data){
            this.ngOnInit()
            this.togglePostForm()
            this.appService.openToast('Posted!')
            if(!this.webService.author_id){
              this.setGuestCount()
              this.setExpiredDate()
  
            }
    
          }
    
        
        } catch (e) {
          
          this.appService.openToast(e)
          
          console.log(e)
        } finally {
          this.appService.hideLoading()
        }
      }
     
    }
    

  }

  getGuestCount(){
   let is_expired = new Date().getTime().toString() > this.getExpiredDate()
   console.log(is_expired)
   if(is_expired){
    localStorage.setItem('guest_count','0')

    return 0
   }
   else{

    let count = localStorage.getItem('guest_count')
    if(count){
      return parseFloat(count)
    }
    else{
      return 0
    }
   }

    
  }
  setGuestCount(){

  

    let a  = this.getGuestCount() + 1
    console.log('set guest')
    console.log(a)
    localStorage.setItem('guest_count',a.toString())
  }

  setExpiredDate(){

    
    var  day=1* 60 * 60 * 1000
    var expired_date = new Date().getTime() + day
   

    localStorage.setItem('guest_expired_date',expired_date.toString())


  }

  getExpiredDate(){
    var exp =localStorage.getItem('guest_expired_date')
    console.log(exp)
    return exp


  }

async addReaction(id,post_id){
  console.log(id)

  let form ={
   
    'name' : id,
    'newsfeed_account_id' : this.webService.author_id,
    'newsfeed_post_id' : post_id
    
  }

  if(this.webService.author_id){
    
  try {
   
    let response = await this.webService.addReaction(form)  
  
   console.log(response.data)
  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
  
   this.ngOnInit()
  }
  }
  else{
    this.appService.openToast('Please login to ' + id)
  }
  

}

_checklike(reaction,type){
  if(reaction.some(item =>
    item.newsfeed_account_id === this.webService.author_id &&
    item.name === type
  
  )){
    return true
    } else{
       return false
    }
   
 
}

  

}
