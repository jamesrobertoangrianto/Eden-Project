import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsGuidelinesComponent } from './news-guidelines.component';

describe('NewsGuidelinesComponent', () => {
  let component: NewsGuidelinesComponent;
  let fixture: ComponentFixture<NewsGuidelinesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsGuidelinesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsGuidelinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
