import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-news-contact',
  templateUrl: './news-contact.component.html',
  styleUrls: ['./news-contact.component.scss']
})
export class NewsContactComponent implements OnInit {
  contactForm = new FormGroup({
    first_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    email: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    phone: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    message: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),




    


    
  })

  constructor(

    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
      
  ) { }

  ngOnInit(): void {
  }

  async addContact(){
    this.appService.showLoading()
    console.log('oi')
    try {
   
      let response = await this.webService.addNewsfeedContact(this.contactForm.value)  
    
     console.log(response.data)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.openToast('Thankyou for contacting us! Our team will contact you soon')
      this.appService.hideLoading()
      this.contactForm.reset()
    }
    }
  

}
