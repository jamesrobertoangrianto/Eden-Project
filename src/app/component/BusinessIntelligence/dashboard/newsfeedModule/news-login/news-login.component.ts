import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-news-login',
  templateUrl: './news-login.component.html',
  styleUrls: ['./news-login.component.scss']
})
export class NewsLoginComponent implements OnInit {
  faTimes=faTimes
  @Output() afterLogin = new EventEmitter()
  @Output() onClose = new EventEmitter()

  loginForm = new FormGroup({
    email: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    password: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    password_hash: new FormControl(null),
    session_id: new FormControl(null),

    
  })

  registerForm = new FormGroup({
    first_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    email: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    password: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    confirm_password: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    password_hash: new FormControl(null),

    session_id: new FormControl(null),

   
    
    
  })
  showForm: string;
  sessions: any;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    
  ) { }

  ngOnInit(): void {
    this.showForm = 'login'
    this.sessions = this.encript(navigator.userAgent)

  }

  isShowWarning(){
    if(this.registerForm.get("confirm_password").value){
      if(this.registerForm.get('password').value !== this.registerForm.get("confirm_password").value){
        return true
      }
      else{
        return false
      }
    }
    else{
      return false
    }
   

    
  }


  encript(data){
    return data.split("").reduce(function(a, b) {
      a = ((a << 5) - a) + b.charCodeAt(0);
      return a & a;
    }, 0);
  }

  addNewAccount(){}

  async addUserAccount(){

    let session_hash = this.encript(this.registerForm.get('email').value.toLowerCase())+this.sessions
   
   
     this.registerForm.get('password_hash').patchValue(this.encript(this.registerForm.get('password').value))
   
     this.registerForm.get('session_id').patchValue(session_hash)
     console.log('dinis')
    console.log(this.registerForm.value)
     try {
      
       this.appService.showLoading()
         let res = await this.webService.addNewsfeedAccount(this.registerForm.value)
         console.log(res.data)
       
         this.appService.openToast(res.message_code)
 
      this.afterLogin.emit(true)
       
     
     
      } catch (e) {
        
        this.appService.openToast(e)
    
        
        console.log(e)
      } finally {
         
         this.registerForm.reset()
         this.appService.hideLoading()
      }
   
   }

   
  async login(){
    
    if(this.loginForm.valid){
      let session_hash = this.encript(this.loginForm.get('email').value.toLowerCase())+this.sessions
      this.loginForm.get('password_hash').patchValue(this.encript(this.loginForm.get('password').value))
      this.loginForm.get('session_id').patchValue(session_hash)
    
      
  

      try {
        this.appService.showLoading()
        let response = await this.webService.newsfeedAccountLogin(this.loginForm.value)
        console.log(response.data)
       this.appService.openToast('Hi, '+response.data.first_name+' welcome back!')
       // this.loginForm.reset();
      this.afterLogin.emit(true)
      
       } catch (e) {
         
         this.appService.openToast(e)
         //this.loginForm.reset();
     
         
         console.log(e)
       } finally {
         this.appService.hideLoading()
       }
    }else{
      this.loginForm.reset();
      this.appService.openToast('Please input correct information')
    }
   
        
      
  }



}
