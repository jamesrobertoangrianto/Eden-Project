import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsTermConditionsComponent } from './news-term-conditions.component';

describe('NewsTermConditionsComponent', () => {
  let component: NewsTermConditionsComponent;
  let fixture: ComponentFixture<NewsTermConditionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsTermConditionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsTermConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
