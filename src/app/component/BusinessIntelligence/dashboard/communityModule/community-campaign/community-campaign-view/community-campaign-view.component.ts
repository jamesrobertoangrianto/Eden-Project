import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faExpandAlt, faExpandArrowsAlt } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
@Component({
  selector: 'app-community-campaign-view',
  templateUrl: './community-campaign-view.component.html',
  styleUrls: ['./community-campaign-view.component.scss']
})
export class CommunityCampaignViewComponent implements OnInit {

  faChevronDown = faChevronDown
  faExpandAlt=faExpandAlt
  faChevronLeft=faChevronLeft
  campaign_id: string;
  campaign: any;
  member: any;
  showModal: null;
  campaign_status: { id: string; label: string; value: string; class: string; }[];
  participant_status: { id: string; label: string; value: string; }[];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params=>{
      this.campaign_id = params.get("id")
      this.getCommunityCampaignById(this.campaign_id)
      
    })

    this.participant_status = [
     
      {
        'id' : 'status',
        'label' : 'approve',
        'value' : 'approve',
        
      },
      {
        'id' : 'status',
        'label' : 'deny',
        'value' : 'deny',
     
      },
    
    ]


    this.campaign_status = [
      {
        'id' : 'status',
        'label' : 'draft',
        'value' : 'draft',
        'class' : 'paid',
      },
      {
        'id' : 'status',
        'label' : 'on_going',
        'value' : 'on_going',
        'class' : 'paid',
      },
      {
        'id' : 'status',
        'label' : 'complete',
        'value' : 'complete',
        'class' : 'paid',
      },
    
    ]
  }

  goToInstagram(link){
  console.log(link.member.instagram_account_link)
    window.open(link.member.instagram_account_link, "_blank");
  }
  async getCommunityCampaignById(id){
    try {
      this.appService.showLoading()
      let res = await this.webService.getCommunityCampaignById(id)
      this.campaign = res.data
      this.campaign.total_participant = this._getParticipant(this.campaign.participant)
      console.log(res)
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }
  
  _getStatus(){
    if(this.campaign.status == 'open_registration'){
      return true
    }
    else{
      return false
    }
  }

  _getParticipant(items){
    let total = 0
    items.forEach(item => {
      if(item.status == 'approve'){
        total += 1
      }
     
    });

    return total
  }

  updateStatus(status){
    let form = {
      'id':'status',
      'value' : status
    }
    console.log(form)
    this.updateCommunityCampaign(form)

  }
  async updateCommunityCampaign(item){
    let form ={}
    form[item.id] = item.value
    console.log(item)
    try {
     
      let res = await this.webService.updateCommunityCampaign(form,this.campaign_id)
      if(res.data){
          this.appService.openToast('Updated!')

      }
      console.log(res)
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.getCommunityCampaignById(this.campaign_id)

      if(item.value == 'complete'){
        this.setWinner()

      }else if(item.value == 'on_going'){
        this.denyMember()
      }
      

    
    }
    
   
  }


  denyMember(){
    let form = {
      'id':'status',
      'value' : 'deny'
    }
    this.campaign.participant.forEach(item => {
      if(item.status !=='approve'){
        this.updateCommunityCampaignMember(form,item.id)

      }
       
    });
    
  }


  
  setWinner(){
    this.campaign.participant.forEach(item => {
        console.log(item.status)
        if(item.status =='approve'){
          this.addMemberReward(item)
          this.addCampaignPoints(item)
        }
       
    });
    
  }



  async getMember(e){
    
    let search = '?search='+e
    try {
    
      let response = await this.webService.getCommunityMember(search)
    
      this.member = response.data
      this.member.forEach(item => {
    
        item.isAdded = this.isMemberAdded(item.id)
      });
     
   
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
  
    }
  }

  async addToCampaign(item){
    
    let form = {
      'community_campaign_id' : this.campaign_id,
      'community_member_id' : item.id,
      'status' : 'request'
    }

      if(this.campaign.id){
        try {
      
          let response = await this.webService.addCommunityCampaignMember(this.campaign_id,form)
          let a = response.data
          a.member = item
          item.isAdded = true
         this.campaign.participant.unshift(a)


          this.appService.openToast('Updated!')
        
        } catch (e) {
          
          this.appService.openToast(e)
          
          console.log(e)
        } finally {
        
      
        }
      }
  }


  
  async addMemberReward(item){
   if(this.campaign.reward_value){
    let form = {
      'description' : 'reward from campaign : ' +  this.campaign.campaign_name,
      'type' :'campaign_reward',
      'name' : this.campaign.reward_value,
      'status' : 'redeem',
      'community_member_id' : item.community_member_id
     
    }
    console.log(item)
    try {
      
      let response = await this.webService.addMemberReward(form,item.id)
        console.log(response)
        if(response.data){
          this.appService.openToast('Updated!')
    
        }
    
    } catch (e) {
      
      this.appService.openToast(e)
      console.log(e)
      
      console.log(e)
    } finally {
      this.getCommunityCampaignById(this.campaign_id)
  
    }
   }
  
  }


  async addCampaignPoints(item){
    if(this.campaign.points_value){
      let form = {
        'description' : 'points from campaign : ' + this.campaign.campaign_name,
        'type' :'credit',
       
        'value' : this.campaign.points_value,
        'community_member_id' : item.community_member_id
       
      }
  
      try {
        
        this.appService.showLoading()
        let res = await this.webService.addCampaignPoints(form,item.id)
      
        console.log(res)
     
      
      } catch (error) {
        this.appService.openToast(error)
        console.log(error)
     
      }
      finally{
        
        this.closeModal()
        this.appService.hideLoading()
      
      }
    }
    
  }


  async updateCommunityCampaignMember(item,id){
    let form ={}
    form[item.id] = item.value
    console.log(item)
    try {
     
      let res = await this.webService.updateCommunityCampaignMember(id,form)
      if(res.data){
        this.appService.openToast('Updated!')
        this.getCommunityCampaignById(this.campaign_id)
      }
      console.log(res)
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
   
    }
   
  }




  getPlaceholderName(item) {
    if(item.first_name && item.last_name){
      var first_name =item.first_name.charAt(0);
      var last_name =item.last_name.charAt(0);
      return first_name + last_name
    }
  
  }


  isMemberAdded(id){

    if(this.campaign.participant.some(member => member.community_member_id === id)){
    return true
    } else{
       return false
    }
   
  }

  
  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }




}
