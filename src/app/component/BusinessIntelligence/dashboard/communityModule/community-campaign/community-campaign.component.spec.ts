import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityCampaignComponent } from './community-campaign.component';

describe('CommunityCampaignComponent', () => {
  let component: CommunityCampaignComponent;
  let fixture: ComponentFixture<CommunityCampaignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityCampaignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityCampaignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
