import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faExpandAlt, faKey, faUser, faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
@Component({
  selector: 'app-community-member',
  templateUrl: './community-member.component.html',
  styleUrls: ['./community-member.component.scss']
})
export class CommunityMemberComponent implements OnInit {
  faExternalLinkAlt=faExternalLinkAlt
  faKey= faKey
  faUser = faUser
  faExpandAlt=faExpandAlt
  faChevronDown = faChevronDown
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];
  contract_status: { id: string; label: string; class: string; }[];

  tab_view: string;
  tab_menu_list: { id: string; label: string; count: number; class: any; }[];
  member: any;
  

  accessForm = new FormGroup({
    username: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    password: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    

    community_member_id: new FormControl(null),
 
  })



  memberForm = new FormGroup({
    
    type: new FormControl(null),

    first_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    // last_name: new FormControl(null,{ 
    //   validators: [Validators.required, Validators.nullValidator]
    // }),
    community_id: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    province: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    instagram_account: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    instagram_follower: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    instagram_account_link: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    email: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    status: new FormControl('new_request'),

  
  
    
    
  })

  partnerForm = new FormGroup({

    type: new FormControl(null),

    phone: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    first_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
  
    community_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    member_count: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    location: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    instagram_account: new FormControl(null),

    instagram_follower: new FormControl(null),


    instagram_account_link: new FormControl(null),




    tiktok_account: new FormControl(null),

    tiktok_account_link: new FormControl(null),


    tiktok_account_follower: new FormControl(null),


    email: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    status: new FormControl('new_request'),

  
  
    
    
  })
  community: any;
  selected_access: any;
  error_message: string;

  domicile_list = [
    'Jakarta','Bogor','Depok','Tangerang - Banten', 'Bekasi','Jawa Barat','Jawa Tengah','Jawa Timur','Bali, NTT, NTB','Sumatra, Aceh, Riau','Kalimantan','Sulawesi','Papua & Lainnya'
  ]

  params: string;
  member_status: { id: string; label: string; value: string; name: string; }[];
  sortListItems: { name: string; items: { id: string; name: string; label: string; }[]; }[];
  city_list: any[];
  city_filter: any;
  page: number;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    private serializer: UrlSerializer,

  ) { }


  
  
  ngOnInit(): void {
    this.page = 0
    //this.export()
    this.getCommunity()

    this.sortListItems = [
      {
       
        'name' : 'status',
        'items' : [
          {
            'id' : 'status',
            'name' : 'new_request',
            'label' : 'New Request',
      
          },
          {
            'id' : 'status',
            'name' : 'active',
            'label' : 'active',
      
          },
          {
            'id' : 'status',
            'name' : 'in_active',
            'label' : 'In Active',
      
          },
        ]
  
      },
    
      {
       
        'name' : 'Domicile',
        'items' : [
          {
            'id' : 'Domicile',
            'name' : 'Jakarta',
            'label' : 'Jakarta',
      
          },
          {
            'id' : 'Domicile',
            'name' : 'Bogor',
            'label' : 'Bogor',
      
          },
          {
            'id' : 'Domicile',
            'name' : 'Tangerang - Banten',
            'label' : 'Tangerang - Banten',
      
          },
          {
            'id' : 'Domicile',
            'name' : 'Bekasi',
            'label' : 'Bekasi',
      
          },
          {
            'id' : 'Domicile',
            'name' : 'Jawa Barat',
            'label' : 'Jawa Barat',
      
          },
          {
            'id' : 'Domicile',
            'name' : 'Jawa Tengah',
            'label' : 'Jawa Tengah',
      
          },
          {
            'id' : 'Domicile',
            'name' : 'Jawa Timur',
            'label' : 'Jawa Timur',
      
          },
        
          {
            'id' : 'Domicile',
            'name' : 'Bali',
            'label' : 'Bali',
      
          },
          {
            'id' : 'Domicile',
            'name' : 'NTT',
            'label' : 'NTT',
      
          },
         
          {
            'id' : 'Domicile',
            'name' : 'NTB',
            'label' : 'NTB',
      
          },
         
          {
            'id' : 'Domicile',
            'name' : 'Sumatra',
            'label' : 'Sumatra',
      
          },
          {
            'id' : 'Domicile',
            'name' : 'Aceh',
            'label' : 'Aceh',
      
          },
          {
            'id' : 'Domicile',
            'name' : 'Riau',
            'label' : 'Riau',
      
          },
          {
            'id' : 'Domicile',
            'name' : 'Kalimantan',
            'label' : 'Kalimantan',
      
          },
          {
            'id' : 'Domicile',
            'name' : 'Sulawesi',
            'label' : 'Sulawesi',
      
          },
          {
            'id' : 'Domicile',
            'name' : 'Papua & Lainnya',
            'label' : 'Papua & Lainnya',
      
          },
         
         
         
        ]
  
      },
      
    ]


    this.route.queryParamMap.subscribe(queryParams => {

       
      this.params = this.serializer.serialize(this.router.createUrlTree([''],
      { queryParams: this.route.snapshot.queryParams}))     


      this.getCommunityMember()

    })


   this.member_status = [
    
    {
      'id' : 'status',
      'label' : 'active',
      'value' : 'active',
      'name' : 'active',

    
    },
    {
      'id' : 'status',
      'label' : 'in_active',
      'value' : 'in_active',
      'name' : 'in_active',

    },
  
  ]

  
  }




  async searchCity($event){
    console.log($event)
    this.city_list=[]
    try {
      this.appService.showLoading()
      let res = await this.webService.getCityList('10',$event)
      
      if(res.data){
        this.city_list = res.data.map(item => {
          return {
            id: item.city_id,
            label: item.city_name,
            name: item.city_name
          };
        });
  
      }
     


     
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
    
    
  }
  selectCity(e){
 
    this.memberForm.get('location').setValue(e.name)

  }

  export(){

    let header = ['fullname','member_id','create_date','community','active_campaign','total_points','dob','location','phone','working_status','mothly_expense','information_source','instagram_account','instagram_follower','instagram_account_link','note']
    const rows = [
      header,
      ...this.member.map(item => {
      //  return [item]
       return [item.first_name,item.id,item.created_at,item.community_name,item.total_campaign,item.total_points,item.dob,item.location,item.phone,item.working_status,item.mothly_expense,item.information_source,item.instagram_account,item.instagram_follower,item.instagram_account_link,this.getNote(item.note)];
      })
    ];



  
   let csvContent = "data:text/csv;charset=utf-8,";
  
    rows.forEach(function(rowArray) {
        let row = rowArray.join(",");
        csvContent += row + "\r\n";
    });


   var encodedUri = encodeURI(csvContent);

    // var csvContent = "data:text/csv;charset=utf-8,%EF%BB%BF" + encodeURI(csvContent);

    // window.open(csvContent);

    var link = window.document.createElement("a");
  link.setAttribute("href", encodedUri);
  link.setAttribute("download", "member.csv");
  link.click();


  }

  getNote(items){
    
    return Array.prototype.map.call(items, s => s.description).toString(); // "1,2,3"

  }

  

  async getCommunity(){
    try {
      this.appService.showLoading()
      let res = await this.webService.getCommunity()
      this.community = res.data
    
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }

  async getCommunityMember(){
    this.page = 1
   
    try {
    
      this.appService.showLoading()
      let res = await this.webService.getCommunityMember(this.params)
      
      if(res.data){
        res.data.forEach(item => {
          item.total_campaign = this._getCampaign(item.campaign)
        });
  
      }
      
     
      
      this.member = res.data

      console.log(this.member)
     
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
     

    
    }
  }

  async loadMore() {
    console.log('load')
    this.page = this.page+1
    this.params = '?/'+this.params+'&page='+this.page
   
    
    try {
      this.appService.showLoading()
    


    let res = await this.webService.getCommunityMember(this.params)
      console.log(res)
    if(res.data){
      res.data.forEach(item => {
        item.total_campaign = this._getCampaign(item.campaign)
      });

      this.member = this.member.concat(res.data)

    }
     
     
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.hideLoading()
  
    }
  }


  


  
  async updateCommunityMember(item,id){
    let form ={}
    form[item.id] = item.value
    console.log(item)
    try {
     
      let res = await this.webService.updateCommunityMember(form,id)
      if(res.data){
          this.appService.openToast('Updated!')
        
          this.getCommunityMember()
      }
      console.log(res)
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
   
    }
    
   
  }



  _getCampaign(items){
    let total = 0
    items.forEach(item => {
      if(item.status == 'approve'){
        total += 1
      }
     
    });

    return total
  }



  async addAccess(){
    
    this.accessForm.get('community_member_id').setValue(this.selected_access.id)
    try {
      console.log(this.accessForm.value)

   
      let response = await this.webService.addClientAuth(this.accessForm.value)
      console.log(response)
      if(response.data){
        this.closeModal()
        this.appService.openToast('New access added!')
     
      }
      
    } catch (e) {
      
      this.appService.openToast(e)
      this.appService.openToast('username exist, please use another username')
      this.error_message = 'username exist, please use another username'
      console.log(e)
    } finally {
     this.accessForm.reset
      // this.appService.hideLoading()
      // this.appService.openToast('New creator added!')
     this.getCommunityMember()
      
    }
  }

  

  async addCommunityMember(con,id){
    try {
      if(id == 'partner'){
        this.partnerForm.get('type').setValue('partner')
      }
      else if(id == 'member'){
        this.partnerForm.get('type').setValue('member')
      }
      this.appService.showLoading()
      let res = await this.webService.addCommunityMember(this.memberForm.value)
     console.log(res)
      if(res.data.id){
      
        if(con){
          this._navigateTo(res.data.id)
        }
        else{
          this.getCommunityMember()
          this.getCommunity()
        }
        this.appService.openToast('New Member Added!')
     
      }
    
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.memberForm.reset()
      this.appService.hideLoading()
      this.closeModal()
    }
  }


  viewAccess(access){
    console.log(access)
    this.selected_access = access
    this.openModal('accessModal')
  }

  
  getPlaceholderName(item) {
    if(item.first_name && item.last_name){
      var first_name =item.first_name.charAt(0);
      var last_name =item.last_name.charAt(0);
      return first_name + last_name
    }
  
  }


  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }

  _navigateTo(page) {
    this.router.navigate(
      ['/community/member/view/'+page]
    );
  }


}
