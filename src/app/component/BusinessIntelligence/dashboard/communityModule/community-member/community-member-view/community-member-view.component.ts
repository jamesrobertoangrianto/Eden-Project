import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-community-member-view',
  templateUrl: './community-member-view.component.html',
  styleUrls: ['./community-member-view.component.scss']
})
export class CommunityMemberViewComponent implements OnInit {
  showModal: any;
  faChevronDown = faChevronDown
  faChevronLeft=faChevronLeft
  member: any;
  member_id: string;
  member_status: { id: string; label: string; value: string; class: string; }[];

  pointsForm = new FormGroup({
    description: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    value: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    
    type: new FormControl('credit'),
    community_member_id: new FormControl(null),
    create_by: new FormControl(null),
    
    
  })



  rewardForm = new FormGroup({
    description: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    value: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    status: new FormControl('redeem'),
    
    

    community_member_id: new FormControl(null),
    create_by: new FormControl(null),
    
    
  })

  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params=>{
      this.member_id = params.get("id")
      this.getCommunityMemberById(this.member_id)
    })


   this.member_status = [
   
    {
      'id' : 'status',
      'label' : 'active',
      'value' : 'active',
      'class' : 'paid',
    },
    {
      'id' : 'status',
      'label' : 'in_active',
      'value' : 'in_active',
      'class' : 'paid',
    },
  
  ]

  }

  async getCommunityMemberById(id){
    try {
      this.appService.showLoading()
      let res = await this.webService.getCommunityMemberById(id)
      this.member = res.data
   
      console.log(res)
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }



  
  


  async updateCommunityMember(item){
    let form ={}
    form[item.id] = item.value
    console.log(item)
    if(this.member_id){
      try {
     
        let res = await this.webService.updateCommunityMember(form,this.member_id)
        if(res.data){
            this.appService.openToast('Updated!')
        }
        console.log(res)
      
      } catch (error) {
        this.appService.openToast(error)
      }
      finally{
     
      }
    }
    
   
  }

  async addMemberPoints(){
    try {
      this.pointsForm.get('community_member_id').setValue(this.member_id)
      this.pointsForm.get('create_by').setValue(this.webService.account_id)
      console.log(this.pointsForm.value)
      this.appService.showLoading()
      let res = await this.webService.addMemberPoints(this.pointsForm.value)
      this.member.points.unshift(res.data)
      console.log(res)
   
    
    } catch (error) {
      this.appService.openToast(error)
      console.log(error)
   
    }
    finally{
      this.pointsForm.reset()
      this.closeModal()
      this.appService.hideLoading()
    
    }
  }




  async addCustomMemberReward(){
    try {
      this.rewardForm.get('community_member_id').setValue(this.member_id)
      this.rewardForm.get('create_by').setValue(this.webService.account_id)

      console.log(this.rewardForm.value)
      this.appService.showLoading()
      let res = await this.webService.addCustomMemberReward(this.rewardForm.value)
      this.member.reward.unshift(res.data)
      console.log(res)
   
    
    } catch (error) {
      this.appService.openToast(error)
      console.log(error)
   
    }
    finally{
      this.rewardForm.reset()
      this.closeModal()
      this.appService.hideLoading()
    
    }
  }




  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }


}
