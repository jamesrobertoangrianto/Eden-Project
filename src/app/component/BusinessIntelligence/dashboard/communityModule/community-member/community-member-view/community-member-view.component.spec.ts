import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityMemberViewComponent } from './community-member-view.component';

describe('CommunityMemberViewComponent', () => {
  let component: CommunityMemberViewComponent;
  let fixture: ComponentFixture<CommunityMemberViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityMemberViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityMemberViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
