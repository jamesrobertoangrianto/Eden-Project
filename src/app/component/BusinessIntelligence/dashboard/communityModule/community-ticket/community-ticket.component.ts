import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-community-ticket',
  templateUrl: './community-ticket.component.html',
  styleUrls: ['./community-ticket.component.scss']
})
export class CommunityTicketComponent implements OnInit {

  
  faChevronDown = faChevronDown
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];
  contract_status: { id: string; label: string; class: string; }[];
  booking_status: { id: string; label: string; class: string; }[];
  tab_view: string;
  ticket: any;
  tab_menu_list: { id: string; label: string; }[];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

   
  ngOnInit(): void {
    this.route.queryParamMap.subscribe(queryParams => {
      this.tab_view = queryParams.get("tab_view")
    })
    this.getCommunityTicket()
    this.setComponentData()
  }


  setComponentData(){
    this.tab_menu_list = [
      {
        'id' : 'new_ticket',
        'label' : 'New Ticket',
       
      
      },
      {
        'id' : 'on_process',
        'label' : 'On Process',
      
      },
      {
        'id' : 'complete',
        'label' : 'Complete',
        
      }
    ]
    
  }


  async getCommunityTicket(){
    try {
      this.appService.showLoading()
      let res = await this.webService.getCommunityTicket()
   this.ticket = res.data
      console.log(res)
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }

  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }

  _navigateTo(page) {
    this.router.navigate(
      ['/community/ticket/view/'+page]
    );
  }



}
