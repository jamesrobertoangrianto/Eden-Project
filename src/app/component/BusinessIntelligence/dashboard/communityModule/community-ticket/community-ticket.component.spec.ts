import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityTicketComponent } from './community-ticket.component';

describe('CommunityTicketComponent', () => {
  let component: CommunityTicketComponent;
  let fixture: ComponentFixture<CommunityTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
