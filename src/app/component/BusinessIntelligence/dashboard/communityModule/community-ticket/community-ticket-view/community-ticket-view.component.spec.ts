import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityTicketViewComponent } from './community-ticket-view.component';

describe('CommunityTicketViewComponent', () => {
  let component: CommunityTicketViewComponent;
  let fixture: ComponentFixture<CommunityTicketViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityTicketViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityTicketViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
