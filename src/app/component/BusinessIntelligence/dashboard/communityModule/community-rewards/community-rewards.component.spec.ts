import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityRewardsComponent } from './community-rewards.component';

describe('CommunityRewardsComponent', () => {
  let component: CommunityRewardsComponent;
  let fixture: ComponentFixture<CommunityRewardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityRewardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityRewardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
