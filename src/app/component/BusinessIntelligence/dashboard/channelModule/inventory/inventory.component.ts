import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faExpandAlt, faTrash, faTrashAlt, faSortAlphaDown, faSort, faSortUp, faSortDown } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {
  faChevronDown = faChevronDown
  faTrashAlt=faTrashAlt
  faExpandAlt=faExpandAlt
  faPenSquare = faPenSquare
  faStickyNote = faStickyNote
  faEnvelope = faEnvelope
  faPhone = faPhone
  faUpload = faUpload
  faChevronRight = faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft = faChevronLeft
  faCalendar = faCalendarAlt
  faMapMarked = faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  faSortUp=faSortUp
  faSort=faSort
  contract_status: {
    id: string;label: string;class: string;
  } [];
  booking_status: {
    id: string;label: string;class: string;
  } [];
  tab_view: string;
  tab_menu_list: {
    id: string;label: string;class: any;
  } [];
  assets: any;
  product: any;
  selected_inventory: any;

  image = '/assets/image/logo.png'


  stockForm = new FormGroup({
    sender: new FormControl(null, {
      validators: [Validators.required, Validators.nullValidator]
    }),
    stock_in: new FormControl(null, {
      validators: [Validators.required, Validators.nullValidator]
    }),
    stock_out: new FormControl(0),
    expired_date: new FormControl(null),
    variant: new FormControl(null),
    location: new FormControl(null),
    inventory_id: new FormControl(null),








  })


  inventoryForm = new FormGroup({
    type: new FormControl(null, {
      validators: [Validators.required, Validators.nullValidator]
    }),
    name: new FormControl(null, {
      validators: [Validators.required, Validators.nullValidator]
    }),
    brand: new FormControl(null),
    status: new FormControl('in_use'),

    
    category_id: new FormControl(null),
    expired_date: new FormControl(null),
    location: new FormControl(null),
    cost: new FormControl(null),

  })

  params: string;
  sortListItems: { name: string; items: any; }[];
  category: any;
  category_option: any[];
  category_filter: any[];
  sortListItemss: { name: string; items: { id: string; label: string; value: string; }[]; }[];
  assets_status: { id: string; label: string; value: string; name: string; }[];
  products: { id: number; name: string; category: string; description: string; price: number; }[];
  sortkey: any;
bulk_stock: any;
  selectedImage: any;
  page: number;
  product_page: number;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService: BusinessIntelligenceService,
    private webService: ManualOrderService,
    private serializer: UrlSerializer,

  ) {}



  ngOnInit(): void {
    

    this.assets_status = [
      {
        'id' : 'status',
        'label' : 'in_use',
        'value' : 'in_use',
        'name' : 'in_use',
      },
      {
        'id' : 'status',
        'label' : 'in_service',
        'value' : 'in_service',
        'name' : 'in_service',
      },
      {
        'id' : 'status',
        'label' : 'in_storage',
        'value' : 'in_storage', 'name' : 'in_storage',
      },
      {
        'id' : 'status',
        'label' : 'missing',
        'value' : 'missing', 'name' : 'missing',
      },
      {
        'id' : 'status',
        'label' : 'sold',
        'value' : 'sold', 'name' : 'sold',
      },
      {
        'id' : 'status',
        'label' : 'expired',
        'value' : 'expired', 'name' : 'expired',
      }
    ]
    this.sortListItemss = [
      
       
       

      {
       
        'name' : 'status',
        'items' :this.assets_status

      },
     
    ]
    this.sortListItems = [
      
       
       
     
    ]
    this.getCategoryFilter()
    this.route.queryParamMap.subscribe(queryParams => {


     
      this.params = this.serializer.serialize(this.router.createUrlTree([''],
      { queryParams: this.route.snapshot.queryParams}))     


      this.sortkey=[]
    
      this.tab_view = queryParams.get("tab_view")
      if (this.tab_view == 'assets') {
      
        this.getAssets()
     
      } else if (this.tab_view == 'product') {
        
        this.getProduct()
      }
      else if (this.tab_view == 'category') {
        this.getVendorProductCategory()
      }

    })

   


    this.tab_menu_list = [

      {
        'id': 'product',
        'label': 'Product',
        'class': null,
      },
      {
        'id': 'assets',
        'label': 'Assets',
        'class': null,
      },
       {
        'id': 'category',
        'label': 'Category',
        'class': null,
      },

    ]


  



  }


  showImage(item){
    console.log(item)
    this.selectedImage = item
  }



  async getAssets() {
    this.page = 1
    try {
      this.appService.showLoading()
      let res = await this.webService.getInventory('assets',this.params)
      this.assets = res.data
      console.log(res)

    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.appService.hideLoading()
    }
  }


  async loadMoreAssets() {
    console.log('load')
    this.page = this.page+1
    this.params = '?/'+this.params+'&page='+this.page
   
    
    try {
      this.appService.showLoading()
    


      let res = await this.webService.getInventory('assets',this.params)
     
    if(res.data){
     

      this.assets = this.assets.concat(res.data)

    }
     
     
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.hideLoading()
  
    }
  }







  async updateVendorProductCategory(e, id) {
    let form = {}
    form[e.id] = e.value
    console.log(form)
    try {
      this.appService.showLoading()
      let res = await this.webService.updateVendorProductCategory(form, id)
      if (res.data) {

        this.appService.openToast('updated!')
      }

      console.log(res)

    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.appService.hideLoading()
    }
  }




  async deleteInventory(type,item) {

    try {
      this.appService.showLoading()
      let res = await this.webService.deleteInventory(type,item.id)
      if (res.data) {
        console.log(res.data)
        this.appService.openToast('Deleted!')
        this.getAssets()
        this.getProduct()
      }

      console.log(res)

    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.appService.hideLoading()
    }
  }

  async deleteVendorProductCategory(item) {

    try {
      this.appService.showLoading()
      let res = await this.webService.deleteVendorProductCategory(item.id)
      if (res.data) {
        let index = this.category.indexOf(item)
        
        this.category.splice(index, 1)
        this.appService.openToast('updated!')
      }

      console.log(res)

    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.appService.hideLoading()
    }
  }

  async getVendorProductCategory() {
    try {
      this.appService.showLoading()
      let res = await this.webService.getVendorProductCategory('inventory',this.params)
      this.category = res.data
      console.log(res)

    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.appService.hideLoading()
    }
  }

  async getCategoryFilter() {
    this.category_filter = []
    try {
      this.appService.showLoading()
      let res = await this.webService.getVendorProductCategory('inventory',this.params)
      
      
      if(res.data){
        this.category_filter = res.data.map(item => {
          return {
            id: item.id,
            label: item.name,
            name: item.name
          };
        });
  
        
        this.sortListItems.push(
          {
         
            'name' : 'category',
            'items' : this.category_filter
          },
        )
        this.sortListItemss.push(
          {
         
            'name' : 'category',
            'items' : this.category_filter
          },
        )
      
      }

      

    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.appService.hideLoading()
    }
  }
  

  _getAge(start_date) {
    var today = new Date()
    var receive = new Date(start_date)
    let time = Math.round((today.getTime() - receive.getTime()) / (1000 * 60 * 60 * 24));
    return time + ' Day(s) Age'
  }

  _getExpired(end_date) {
    var today = new Date()
    var expired = new Date(end_date)
    let time = Math.round((expired.getTime() - today.getTime()) / (1000 * 60 * 60 * 24));
    if (time < 0) {
      return 'Expired'
    } else {
      return time + ' Day(s) Left'
    }

  }


  async getProduct() {
    this.product_page = 1
    try {
      this.appService.showLoading()
      let res = await this.webService.getInventory('product',this.params)
      this.product = res.data
      this.calculateTotalStock()
      console.log(res)

    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.appService.hideLoading()
    }
  }


  async loadMoreProduct() {
    console.log('load')
    this.product_page = this.product_page+1
    this.params = '?/'+this.params+'&page='+this.product_page
   
    
    try {
      this.appService.showLoading()
    


      let res = await this.webService.getInventory('product',this.params)


     
    if(res.data){
     

      this.product = this.product.concat(res.data)

    }
     
     
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.hideLoading()
  
    }
  }


  calculateTotalStock() {
    if (this.product) {
      this.product.forEach(item => {
        item.total_stock = this._getAvailableStock(item.stock)
      });
    }

  }
  _getAvailableStock(stock) {
    let total = 0
    stock.forEach(item => {
      total += item.stock_in - item.stock_out
    });
    return total
  }

  async updateStock(action, item) {
    if (action == 'add') {
      item.stock_out = item.stock_out + 1
    } else if (action == 'reduce') {
      item.stock_out = item.stock_out - 1
    }
    console.log(item.stock_out)
    let form = {}
    form['stock_out'] = item.stock_out
    try {


      let res = await this.webService.updateInventoryStock(form, item.inventory_id,item.id)

      console.log(res)
      this.appService.openToast('Stock Update!')
      this.calculateTotalStock()
    } catch (error) {
      this.appService.openToast(error)
    } finally {

    }
  }


  async bulkStock(item) {
    var total_stock = item.stock_in - item.stock_out
   
    if(total_stock < item.bulk_stock){
      this.appService.openToast('cant update! current available stock '+total_stock)
    }else{
      item.stock_out = item.stock_out + item.bulk_stock

    let form = {}
    form['stock_out'] = item.stock_out
    try {


      let res = await this.webService.updateInventoryStock(form, item.inventory_id,item.id)

      console.log(res)
      this.appService.openToast('Stock Update!')
      this.calculateTotalStock()
    } catch (error) {
      this.appService.openToast(error)
    } finally {
      item.bulk_stock = null

    }
    }




    
  }


  async onChange(event: any,id) {
    const file: File = event.target.files[0];

    if (file) {
     

      const formData = new FormData();
  
      formData.append('file', file);

     
      
      try {
        this.appService.showLoading()
        let res = await this.webService.uploadPhoto(formData)
        console.log(res)
        if(res.data){
          let form = {
            'id' : 'image_url',
            'value' : res.data
          }

        
          this.updateProduct(form,id)

        }
        
      } catch (error) {
        this.appService.openToast(error)
      }
      finally{
       
        this.appService.hideLoading()
      
      }

    }
  }


  async onChangeStock(event: any,id) {
    const file: File = event.target.files[0];

    if (file) {
     

      const formData = new FormData();
  
      formData.append('file', file);

     
      
      try {
        this.appService.showLoading()
        let res = await this.webService.uploadPhoto(formData)
        console.log(res)
        if(res.data){
          let form = {
            'id' : 'image_url',
            'value' : res.data
          }

        
          this.updateInventoryStock(form,id)

        }
        
      } catch (error) {
        this.appService.openToast(error)
      }
      finally{
       
        this.appService.hideLoading()
      
      }

    }
  }



  async updateProduct(e, id) {

    let form = {}
    form[e.id] = e.value
    console.log(form)
    try {

      let res = await this.webService.updateInventory(form, id)

      if (res.data) {
        this.appService.openToast('Updated!')
      }
    } catch (error) {
      this.appService.openToast(error)
    } finally {

      this.getAssets()
    }
  }

  
  async updateInventoryStock(event, item) {
    
    let form = {}
    if(item.stock_out > event.value){
      this.appService.openToast('Cant edit stock in lower than stock out')
      event.value = item.stock_in
    }
    else{
      form[event.id] = event.value
   
    try {

        this.appService.showLoading()
        let res = await this.webService.updateInventoryStock(form, item.inventory_id,item.id)

        console.log(res)
        this.calculateTotalStock()
      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.appService.openToast('Stock Update!')
        this.appService.hideLoading()
        this.getProduct()
      }
    }
    
  }


  selectCategory(e) {
    console.log(e)
    this.inventoryForm.get('category_id').setValue(e.value)
    

  }



  async updateVendor(e, id) {

    let form = {}
    form[e.id] = e.value
    console.log(form)
    try {

      let res = await this.webService.updateVendor(form, id)
      if (res.data) {
        this.appService.openToast('Updated!')
      }

    } catch (error) {
      this.appService.openToast(error)
    } finally {


    }
  }


  async searchCategory(e) {
    this.category_option = []
    let search = '?search='+e
    try {
     // this.appService.showLoading()
      let res = await this.webService.getVendorProductCategory('inventory',search)







    this.category_option = res.data.map(item => {
      return {
        id: 'category_id',
        label: item.name,
        value: item.id
      };
    });


      console.log(res)
    } catch (error) {
      
    }


    


  }


  async addVendorProductCategory() {
    let form = {
      'type':'inventory'
    }
    try {
      this.appService.showLoading()
      let res = await this.webService.addVendorProductCategory(form)
      if (res.data) {
        this.category.unshift(res.data)
        this.appService.openToast('category added!')
      }

      console.log(res)

    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.appService.hideLoading()
    }
  }


  async updateInventory(event, id) {
    let form = {}
    form[event.id] = event.value
    console.log(form)
    try {

      this.appService.showLoading()
      let res = await this.webService.updateInventory(form, id)
      if(res.data){
        this.getAssets()
        this.getProduct()
      }
      
      console.log(res)
    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.appService.openToast('Stock Update!')
      this.appService.hideLoading()
    }
  }


  async addInventory() {
    console.log(this.inventoryForm.value)
    try {

      this.appService.showLoading()
      let res = await this.webService.addInventory(this.inventoryForm.value)
      if (res.data) {

        this.appService.openToast('Inventory Added!')
        if (this.inventoryForm.get('type').value == 'product') {
          this.getProduct()
        } else if (this.inventoryForm.get('type').value == 'assets') {
          this.getAssets()
        }
      }



    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.inventoryForm.reset()
      this.appService.hideLoading()
      this.closeModal()


    }
  }



  async addInventoryStock() {
    console.log(this.stockForm.value)
    try {
      this.stockForm.get('inventory_id').setValue(this.selected_inventory.id)
      this.appService.showLoading()

      let res = await this.webService.addInventoryStock(this.stockForm.value, this.selected_inventory.id)
      console.log(res.data)

      res.data.isNew = true
      this.selected_inventory.stock.unshift(res.data)
      this.calculateTotalStock()

    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.stockForm.reset()
      this.closeModal()
      this.appService.hideLoading()
      this.appService.openToast('Stock Added!')
    }
  }

  toggleShow(item) {
    item.isShow = !item.isShow
  }


  showStock(item) {
    this.selected_inventory = item
    this.openModal('stockModal')
    console.log(item)
  }

  openModal(modal) {
    this.showModal = modal
  }
  closeModal() {
    this.showModal = null
  }



  _navigateTo(page) {
    this.router.navigate(
      ['/creator/contract/view/' + page]
    );
  }

}
