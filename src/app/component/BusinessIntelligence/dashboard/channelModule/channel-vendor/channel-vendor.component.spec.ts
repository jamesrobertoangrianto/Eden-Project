import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelVendorComponent } from './channel-vendor.component';

describe('ChannelVendorComponent', () => {
  let component: ChannelVendorComponent;
  let fixture: ComponentFixture<ChannelVendorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChannelVendorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelVendorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
