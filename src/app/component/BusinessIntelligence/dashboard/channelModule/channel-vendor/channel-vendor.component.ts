    import { Component, OnInit } from '@angular/core';
    import { FormGroup, FormControl, Validators } from '@angular/forms';
    import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
    import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faAddressBook, faExpandAlt, faKey, faTrashAlt, faSort } from '@fortawesome/free-solid-svg-icons';
    import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
    import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
  @Component({
    selector: 'app-channel-vendor',
    templateUrl: './channel-vendor.component.html',
    styleUrls: ['./channel-vendor.component.scss']
  })
  export class ChannelVendorComponent implements OnInit {
    faAddressBook = faAddressBook
    faUpload=faUpload
    faTrashAlt=faTrashAlt
    faChevronDown = faChevronDown
    faExpandAlt=faExpandAlt
    faSort=faSort
    faKey=faKey
    show_columns: boolean;
    selected_columns: any;
    columns_view: string;
    showModal: any;
    payment_status: {} [];
    contract_status: {
      id: string;label: string;class: string;
    } [];
    booking_status: {
      id: string;label: string;class: string;
    } [];
    tab_view: string;
    
    vendor: any;




    contactForm = new FormGroup({

      name: new FormControl(null, {
        validators: [Validators.required, Validators.nullValidator]
      }),
     
      mobile: new FormControl(null, {
        validators: [Validators.required, Validators.nullValidator]
      }),
      position: new FormControl(null, {
        validators: [Validators.required, Validators.nullValidator]
      }),
      vendor_id: new FormControl(null),

    })








    vendorForm = new FormGroup({
      name: new FormControl(null, {
        validators: [Validators.required, Validators.nullValidator]
      }),
      type: new FormControl('channel'),



      category_id: new FormControl(null),

      phone: new FormControl(null),
      rating_name: new FormControl('not-set'),
      website: new FormControl(null),

    })

    productForm = new FormGroup({
      name: new FormControl(null, {
        validators: [Validators.required, Validators.nullValidator]
      }),
      type: new FormControl('channel'),
      vendor_id: new FormControl(null),

      category_id: new FormControl(null),

      price: new FormControl(null),


      stock: new FormControl(null),

    })
    product: any;
    selected_vendor: any;
    
    category: any;
    category_option: any;
    selected_access: any;



  accessForm = new FormGroup({
    username: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    password: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    vendor_id: new FormControl(null),

    


    
  })
    error_message: string;
    params: string;
    sortListItems: { name: string; items: any; }[];
    category_filter: any;



    tab_menu_list = [{
      'id': 'vendor',
      'label': 'Vendor',

      'class': null,
    },
    {
      'id': 'product',
      'label': 'Product',

      'class': null,
    },
    {
      'id': 'category',
      'label': 'Category',

      'class': null,
    },







  ]


    rating = [

      {
        'id': 'rating_name',
        'label': 'bad',
        'value': 'bad',

      },
      {
        'id': 'rating_name',
        'label': 'good',
        'value': 'good',

      },
      {
        'id': 'rating_name',
        'label': 'best',
        'value': 'best',

      },


    ]
    sortListItemss: any[];
    sortkey: any;

    constructor(
      private route: ActivatedRoute,
      private router: Router,
      private appService: BusinessIntelligenceService,
      private webService: ManualOrderService,
      private serializer: UrlSerializer,
    ) {}

    ngOnInit(): void {
      this.sortListItemss = []
      this.sortListItems = [
      
       
       

        {
         
          'name' : 'rating',
          'items' :[
            {
              'name' :'bad',
              'label' :'bad'
            },
            {
              'name' :'good',
              'label' :'good'
            },
            {
              'name' :'best',
              'label' :'best',
  
            },
           
          ]
  
        },
       
      ]
      this.getCategoryFilter()
      this.route.queryParamMap.subscribe(queryParams => {
        this.params = this.serializer.serialize(this.router.createUrlTree([''],
        { queryParams: this.route.snapshot.queryParams}))      


        this.tab_view = queryParams.get("tab_view")
        if (this.tab_view == 'vendor') {

          this.sortkey = []
          this.getVendor()

        

        } else if (this.tab_view == 'product') {
          this.sortkey = []
          this.getVendorProduct()
        } else if (this.tab_view == 'category') {
          this.sortkey = []
          this.getVendorProductCategory()
        }


      })

      


  




    }



  
    async searchCategory(e) {
      this.category_option = []
      let search = '?search='+e
      try {
       // this.appService.showLoading()
        let res = await this.webService.getVendorProductCategory('channel',search)







      this.category_option = res.data.map(item => {
        return {
          id: 'category_id',
          label: item.name,
          value: item.id
        };
      });


        console.log(res)
      } catch (error) {
        
      }


      


    }
    selectCategory(e) {
      console.log(e)
      this.vendorForm.get('category_id').setValue(e.value)
      this.productForm.get('category_id').setValue(e.value)
    }

    selectTabView($event) {}
    async getVendor() {
      try {
        this.appService.showLoading()
        let res = await this.webService.getVendor('channel',this.params)
        this.vendor = res.data
        console.log(res)

      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.appService.hideLoading()
      }
    }

    viewProduct(item){
      this.selected_vendor = item
      this.openModal('productModal')
    }

    async onChange(event: any,id) {
      const file: File = event.target.files[0];
  
      if (file) {
       
  
        const formData = new FormData();
    
        formData.append('file', file);
  
       
        
        try {
          this.appService.showLoading()
          let res = await this.webService.uploadPhoto(formData)
          console.log(res)
          if(res.data){
            let form = {
              'id' : 'image_url_1',
              'value' : res.data
            }
  
          
            this.updateProduct(form,id)
  
          }
          
        } catch (error) {
          this.appService.openToast(error)
        }
        finally{
         
          this.appService.hideLoading()
        
        }
  
      }
    }
  


    async getVendorProduct() {
      try {
        this.appService.showLoading()
        let res = await this.webService.getVendorProduct('channel',this.params)
        this.product = res.data
        console.log(res)

      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.appService.hideLoading()
      }
    }
    viewAccess(access){
      console.log(access)
      this.selected_access = access
      this.openModal('accessModal')
    }

    viewDetails(item){
     
      this.selected_vendor = item
      this.openModal('detailModal')
    }
  
    remove(i,item){
     // item.product.splice(i,1)

         
      // let index =  this.selected_vendor.product.indexOf(item)
      // console.log(index)
      //  this.selected_vendor.product.splice(index, 1)


      console.log(item)
      
    }

    async deleteVendor(id) {
      try {
        this.appService.showLoading()
        let res = await this.webService.deleteVendor(id)
       
        console.log(res)

      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.ngOnInit()
        this.appService.hideLoading()
      }
    }
    async getVendorProductCategory() {
      this.category_filter = []
      try {
        this.appService.showLoading()
        let res = await this.webService.getVendorProductCategory('channel',this.params)
        this.category = res.data
        

        

      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.appService.hideLoading()
      }
    }

    async getCategoryFilter() {
      this.category_filter = []
      try {
        this.appService.showLoading()
        let res = await this.webService.getVendorProductCategory('channel',this.params)
        
        
        if(res.data){
          this.category_filter = res.data.map(item => {
            return {
              id: item.id,
              label: item.name,
              name: item.name
            };
          });
    
          
          this.sortListItems.push(
            {
           
              'name' : 'category',
              'items' : this.category_filter
            },
          )
          this.sortListItemss.push(
            {
           
              'name' : 'category',
              'items' : this.category_filter
            },
          )
        }

        

      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.appService.hideLoading()
      }
    }


    async addVendorProductCategory() {
      let form = {
        'type':'channel'
      }
      try {
        this.appService.showLoading()
        let res = await this.webService.addVendorProductCategory(form)
        if (res.data) {
          this.category.unshift(res.data)
          this.appService.openToast('category added!')
         
        }

        console.log(res)

      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.appService.hideLoading()
      }
    }


  async addAccess(){
    this.error_message = null
    this.accessForm.get('vendor_id').setValue(this.selected_access.id)
    try {
      console.log(this.accessForm.value)

   
      let response = await this.webService.addClientAuth(this.accessForm.value)
      console.log(response)
      if(response.data){
        this.appService.openToast('Access Added')
        this.closeModal()
     
      }
      
    } catch (e) {
      
      this.appService.openToast(e)
      this.appService.openToast('username exist, please use another username')
      this.error_message = 'username exist, please use another username'
      console.log(e)
    } finally {
     this.accessForm.reset
      // this.appService.hideLoading()
      // this.appService.openToast('New creator added!')
      this.getVendor()
      
    }
  }

    async updateVendorProductCategory(e, id) {
      let form = {}
      form[e.id] = e.value
      console.log(form)
      try {
        this.appService.showLoading()
        let res = await this.webService.updateVendorProductCategory(form, id)
        if (res.data) {

          this.appService.openToast('updated!')
        }

        console.log(res)

      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.appService.hideLoading()
      }
    }

    async deleteVendorProductCategory(item) {

      try {
        this.appService.showLoading()
        let res = await this.webService.deleteVendorProductCategory(item.id)
        if (res.data) {
          let index = this.category.indexOf(item)
          
          this.category.splice(index, 1)
          this.appService.openToast('updated!')
        }

        console.log(res)

      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.appService.hideLoading()
      }
    }


    async deleteVendorProduct(child,item,i) {

      try {
        this.appService.showLoading()
        let res = await this.webService.deleteVendorProduct(child.id)
        if (res.data) {
          item.product_count = item.product_count -1
          item.product.splice(i,1)

          this.appService.openToast('updated!')
          //this.getVendorProduct()
        }
  
        console.log(res)
  
      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.appService.hideLoading()
      }
    }



    async deleteProduct(id) {

      try {
        this.appService.showLoading()
        let res = await this.webService.deleteVendorProduct(id)
        if (res.data) {
          
          this.ngOnInit()
        }
  
        console.log(res)
  
      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.appService.hideLoading()
      }
    }



    toggleShow(item,name) {
      item.isShow = name
    }
    async addVendor() {
      //console.log(this.vendorForm.value)
      this.vendorForm.get('type').setValue('channel')
      try {
        this.appService.showLoading()
        let res = await this.webService.addVendor(this.vendorForm.value)
        console.log(res)
        if(res.data){
         
          this.getVendor()
          this.appService.openToast('New vendor added!')
        }
       
      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.vendorForm.reset()
        this.appService.hideLoading()
        this.closeModal()
       // this.ngOnInit()
      }
    }

    async updateVendor(e, id) {

      let form = {}
      form[e.id] = e.value
      console.log(form)
      try {

        let res = await this.webService.updateVendor(form, id)
        if (res.data) {
          this.appService.openToast('Updated!')
          this.getVendor()
        }

      } catch (error) {
        this.appService.openToast(error)
      } finally {


      }
    }

    createProduct(item) {
      console.log(item)
      this.selected_vendor = item
      this.openModal('addProductModal')
    }


    async updateProduct(e, id) {

      let form = {}
      form[e.id] = e.value
      console.log(form)
      try {

        let res = await this.webService.updateVendorProduct(form, id)

        if (res.data) {
          this.appService.openToast('Updated!')
        }
      } catch (error) {
        this.appService.openToast(error)
      } finally {

        this.getVendorProduct()
      }
    }

    createContact(item) {
      this.selected_vendor = item
      this.openModal('addContactModal')
    }

    async addContact() {
      this.contactForm.get('vendor_id').setValue(this.selected_vendor.id)


      try {
        this.appService.showLoading()
        let response = await this.webService.addContact(this.contactForm.value)
        this.selected_vendor.contact.unshift(response.data)
        console.log(response)
      } catch (e) {

        this.appService.openToast(e)

        console.log(e)
      } finally {
        this.contactForm.reset()
        this.appService.hideLoading()

        this.closeModal()

      }
    }

    async updateContact(id, contact) {


      let form = {}
      form[contact.id] = contact.value

      console.log(form)
      try {

        let response = await this.webService.updateContact(id, form)
        console.log(response)

      } catch (e) {

        this.appService.openToast(e)

        console.log(e)
      } finally {


      }
    }

    async addVendorProduct(item ? ) {
      
      
      this.productForm.get('vendor_id').setValue(this.selected_vendor.id)
       this.productForm.get('type').setValue('channel')
      console.log(this.productForm.value)
      try {

        let res = await this.webService.addVendorProduct(this.productForm.value)
        if (res.data) {
          res.data.isNew = true
          this.selected_vendor.product_count = this.selected_vendor.product_count + 1
          this.selected_vendor.product.unshift(res.data)
          this.appService.openToast('New Product Added!')
        }



      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.productForm.reset()
        //this.closeModal()
      }


    }


    _navigateToTabView(tab_view) {
      this.router.navigate(
        [], {
          queryParams: {
            tab_view: tab_view
          },
          queryParamsHandling: 'merge'
        }
      );
    }

    openModal(modal) {
      this.showModal = modal
    }
    closeModal() {
      this.showModal = null
    }

  }
