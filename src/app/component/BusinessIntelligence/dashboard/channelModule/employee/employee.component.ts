import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faClock, faGraduationCap, faBookOpen, faBriefcase, faSort } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
  faClock= faClock
  faBriefcase=faBriefcase
  faGraduationCap=faGraduationCap
  faChevronDown = faChevronDown
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  faSort=faSort
  showModal: any;
  tab_view: string;
  employee: any;

  divisi = ['Account','Planner','Creative','KOL','Finance','HR','General Affair', 'Community','ECU']


  employeForm = new FormGroup({
    first_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    last_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    dob: new FormControl(),

    location: new FormControl(null),

    phone: new FormControl(null),

    position: new FormControl(null),

    email: new FormControl(null),

    status: new FormControl('new_applicant'),

  
  
    
    
  })
  
  params: string;

  employee_status: ({ id: string; label: string; value: string; name: string; class: string; } | { id: string; label: string; value: string; class: string; name?: undefined; })[];
  sortListItems: ({ name: string; items: ({ id: string; label: string; value: string; name: string; class: string; } | { id: string; label: string; value: string; class: string; name?: undefined; })[]; } | { name: string; items: ({ name: string; label: string; } | { name: string; label?: undefined; })[]; })[];
  sortkey: any;
  city_list: any[];
  page: number;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    private serializer: UrlSerializer,

  ) { }


  
  
  ngOnInit(): void {
    this.route.queryParamMap.subscribe(queryParams => {

       
      this.params = this.serializer.serialize(this.router.createUrlTree([''],
      { queryParams: this.route.snapshot.queryParams}))     
      this.sortkey=[]

      this.tab_view = queryParams.get("tab_view")
      this.getEmployee()
    })

  

    this.employee_status = [
      {
        'id' : 'status',
        'label' : 'New Application',
        'value' : 'new_applicant',
        'name' : 'new_applicant',
        'class' : 'paid',
      },
      {
        'id' : 'status',
        'label' : 'interviewed',
        'value' : 'interviewed',
        'class' : 'paid',
        'name' : 'interviewed',

      },
      {
        'id' : 'status',
        'label' : 'candidates',
        'value' : 'candidates',
        'class' : 'paid',
        'name' : 'candidates',

      },
    
      {
        'id' : 'status',
        'label' : 'On Board',
        'value' : 'on_board',
        'class' : 'paid',
        'name' : 'on_board',

      },
      {
        'id' : 'status',
        'label' : 'resign',
        'value' : 'resign',
        'class' : 'paid',
        'name' : 'resign',

      },
      {
        'id' : 'status',
        'label' : 'rejected',
        'value' : 'rejected',
        'class' : 'paid',
        'name' : 'rejected',

      },


      
    ]



       this.sortListItems = [
      {
       
        'name' : 'status',
        'items' : this.employee_status

      },
      {
       
        'name' : 'education',
        'items' : [
          {
            'name':'Master',
            'label' : 'Master',
          },
          {
            'name':'Diploma',
            'label' : 'Diploma',
          },
          {
            'name':'Senior',
            'label' : 'Senior',
          },
          {
            'name':'Junior',
            'label' : 'Junior',
          }
        
          
        ]

      },
      {
       
        'name' : 'division',
        'items' : [
          {
            'name':'Account',
            'label' : 'Account',
          },
          {
            'name':'Planner',
            'label' : 'Planner',
          },
          {
            'name':'Creative',
            'label' : 'Creative',
          },
          {
            'name':'KOL',
            'label' : 'KOL',
          },
          {
            'name':'Finance',
            'label' : 'Finance',
          },
          {
            'name':'General Affair',
            'label' : 'General Affair',
          },
          {
            'name':'Community',
            'label' : 'Community',
          },
          {
            'name':'ECU',
            'label' : 'ECU',
          }
        
        
        
          
        ]

      },
    ]


  }

  selectCity(e){
 
    this.employeForm.get('location').setValue(e.name)

  }


  async searchCity($event){
    console.log($event)
  
    this.city_list=[]
    try {
      this.appService.showLoading()
      let res = await this.webService.getCityList('10',$event)
   
      if(res.data){
        this.city_list = res.data.map(item => {
          return {
            id: item.city_id,
            label: item.city_name,
            name: item.city_name
          };
        });
  
      }
     


     
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
    
    
  }

  async getEmployee(){
    this.page = 1
    try {
      this.appService.showLoading()
      let res = await this.webService.getEmployee(this.params)
      this.employee = res.data
      this.employee.forEach(item => {
        item.total_performance = this._getPerformance(item.performance)
        item.age = this._getAge(item.dob)
      });

      console.log(res)
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    }
  }


  async loadMore() {
    console.log('load')
    this.page = this.page+1
    this.params = '?/'+this.params+'&page='+this.page
   
    console.log(this.params)
    try {
      this.appService.showLoading()
    


      let res = await this.webService.getEmployee(this.params)

    if(res.data){
      res.data.forEach(item => {
        item.total_performance = this._getPerformance(item.performance)
        item.age = this._getAge(item.dob)
      });

      this.employee = this.employee.concat(res.data)

    }
     
     
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.hideLoading()
  
    }
  }


  async updateEmployee(item,id){
    let form ={}
    form[item.id] = item.value
    console.log(item)
    if(id){
      try {
     
        let res = await this.webService.updateEmployee(form,id)
   
        console.log(res)
      
      } catch (error) {
        this.appService.openToast(error)
      }
      finally{
       this.appService.openToast('Updated!')
       this.getEmployee()
      }
    }
   
  }

  _getAge(dob){
    if(dob){
      var today = new Date()
      var receive = new Date(dob)
      let time = Math.round((today.getTime() - receive.getTime())/(1000*60*60*24*365));
     return 'Age '+time
    }
   
  }




  _getPerformance(performance){
    let total = 0
    performance.forEach(item => {
      total += parseFloat(item.performance_score) / 6
    });

    if( total > 67 && total <= 100){
      return 'best'
    }
    else if( total > 37 && total < 67){
      return 'good'
    }
    else if( total >0 && total< 37){
      return 'bad' 
    }
    else{
      return 'not set'
    }

  }

  async addEmployee(con){
    try {
      this.appService.showLoading()
      let res = await this.webService.addEmployee(this.employeForm.value)
  
      if(res.data.id){
        console.log(res.data)
       this.ngOnInit()
        if(con){
          this._navigateTo(res.data.id)
        }
        this.appService.openToast('New Employee Added!')
     
      }
    
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.employeForm.reset()
      this.appService.hideLoading()
      this.closeModal()
    }
  }

  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }

  _navigateTo(page) {
    this.router.navigate(
      ['/channel/employee/view/'+page]
    );
  }

  getPlaceholderName(item) {
    if(item.first_name && item.last_name){
      var first_name =item.first_name.charAt(0);
      var last_name =item.last_name.charAt(0);
      return first_name + last_name
    }
  
  }


}
