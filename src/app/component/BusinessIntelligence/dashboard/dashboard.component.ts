import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faCoins, faUsers, faDownload, faSquare, faClock, faCommentAlt, faEllipsisH, faPaperclip, faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class BIDashboardComponent implements OnInit{

  faCheckCircle=faCheckCircle

  faTimesCircle =faClock
  faClock= faClock
  faEllipsisH = faEllipsisH
  faChevronCircleRight= faChevronRight
  faChevronDown=faChevronDown
  faPaperclip=faPaperclip
  faCommentAlt=faCommentAlt
  faSquareFull = faSquare
  faUserPlus= faUserPlus
  calendar_view: any;
  showModal: any;
  show_button: any;
  selected_task: any;
  tab_view: string;
  show_sidebar: boolean;
  user_account: any;
  group: any;
  activity: any;
  task: any;
  selected_task_discussion: any;
  task_message: any;
  tab_menu_list: { id: string; label: string; class: any; }[];

  sortListItems = [
      
    
    {
     
      'name' : 'status',
      'items' :[
        {
          'name' :'pending',
          'label' :'pending'
        },
        {
          'name' :'processing',
          'label' :'processing'
        },
        {
          'name' :'done',
          'label' :'done',

        }
   
      ]

    },
    {
     
      'name' : 'priority',
      'items' :[
        {
          'name' :'low',
          'label' :'low'
        },
        {
          'name' :'medium',
          'label' :'medium'
        },
        {
          'name' :'high',
          'label' :'high',

        }
   
      ]

    }
   
  ]

  label = [
    {
      'id' : 'labels',
      'label' : 'creator',
      'name' : 'creator',
      'value' : 'creator',

    },
    {
      'id' : 'labels',
      'label' : 'client',
      'name' : 'client',
      'value' : 'client',
   
    },
    {
      'id' : 'labels',
      'label' : 'channel',
      'name' : 'channel',
      'value' : 'channel',
   
    },
    {  'id' : 'labels',
    
      'label' : 'community',
      'name' : 'community',
      'value' : 'community',
   
    },
    {
      'id' : 'labels',
      'label' : 'creative',
      'name' : 'creative',
      'value' : 'creative',
      

    },
 

    
  ]
  
  status = [
    {
      'id' : 'status',
      'label' : 'pending',
      'name' : 'pending',
      'value' : 'pending',

    },

    {
      'id' : 'status',
      'label' : 'processing',
      'name' : 'processing',
      'value' : 'processing',

    },

    {
      'id' : 'status',
      'label' : 'done',
      'name' : 'done',
      'value' : 'done',

    },
    {
      'id' : 'status',
      'label' : 'archive',
      'name' : 'archive',
      'value' : 'archive',

    },
    

    
  ]

  priority = [
    {
     
      'label' : 'low',
      'name' : 'low',
      'value' : 'low',
      'id' : 'priority',

    },
    {
     
      'label' : 'medium',
      'name' : 'medium',
      'value' : 'medium',
      'id' : 'priority',
   
    },
    {
    
      'label' : 'high',
      'name' : 'high',
      'value' : 'high',
      'id' : 'priority',
   
    },
    
 

    
  ]
  params: string;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    private serializer: UrlSerializer,
  ) {
  }

  async ngOnInit() {

    this.webService.userSessionSubject.subscribe(
      (res)=>{
     
        if(res){
          this.user_account = res

          
          this.route.queryParamMap.subscribe(queryParams => {
            this.tab_view = queryParams.get("tab_view")
      
            this.params = this.serializer.serialize(this.router.createUrlTree([''],
            { queryParams: this.route.snapshot.queryParams}))      
      
            if(this.tab_view ){
              this.getUserAccountTask(this.user_account?.id)
            }
      
           
      
            this.getUserGroupById(this.user_account.group_id)
            this.getUserAccountNotification(this.user_account.id)
          
      
          })
         
          
        }
      }
    )


   



    
   
    

   
    this.setComponentData()



  }

  download(link){
    window.open(link, '_blank');
  }


  async onChange(event: any) {
    const file: File = event.target.files[0];

    if (file) {
     

      const formData = new FormData();
  
      formData.append('file', file);

     
      
      try {
        this.appService.showLoading()
        let res = await this.webService.uploadPhoto(formData)
        console.log(res)
        if(res.data){
          let form = {
            'id' : 'attachment_url_1',
            'value' : res.data
          }


        
          this.updateTask(form)

        }
        
      } catch (error) {
        this.appService.openToast(error)
      }
      finally{
       
        this.appService.hideLoading()
      
      }

    }
  }


    async updateTask(item,id?){


      var id = id?id:this.selected_task.id
    let form ={}
    form[item.id] = item.value
    try {
        
      let response = await this.webService.updateUserTask(form,id)
       
      if(response.data){
        this.appService.openToast('Updated!')
        this.getUserAccountTask(this.user_account.id)
      }
       
        
        } catch (e) {
          
          this.appService.openToast(e)
      
          
          console.log(e)
        } finally {
       
        }
     }
   
    

  addAttachment(item){
    console.log(item)
  }


  async getUserAccountNotification(id){
    
    
    try {
     
      let response = await this.webService.getUserAccountNotification(id,'activity')
      this.activity = response.data
    console.log(response.data)
     
     } catch (e) {
       
       this.appService.openToast(e)
   
       
       console.log(e)
     } finally {
     
   
     }
  }

  async getUserAccountTask(id){
    
     

    try {
     
      let response = await this.webService.getUserAccountTask(id,this.params)
     console.log(response)
     this.task = response.data
    
     
     } catch (e) {
       
       this.appService.openToast(e)
   
       
       console.log(e)
     } finally {
     
   
     }
  }

  async selectTask(item){
    console.log(item)
   this.selected_task=item
 
    try {
     
      let response = await this.webService.getUserAccountTaskDiscussion(item.owner_id,item.id)
     console.log(response)
      this.selected_task_discussion = response.data

    
   
    
     
     } catch (e) {
       
       this.appService.openToast(e)
   
       
       console.log(e)
     } finally {
      this.openModal('taskDetailModal')
     }
  }

  async addTaskDiscussion(){
    let form = {
      'message' : this.task_message,
      'sender_id' : this.webService.account_id,
      'task_id' : this.selected_task.id
    }

    console.log(form)
   
      if(this.webService.account_id &&  this.selected_task.id){
        try {
          this.appService.showLoading()
          let response = await this.webService.addUserAccountTaskDiscussion(form)
        
        
       
        
         
         } catch (e) {
           
           this.appService.openToast(e)
       
           
           console.log(e)
         } finally {
          this.appService.hideLoading()
          this.task_message=null
            let response = await this.webService.getUserAccountTaskDiscussion(this.webService.account_id,this.selected_task.id)
            this.selected_task_discussion = response.data
       
         }
      }
  
     
   }
 
 
   



  getPlaceholderName(item) {

    var first_name =item.first_name.charAt(0);
    var last_name =item.last_name.charAt(0);
    return first_name + last_name
  }

  async  getUserGroupById(group_id){
   try {
        let res = await this.webService.getUserGroupById(group_id)
     
      this.group = res.data?.accounts

     
     } catch (e) {
       
       this.appService.openToast(e)
   
     } finally {
     
     }
  
  }


  setComponentData(){
    this.tab_menu_list = [
      {
        'id' : 'to_do',
        'label' : 'To Do',
      
        'class' : null,
      },
      {
        'id' : 'archive',
        'label' : 'Archive',
     
        'class' : null,
      },
   
      

    
    
     
     
    ]

  }



  showButton(a){
    console.log('dad')
    this.show_button = a
  }
  hideButton(){
    this.show_button = null
  }

  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }

  _navigateToTabView(tab_view) {
    this.router.navigate(
      [],
      { queryParams: { tab_view: tab_view},
      queryParamsHandling: 'merge' }
    );
  }


 
}
