import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreativeVendorComponent } from './creative-vendor.component';

describe('CreativeVendorComponent', () => {
  let component: CreativeVendorComponent;
  let fixture: ComponentFixture<CreativeVendorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreativeVendorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreativeVendorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
