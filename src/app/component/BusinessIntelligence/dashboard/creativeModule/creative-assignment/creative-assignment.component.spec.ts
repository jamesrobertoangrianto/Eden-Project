import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreativeAssignmentComponent } from './creative-assignment.component';

describe('CreativeAssignmentComponent', () => {
  let component: CreativeAssignmentComponent;
  let fixture: ComponentFixture<CreativeAssignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreativeAssignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreativeAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
