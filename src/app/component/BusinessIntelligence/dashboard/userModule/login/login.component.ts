import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faCheckCircle, faCircle, faCircleNotch } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  page_view: string;
  user_account: any;
  faCircle=faCircle
  faCircleNotch=faCircleNotch
  faCheckCircle=faCheckCircle
  
  domicile_list = [
    'Jakarta','Bogor','Depok','Tangerang - Banten', 'Bekasi','Jawa Barat','Jawa Tengah','Jawa Timur','Bali, NTT, NTB','Sumatra, Aceh, Riau','Kalimantan','Sulawesi','Papua & Lainnya'
  ]
  working_status = [
   
    {
      value : 'Pegawai Swasta',
      label:'Pegawai Swasta',
      selected : null
    },
    {
      value : 'PNS',
      label:'PNS',
     
    },
    {
      value : 'Wirausaha',
      label:'Wirausaha'
    },
    
    {
      value : 'Freelancer',
      label:'Freelancer'
    },
    {
      value : 'Mahasiswa',
      label:'Mahasiswa'
    },
    
    {
      value : 'Lainnya',
      label: 'Lainnya'
    },
    
   
  ]


  number_of_kids = [
   
    {
      value : 'Calon Ibu',
      label:'Calon Ibu'
    },
    {
      value : '1 - 2 Anak',
      label:'1 - 2 Anak'
    },
    {
      value : 'Lebih dari 2 Anak',
      label:'Lebih dari 2 Anak'
    },
    
   
  ]


  is_breast_feed = [
   
    {
      value : 'Yes',
      label:'Yes'
    },
    {
      value : 'No',
      label:'No',
     
    },
    
    
   
  ]

  field = [
    
    {
      value : 'Fitness',
      label:'Fitness'
    },
    {
      value : 'Health & Beauty',
      label:'Health & Beauty',
      selected : true
    },
    {
      value : 'Digital',
      label:'Digital'
    },
    {
      value : 'Home Decor',
      label:'Home Decor'
    },
    {
      value : 'Memasak/Kuliner',
      label:'Memasak/Kuliner'
    },
    {
      value : 'Lainnya',
      label: 'Lainnya'
    },
  
   
  ]

  information_source = [
    
    {
      value : 'Social Media',
      label:'Social Media'
    },
    {
      value : 'Teman',
      label:'Teman',
      selected : true
    },
    {
      value : 'WhatsApp Group',
      label:'WhatsApp Group'
    },
    {
      value : 'Komunitas',
      label:'Komunitas'
    },
    {
      value : 'Lainnya',
      label:'Lainnya'
    },
    
  
   
  ]

  mothly_expense = [
    
    {
      value : '>5 Juta',
      label:'>5 Juta'
    },
    {
      value : '3-5 Juta',
      label:'3-5 Juta',
      
    },
    {
      value : '1-2 Juta',
      label:'1-2 Juta'
    },
    {
      value : '< 1 Juta',
      label:'< 1 Juta'
    },
    
  
   
  ]
  
  loginForm = new FormGroup({
    email: new FormControl(null,{ 
      validators: [Validators.required, Validators.email, Validators.nullValidator]
    }),
    password_hash: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    session_id: new FormControl(null,{ 
     
    }),
  })

    
  clientLoginForm = new FormGroup({
    username: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    password: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

   
  })

  registerForm = new FormGroup({
    first_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    location: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    city: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    email: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    password: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    confirm_password: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    password_hash: new FormControl(null),

    dob: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    referal_code: new FormControl(null),

    


    working_status: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    number_of_kids: new FormControl(null),

    is_breast_feed: new FormControl(null),

    field: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
   
    information_source: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

      
    status: new FormControl('new_request'),

  
   
    mothly_expense: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    what_to_expect: new FormControl(null),


   

    instagram_account: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    instagram_account_link: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    instagram_follower: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    phone: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    community_id: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

   
   
  })
  

  sessions: any;
  login_view: string;
  register_view: string;
  city_list: any[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    
  ) { }

  ngOnInit(): void {
    // this.webService.userSessionSubject.subscribe(
    //   (res)=>{
    //     console.log(res)
    //   }
    // )

    this.register_view = 'auth'
    this.login_view = this.router.url

    console.log(this.login_view)
    this.sessions = this.encript(navigator.userAgent)
    this.route.queryParamMap.subscribe(queryParams => {

      this.page_view = queryParams.get("page_view")
      
      if(!this.page_view){
        this.page_view = 'login'
      }

      
    })
    
  }

  encript(data){
    return data.split("").reduce(function(a, b) {
      a = ((a << 5) - a) + b.charCodeAt(0);
      return a & a;
    }, 0);
  }

  validateRegister(){
    if(this.registerForm.get('email').valid
    && this.registerForm.get('password').valid
    && this.registerForm.get('confirm_password').valid 
    && this._validateConfirmPassword()

    

    ){
      return true
    }else{
      return false
    }
  }

  checkCustomField(item,e,id){
   console.log(this.registerForm.get(id))
  }

  selectOption(item,array,id){
    console.log(item)

    array.map(item => {
      item.selected =  null
    });

    item.selected = item.value
    this.registerForm.get(id).setValue(item.value)

    
  }

  addCustomData(e,id){
    this.registerForm.get(id).setValue(e.target.value)
    console.log(e.target.value)
  }

  getCommunityName(){
    let id = this.registerForm.get('community_id').value
      if(id == 1){
     

      

        return 'Mak Pintar'
      }else if(id == 2){
       
        return 'Pekerja Pintar'
      }
   

  }


  _validateConfirmPassword(){
    if(this.registerForm.get('password').value && this.registerForm.get('confirm_password').value ){
      if(this.registerForm.get('password').value == this.registerForm.get('confirm_password').value){
        return true
      }else{
        return false
      }
    }
    else{
      return true

    }
   
  }

  async searchCity($event){
    console.log($event)
    this.city_list=[]
    try {
      this.appService.showLoading()
      let res = await this.webService.getCityList('6',$event)
      
      if(res.data){
        this.city_list = res.data.map(item => {
          return {
            id: item.city_id,
            label: item.city_name,
            name: item.city_name
          };
        });
  
      }
     


     
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
    
    
  }

  
  selectCity(e){
 
    this.registerForm.get('location').setValue(e.name)

  }



  public numbersOnlyValidator(event: any) {
    const pattern = /^[0-9\-]*$/;
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^0-9\-]/g, "");
    }
  }
  async userLogin(){

    if(this.loginForm.valid){
      let session_hash = this.encript(this.loginForm.get('email').value.toLowerCase())+this.sessions
      this.loginForm.get('password_hash').patchValue(this.encript(this.loginForm.get('password_hash').value))
      this.loginForm.get('session_id').patchValue(session_hash)
    
   
     

      console.log(this.loginForm.value)

      try {
        this.appService.showLoading()
        let response = await this.webService.AccountLogin(this.loginForm.value)
        console.log(response.data)
        this.router.navigate(
          ['/']
        );
        this.loginForm.reset();
      
      
       } catch (e) {
         
         this.appService.openToast(e)
         this.loginForm.reset();
     
         
         console.log(e)
       } finally {
         this.appService.hideLoading()
       }
    }else{
      this.loginForm.reset();
      this.appService.openToast('Please input correct information')
    }
   
        
      
  }


  

  async clientLogin(){
   

    try {
      this.appService.showLoading()
      let response = await this.webService.clientLogin(this.clientLoginForm.value)
      console.log(response.data)
     
      this.clientLoginForm.reset();
      if(response.data.creator_id){
        this.router.navigate(
          ['client-area/creator-account/view/'+response.data.creator_id]
        );
      }
      else if(response.data.community_member_id){
        this.router.navigate(
          ['client-area/community-account/view/'+response.data.community_member_id]
        );
      }
      else if(response.data.vendor_id){
        this.router.navigate(
          ['client-area/channel-account/view/'+response.data.vendor_id]
        );
      }

      else if(response.data.creator_management_id){
        this.router.navigate(
          ['client-area/management-account/view/'+response.data.creator_management_id]
        );
        console.log('management')
      }
    
     } catch (e) {
       
       this.appService.openToast(e)
       this.clientLoginForm.reset();
   
       
       console.log(e)
     } finally {
       this.appService.hideLoading()
     }
        
      
  }


  async clientRegister(){
   
    console.log(this.registerForm.value)
    try {
      this.appService.showLoading()
      // let response = await this.webService.clientLogin(this.clientLoginForm.value)
      // console.log(response.data)
     
    
    
     } catch (e) {
       
       this.appService.openToast(e)
       this.clientLoginForm.reset();
   
       
       console.log(e)
     } finally {
       this.appService.hideLoading()
     }
        
      
  }

  async addCommunityMember(){
   
    this.registerForm.get('password_hash').patchValue(this.registerForm.get('password').value)
    console.log(this.registerForm.value)

    try {
      this.appService.showLoading()
      let res = await this.webService.addCommunityMember(this.registerForm.value)
     console.log(res)
    
     if(res.auth){
      this.clientLoginForm.get('username').setValue(res.auth.username)
      this.clientLoginForm.get('password').setValue(res.auth.password)
      //this.clientLogin()
      console.log('here')
      console.log(this.clientLoginForm.value)
      this.clientLogin()
     }
     
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
     this.registerForm.reset()
    //  this.closeModal()
    }
  }



  async checkAuth(){
   
   

    try {
      this.appService.showLoading()
      let res = await this.webService.getClientAuthByEmail(this.registerForm.get('email').value)
     console.log(res)
     if(res.data == null){
      this.register_view ='form_data'

      if(this.registerForm.get('community_id').value == 1){
        this.registerForm.controls["number_of_kids"].setValidators([Validators.required])
        this.registerForm.controls["is_breast_feed"].setValidators([Validators.required])
  
        this.registerForm.controls["number_of_kids"].updateValueAndValidity()
        this.registerForm.controls["is_breast_feed"].updateValueAndValidity()

        this.working_status = [
   
          {
            value : 'IRT',
            label:'IRT'
          },
          {
            value : 'Pegawai',
            label:'Pegawai',
            
          },
          {
            value : 'Wirausaha',
            label:'Wirausaha'
          },
        
          {
            value : 'Lainnya',
            label:'Lainnya'
          },
          
         
        ]
      
  
      }
     
      
      
     }else{
      this.appService.openToast('Email yang Anda masukan sudah terdaftar!')

     }
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
     // this.registerForm.reset()
    //  this.closeModal()
    }
  }


  _navigateToPage(user) {
    if(user.account_type == 'employee'){
      this.router.navigate(
        ['/']
      );
    }else if(user.account_type == 'community_member'){
      this.router.navigate(
        ['client-area/community-member']
      );
    }
    else if(user.account_type == 'creator'){
      this.router.navigate(
        ['client-area/creator']
      );
    }
   
  }

  _navigateTo(page) {
    this.router.navigate(
      [],
      { queryParams: { page_view: page} }
    );
  }

  async openDialog(){
    this.appService.hideLoading()
  //   this.appService.openDialog("Login","Please login to continue","Submit", async ()=>{
  //     // await this.test()
  //  })
  }

  async openToast(){
    this.appService.openToast("Please login to continue","Submit", async ()=>{
      // await this.test()
   })
  }


}