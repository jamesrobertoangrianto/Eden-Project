import { Component, OnInit } from '@angular/core';
import { faCheck, faTimes } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-user-role',
  templateUrl: './user-role.component.html',
  styleUrls: ['./user-role.component.scss']
})
export class UserRoleComponent implements OnInit {
  faTimes=faTimes
  faCheck=faCheck
  active_modal: any;
  user_role_list: any;
  user_role: any;

  constructor(
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

  ngOnInit(): void {
    this.getUserRole()
  }
  toggleModal(modal_name){
    if(this.active_modal){
      this.active_modal = false
    }
    else{
      this.active_modal = modal_name
    }
    
  }






  async getUserRole(){
    try {
    
      // this.appService.showLoading()
        let res = await this.webService.getUserRole()
       

      this.user_role = res.data
      console.log(res)
    
    
     } catch (e) {
       
       this.appService.openToast(e)
   
       
       console.log(e)
     } finally {
     
   //    this.appService.hideLoading()
     }
  
  }


}
