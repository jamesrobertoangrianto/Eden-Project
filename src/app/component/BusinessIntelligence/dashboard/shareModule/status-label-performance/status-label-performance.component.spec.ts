import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusLabelPerformanceComponent } from './status-label-performance.component';

describe('StatusLabelPerformanceComponent', () => {
  let component: StatusLabelPerformanceComponent;
  let fixture: ComponentFixture<StatusLabelPerformanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusLabelPerformanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusLabelPerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
