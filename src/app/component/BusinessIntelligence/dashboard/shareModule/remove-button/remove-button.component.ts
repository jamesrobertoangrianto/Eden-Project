import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-remove-button',
  templateUrl: './remove-button.component.html',
  styleUrls: ['./remove-button.component.scss']
})
export class RemoveButtonComponent implements OnInit {
  @Output() onClick = new EventEmitter()
  @Input() message :any
  isShow: boolean;
  constructor() { }

  ngOnInit(): void {
  }

  confirm(data){
    if(data){
      this.onClick.emit()
      this.toggle()
    
    }else{
      this.toggle()
    }
 
  }

  toggle(){
    this.isShow = !this.isShow
  }
}
