import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopNavigationClientComponent } from './top-navigation-client.component';

describe('TopNavigationClientComponent', () => {
  let component: TopNavigationClientComponent;
  let fixture: ComponentFixture<TopNavigationClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopNavigationClientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopNavigationClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
