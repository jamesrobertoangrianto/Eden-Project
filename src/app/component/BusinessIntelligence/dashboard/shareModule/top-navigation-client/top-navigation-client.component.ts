import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faCheckCircle, faInbox, faBell, faHome, faHamburger, faArrowCircleRight, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-top-navigation-client',
  templateUrl: './top-navigation-client.component.html',
  styleUrls: ['./top-navigation-client.component.scss']
})
export class TopNavigationClientComponent implements OnInit {
  faSignOutAlt=faSignOutAlt
  faInbox=faInbox
  faArrowCircleRight=faArrowCircleRight
  faHamburger=faHamburger
  faTasks=faCheckCircle
  faHome=faHome
  faBell=faBell
  user_account: any;
  notification: any;
  showModal: any;
  inital_name: any;
  notification_count: any;
  client_account: any;
  constructor(
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.webService.clientSessionSubject.subscribe(
      (res)=>{
       
     console.log(res)
      }
    )

  
     let client_session = this.webService.getClientSession()

    //  this.playSound('https://assets.mixkit.co/active_storage/sfx/2870/2870-preview.mp3')
  }





      

  playSound(url) {
    const audio = new Audio(url);
    audio.play();
  }

  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }

  
  async userLogout(){

    
      try {
       
        let response = await this.webService.clientLogout()
       
        this.router.navigate(
          ['/client/login']
        );

         
       
       } catch (e) {
         
         this.appService.openToast(e)
     
         
         console.log(e)
       } finally {
        this.user_account = null
        this.appService.openToast('Logout Success')
     
       }
    }
        

}
