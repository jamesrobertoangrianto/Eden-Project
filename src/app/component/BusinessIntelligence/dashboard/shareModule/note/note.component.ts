import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faPhone, faEnvelope, faMapPin, faChevronDown, faDotCircle, faChevronCircleRight, faChevronRight, faColumns, faExpandAlt, faExpandArrowsAlt, faExpand, faTrash } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit {
  @Input() note: any
  @Input() id: any
  @Input() value: any
  @Output() afterSubmit = new EventEmitter()

  faTrash=faTrash


  description: any;
  showMiniModal: any;
  showModal: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    private serializer: UrlSerializer,
  ) { 
    
  }

  ngOnInit(): void {
    console.log(this.note)
  }


  
  async addNote(){

   let form = {
    // 'description':this.description,
    'account_id' : this.webService.account_id
   }
   form[this.id] = this.value
    try {
     
      let response = await this.webService.addNote(form)
      console.log(response)
      this.description = null
     
     } catch (e) {
       
       this.appService.openToast(e)
   
       
       console.log(e)
     } finally {
      this.closeModal()
      this.afterSubmit.emit(true)
   
     }
  }

  async updateNote(id,item){

    let form = {
     'description':item.value,
    
    }

     try {
      
       let response = await this.webService.updateNote(id,form)
       console.log(response)
       this.description = null
      
      } catch (e) {
        
        this.appService.openToast(e)
    
        
        console.log(e)
      } finally {
       this.closeModal()
       this.afterSubmit.emit(true)
    
      }
   }
 


  async removeNote(id){


     try {
      
       let response = await this.webService.removeNote(id)
       console.log(response)
       this.description = null
      
      } catch (e) {
        
        this.appService.openToast(e)
    
        
        console.log(e)
      } finally {
       this.closeModal()
       this.afterSubmit.emit(true)
    
      }
   }
 

  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }

}
