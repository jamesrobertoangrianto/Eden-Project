import { Component, OnInit } from '@angular/core';
import { faBell } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-top-navigation-newsfeed',
  templateUrl: './top-navigation-newsfeed.component.html',
  styleUrls: ['./top-navigation-newsfeed.component.scss']
})
export class TopNavigationNewsfeedComponent implements OnInit {
  faBell=faBell
  tab_menu_list: {}[];
  isShowlogin: boolean;
  author_account: any;
  session_id: any;
  isHideLogin: boolean;
  constructor(
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

  top_link =[
    {
      'id':'',
      'label':'Home',
      'link':'/newsfeed/'
    },
    {
      'id':'',
      'label':'Guidelines',
      'link':'/newsfeed/guidelines'
    },
    {
      'id':'',
      'label':'Contact',
      'link':'/newsfeed/contact'
    }
  ]

  ngOnInit(): void {
  this.isHideLogin = true
   this.isShowlogin = false
   setTimeout(()=>{                           // <<<---using ()=> syntax
    this.webService.authorSessionSubject.subscribe(
      (res)=>{
       
       
        if(res){
          this.author_account = res
      
            }else{
          this.author_account = null
        }
      }
    )
    this.isHideLogin = false
  }, 200);  

  
  
  }

 


  encript(data){
    return data.split("").reduce(function(a, b) {
      a = ((a << 5) - a) + b.charCodeAt(0);
      return a & a;
    }, 0);
  }
  async authorLogout(){

    
    try {
     
      let response = await this.webService.authorLogout()
     console.log(response)
    // window.location.reload();
       
     
     } catch (e) {
       
       this.appService.openToast(e)
   
       
       console.log(e)
     } finally {
    
      this.appService.openToast('Logout Success')
   
     }
  }
      

  toggleLogin(){
    this.isShowlogin = !this.isShowlogin
  }

}
