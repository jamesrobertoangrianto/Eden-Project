import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopNavigationNewsfeedComponent } from './top-navigation-newsfeed.component';

describe('TopNavigationNewsfeedComponent', () => {
  let component: TopNavigationNewsfeedComponent;
  let fixture: ComponentFixture<TopNavigationNewsfeedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopNavigationNewsfeedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopNavigationNewsfeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
