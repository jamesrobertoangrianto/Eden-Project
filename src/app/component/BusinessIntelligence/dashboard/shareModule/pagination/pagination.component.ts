import { Component, OnInit } from '@angular/core';
import { faChevronRight , faChevronLeft} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  faChevronRight=faChevronRight
  faChevronLeft=faChevronLeft
  constructor() { }

  ngOnInit(): void {
  }

}
