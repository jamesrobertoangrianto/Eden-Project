import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faCoins, faUsers, faDownload, faSquare, faClock, faCommentAlt, faEllipsisH, faPaperclip } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  faUserPlus = faUserPlus
  task ={
    "title": null,
    "description": null,
    "labels": null,
    "priority": null,
    "owner_id": 1,
    "member" : []
  }



   

  label = [
    {
     
      'label' : 'creator',
      'name' : 'creator',
      'value' : 'creator',

    },
    {
     
      'label' : 'client',
      'name' : 'client',
      'value' : 'client',
   
    },
    {
    
      'label' : 'channel',
      'name' : 'channel',
      'value' : 'channel',
   
    },
    {
    
      'label' : 'community',
      'name' : 'community',
      'value' : 'community',
   
    },
    {
    
      'label' : 'creative',
      'name' : 'creative',
      'value' : 'creative',

    },
 

    
  ]
  

  priority = [
    {
     
      'label' : 'low',
      'name' : 'low',
      'value' : 'low',
      'id' : 'priority',

    },
    {
     
      'label' : 'medium',
      'name' : 'medium',
      'value' : 'medium',
      'id' : 'priority',
   
    },
    {
    
      'label' : 'high',
      'name' : 'high',
      'value' : 'high',
      'id' : 'priority',
   
    },
 

    
  ]
  user_account: any;
  selected_members: any[];
  showModal: any;
  @Output() onClose = new EventEmitter()
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

  ngOnInit(): void {
  
    this.getUserAccount()
  }

  async getUserAccount(){
    try {
      let res = await this.webService.getUserAccount()
      this.user_account = res.data
      console.log(this.user_account)
    } catch (error) {
      
    }
  }

  addMember(item){
    this.task.member.push(item)
    this.closeModal()
  }

  closeModal(){
    this.showModal = false
  }


  openModal(modal){
    this.showModal = modal
  }

  getPlaceholderName(item) {

    var first_name =item.first_name.charAt(0);
    var last_name =item.last_name.charAt(0);
    return first_name + last_name
  }

  setLabel(item){
    this.task.labels = item.name

    console.log(this.task)
    this.closeModal()
  }
  setPriority(item){
    this.task.priority = item.name
    this.closeModal()
  }

  async addUserAccountTask(){
    this.task.owner_id = this.webService.account_id
    console.log(this.task)
    
    try {
     
      let response = await this.webService.addUserAccountTask(this.task)
      console.log(response)
      if(response.data){
        this.appService.openToast('Task Added!')
      }
    
     
     } catch (e) {
       
       this.appService.openToast(e)
   
       
       console.log(e)
     } finally {
     
      this.task = null
      this.onClose.emit(false)
        await this.webService.getUserSession( this.webService.getSessionId());

     }
  }
}
