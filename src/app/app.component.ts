import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ManualOrderService } from './services/manual-order/manual-order.service';





@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  deviceInfo: any;
  session_id: any;
  page_view: string;




  constructor(
 
    private webService : ManualOrderService,
    private router: Router,
    
    

  ) {

  }

  ngOnInit(): void {
  
   this.validateAuthorLogin()
    

    
  }

  async validateAuthorLogin(){
    console.log('val')
    this.session_id = this.encript(navigator.userAgent)
    console.log(this.session_id)
    try {
    
      let response =  await this.webService.getAuthorSession( this.webService.getAuthorSessionId());
      // console.log(response)
      if(response){
        let session_hash = this.encript(response.data.email.toLowerCase()) + this.session_id
       
        if(parseFloat(response.data.session_id) !== parseFloat(session_hash)){
       this.webService.authorLogout()
        }
  
      }else{
       // this.router.navigate( ['/login'] );
       console.log('author log')
      }
   
      
       
     
     } catch (e) {
       
     
       
       console.log(e)
     } finally {
     
   
     }
     
   
  }


  encript(data){
    return data.split("").reduce(function(a, b) {
      a = ((a << 5) - a) + b.charCodeAt(0);
      return a & a;
    }, 0);
  }

  
}
