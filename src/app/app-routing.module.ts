import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BIDashboardComponent } from './component/BusinessIntelligence/dashboard/dashboard.component';
import { LoginComponent } from './component/BusinessIntelligence/dashboard/userModule/login/login.component';
import { UserAccountComponent } from './component/BusinessIntelligence/dashboard/userModule/user-account/user-account.component';
import { UserRoleComponent } from './component/BusinessIntelligence/dashboard/userModule/user-role/user-role.component';
import { ProfileComponent } from './component/BusinessIntelligence/dashboard/userModule/profile/profile.component';
import { CampaignComponent } from './component/BusinessIntelligence/dashboard/creatorModule/campaign/campaign.component';
import { ContractComponent } from './component/BusinessIntelligence/dashboard/creatorModule/contract/contract.component';
import { CampaignViewComponent } from './component/BusinessIntelligence/dashboard/creatorModule/campaign/campaign-view/campaign-view.component';
import { SalesPipelineComponent } from './component/BusinessIntelligence/dashboard/clientModule/sales-pipeline/sales-pipeline.component';
import { SalesPipelineViewComponent } from './component/BusinessIntelligence/dashboard/clientModule/sales-pipeline/sales-pipeline-view/sales-pipeline-view.component';
import { ClientComponent } from './component/BusinessIntelligence/dashboard/clientModule/client/client.component';
import { NewsfeedBackendComponent } from './component/BusinessIntelligence/dashboard/clientModule/newsfeed-backend/newsfeed-backend.component';
import { ChannelVendorComponent } from './component/BusinessIntelligence/dashboard/channelModule/channel-vendor/channel-vendor.component';
import { InventoryComponent } from './component/BusinessIntelligence/dashboard/channelModule/inventory/inventory.component';
import { EmployeeComponent } from './component/BusinessIntelligence/dashboard/channelModule/employee/employee.component';
import { CommunityMemberComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-member/community-member.component';
import { CommunityCampaignComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-campaign/community-campaign.component';
import { CommunityRewardsComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-rewards/community-rewards.component';
import { CommunityTicketComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-ticket/community-ticket.component';
import { AssetsComponent } from './component/BusinessIntelligence/dashboard/channelModule/assets/assets.component';
import { CommunityTicketViewComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-ticket/community-ticket-view/community-ticket-view.component';
import { CreativeVendorComponent } from './component/BusinessIntelligence/dashboard/creativeModule/creative-vendor/creative-vendor.component';
import { CommunityPointsComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-points/community-points.component';
import { CommunityAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/community-account/community-account.component';
import { CreatorAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/creator-account/creator-account.component';
import { CreativeAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/creative-account/creative-account.component';
import { ChannelAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/channel-account/channel-account.component';
import { GeneralContactComponent } from './component/BusinessIntelligence/dashboard/userModule/general-contact/general-contact.component';
import { CreatorViewComponent } from './component/BusinessIntelligence/dashboard/creatorModule/creator/creator-view/creator-view.component';
import { CreatorComponent } from './component/BusinessIntelligence/dashboard/creatorModule/creator/creator.component';
import { EmployeeViewComponent } from './component/BusinessIntelligence/dashboard/channelModule/employee/employee-view/employee-view.component';
import { CommunityMemberViewComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-member/community-member-view/community-member-view.component';
import { CommunityCampaignViewComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-campaign/community-campaign-view/community-campaign-view.component';
import { NewsfeedPostViewComponent } from './component/BusinessIntelligence/dashboard/clientModule/newsfeed-backend/newsfeed-post-view/newsfeed-post-view.component';
import { NewsHomepageComponent } from './component/BusinessIntelligence/dashboard/newsfeedModule/news-homepage/news-homepage.component';
import { NewsContactComponent } from './component/BusinessIntelligence/dashboard/newsfeedModule/news-contact/news-contact.component';
import { NewsGuidelinesComponent } from './component/BusinessIntelligence/dashboard/newsfeedModule/news-guidelines/news-guidelines.component';
import { NewsTermConditionsComponent } from './component/BusinessIntelligence/dashboard/newsfeedModule/news-term-conditions/news-term-conditions.component';
import { NewsPrivacyComponent } from './component/BusinessIntelligence/dashboard/newsfeedModule/news-privacy/news-privacy.component';
import { NewsAboutComponent } from './component/BusinessIntelligence/dashboard/newsfeedModule/news-about/news-about.component';
import { ManagementAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/management-account/management-account.component';
import { ClientLoginComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/client-login/client-login.component';
import { AuthGuard } from './services/auth-guard.service';
import { AuthGuardClientService } from './services/auth-guard-client.service';


const routes: Routes = [

  {
    path: '',
    component: BIDashboardComponent,
    canActivate: [AuthGuard],

  },
 



  {
    path: 'newsfeed',children:[
      {
        path:'',
        component: NewsHomepageComponent,
      },
      {
        path:'post/:id',
        component: NewsHomepageComponent,
       
      },
      {
        path:'post/:id/:slug',
        component: NewsHomepageComponent,
       
      },
      {
        path:'contact',
        component: NewsContactComponent,
       
      },
      {
        path:'guidelines',
        component: NewsGuidelinesComponent,
       
      },
      {
        path:'term-conditions',
        component: NewsTermConditionsComponent,
       
      },
      {
        path:'about',
        component: NewsAboutComponent,
       
      },
      {
        path:'privacy',
        component: NewsPrivacyComponent,
       
      },
      
    
    ]
    
  },

  
   
    {
      path: 'login',children:[
        {
          path:'',
          component: LoginComponent,
         
        },
      
      ]
      
    },
    {
      path: 'client/login',children:[
        {
          path:'',
          component: ClientLoginComponent,

         
        },
      
      ]
      
    },
    {
      path: 'client/register',children:[
        {
          path:'',
          component: ClientLoginComponent,
          


         
        },
      
      ]
      
    },

    {
      path: 'user/contact',children:[
        {
          path:'',
          component: GeneralContactComponent,
          canActivate: [AuthGuard]

         
        },
      
      ],
      
    },

    {
      path: 'user/user-account',children:[
        {
          path:'',
          component: UserAccountComponent,
          canActivate: [AuthGuard]

         
        },
      
      ],
      
    },
    {
      path: 'user/user-role',children:[
        {
          path:'',
          component: UserRoleComponent,
          canActivate: [AuthGuard]

         
         
        },
      
      ]
      
    },

    {
      path: 'user/profile',children:[
        {
          path:'',
          component: ProfileComponent,
          canActivate: [AuthGuard]

         
        },
      
      ]
      
    },

    {
      path: 'creator/campaign',children:[
        {
          path:'',
          component: CampaignComponent,
          canActivate: [AuthGuard]
        },
        {
          path:'view/:id',
          component: CampaignViewComponent,
          canActivate: [AuthGuard]
         
        }
        
      
      ]
      
    },

    {
      path: 'creator/creator',children:[
        {
          path:'',
          component: CreatorComponent,
          canActivate: [AuthGuard]
        },
        {
          path:'view/:id',
          component: CreatorViewComponent,
          canActivate: [AuthGuard]
         
        }
        
      
      ]
      
    },
   
    {
      path: 'creator/contract',children:[
        {
          path:'',
          component: ContractComponent,
          canActivate: [AuthGuard]
         
        },
     
      
      ]
      
    },
 

    {
      path: 'client/aquisition',children:[
        {
          path:'',
          component: SalesPipelineComponent,
          canActivate: [AuthGuard]

        },
        {
          path:'view/:id',
          component: SalesPipelineViewComponent,
          canActivate: [AuthGuard]

         
        }
        
      
      ]
      
    },

    {
      path: 'client/client',children:[
        {
          path:'',
          component: ClientComponent,
          canActivate: [AuthGuard]

        },
      
      
      ]
      
    },
    {
      path: 'client/newsfeed',children:[
        {
          path:'',
          component: NewsfeedBackendComponent,
          canActivate: [AuthGuard]

        },
        {
          path:'view/:id',
          component: NewsfeedPostViewComponent,
          canActivate: [AuthGuard]

         
        }
      
      
      
      ]
      
    },

    {
      path: 'channel/vendor',children:[
        {
          path:'',
          component: ChannelVendorComponent,
          canActivate: [AuthGuard]

        },
      
      
      ]
      
    },


    {
      path: 'channel/inventory',children:[
        {
          path:'',
          component: InventoryComponent,
          canActivate: [AuthGuard]

        },
      
      
      ]
      
    },

    {
      path: 'channel/assets',children:[
        {
          path:'',
          component: AssetsComponent,
          canActivate: [AuthGuard]

        },
      
      
      ]
      
    },


    {
      path: 'channel/employee',children:[
        {
          path:'',
          component: EmployeeComponent,
          canActivate: [AuthGuard]

        },
        {
          path:'view/:id',
          component: EmployeeViewComponent,
          canActivate: [AuthGuard]

         
        }
      
      
      ]
      
    },


  


    {
      path: 'community/member',children:[
        {
          path:'',
          component: CommunityMemberComponent,
          canActivate: [AuthGuard]

        },
        {
          path:'view/:id',
          component: CommunityMemberViewComponent,
          canActivate: [AuthGuard]

         
        }
      
      
      ]
      
    },


    {
      path: 'community/campaign',children:[
        {
          path:'',
          component: CommunityCampaignComponent,
          canActivate: [AuthGuard]

        },
        {
          path:'view/:id',
          component: CommunityCampaignViewComponent,
          canActivate: [AuthGuard]

         
        }
      
      
      
      ]
      
    },

    {
      path: 'community/rewards',children:[
        {
          path:'',
          component: CommunityRewardsComponent,
          canActivate: [AuthGuard]

        },
      
      
      ]
      
    },

    {
      path: 'community/points',children:[
        {
          path:'',
          component: CommunityPointsComponent,
          canActivate: [AuthGuard]

        },
      
      
      ]
      
    },

    

    {
      path: 'community/ticket',children:[
        {
          path:'',
          component: CommunityTicketComponent,
          canActivate: [AuthGuard]

        },
        {
          path:'view/:id',
          component: CommunityTicketViewComponent,
          canActivate: [AuthGuard]

         
        }
        
      
      
      ]
      
    },


    {
      path: 'creative/vendor',children:[
        {
          path:'',
          component: CreativeVendorComponent,
          canActivate: [AuthGuard]

        },
      
      
      ]
      
    },


    {
      path: 'client-area/community-account',children:[
        {
          path:'',
          component: CommunityAccountComponent,
          canActivate: [AuthGuardClientService]

        },
        {
          path:'view/:id',
          component: CommunityAccountComponent,
          canActivate: [AuthGuardClientService]


         
        }
      
      
      ]
      
    },

    {
      path: 'client-area/creator-account',children:[
        {
          path:'',
          component: CreatorAccountComponent,
          canActivate: [AuthGuardClientService]


        },
        {
          path:'view/:id',
          component: CreatorAccountComponent,
          canActivate: [AuthGuardClientService]


         
        }
      
      
      ]
      
    },
    {
      path: 'client-area/management-account',children:[
        {
          path:'',
          component: ManagementAccountComponent,
          canActivate: [AuthGuardClientService]


        },
        {
          path:'view/:id',
          component: ManagementAccountComponent,
          canActivate: [AuthGuardClientService]


         
        }
      
      
      ]
      
    },

    {
      path: 'client-area/channel-account',children:[
        {
          path:'',
          component: ChannelAccountComponent,
          canActivate: [AuthGuardClientService]


        },
        {
          path:'view/:id',
          component: ChannelAccountComponent,
          canActivate: [AuthGuardClientService]


         
        }
      
      
      ]
      
    },

    {
      path: 'client-area/creative-account',children:[
        {
          path:'',
          component: CreativeAccountComponent,
          canActivate: [AuthGuardClientService]


        },
        {
          path:'view/:id',
          component: CreativeAccountComponent,
          canActivate: [AuthGuardClientService]


         
        }
      
      
      ]
      
    },

   





    
    
   
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
