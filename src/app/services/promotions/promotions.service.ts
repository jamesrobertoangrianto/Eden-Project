import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PromotionsService {
  private link="https://wia.id/wia-module/promotion"

  constructor(
    private http: HttpClient
  ) { }

  getAPromotion(key){
    return this.http.get(`https://wia.id/wia-module/promotion?promotion_identifier=${key}`)
  }



  async updatePromotionDetails(formvalue){
    //console.log(formvalue)
    let res = await fetch(environment.APIBaseURL+'/promotion/updatePromotionById/',{
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    return data
  }

  async addPromotion(){
    //console.log(formvalue)
    let res = await fetch(environment.APIBaseURL+'/promotion/addPromotion/',{
      method: "POST",
     
    })
    let data = await res.json()
    return data
  }

  async getPromotionById(id){
    
    let res = await fetch(environment.APIBaseURL+'/promotion/getPromotionById/id/'+id,{
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  

  async updatePromotionPicture(body){
    //console.log(formvalue)
    let res = await fetch(environment.APIBaseURL+'/promotion/upload/',{
      method: "POST",
      body: JSON.stringify(body)
     
    })
    let data = await res.json()
    return data
  }


  
  async deletePromotion(body){
    //console.log(formvalue)
    let res = await fetch(environment.APIBaseURL+'/promotion/deletePromotion/',{
      method: "POST",
      body: JSON.stringify(body)
     
    })
    let data = await res.json()
    return data
  }


}
