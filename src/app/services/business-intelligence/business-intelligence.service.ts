import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment'
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})

export class BusinessIntelligenceService {
  apiURL = "https://wia.id/wia-module/Api/"


  public loadingToastSubject = new BehaviorSubject(null)

  dialogEvent: EventEmitter<any> = new EventEmitter()
  toastEvent: EventEmitter<any> = new EventEmitter()
  navigation_menu: any
  persona: { id: number; label: string; name: string; }[];


  constructor(
    private http: HttpClient,
    private router: Router,
  ) { 
    this.persona = [
      {
        'id' : 1,
        'label' :'Lifestyle',
        'name' :'Lifestyle'
      
      },
      {
        'id' : 2,
        'label' :'Beauty',
        'name' :'Beauty'
      
      },
      {
        'id' : 3,
        'label' :'Fashion',
        'name' :'Fashion'
      
      },
      {
        'id' : 4,
        'label' :'Moms',
        'name' :'Moms'
      
      },
      {
        'id' : 5,
        'label' :'Comedian',
        'name' :'Comedian'
      
      },
      {
        'id' : 6,
        'label' :'Foodies',
        'name' :'Foodies'
      
      },
      {
        'id' : 7,
        'label' :'Actor',
        'name' :'Actor'
      
      },
      {
        'id' : 8,
        'label' :'Actress',
        'name' :'Actress'
      
      },
      {
        'id' : 9,
        'label' :'Model',
        'name' :'Model'
      
      },
      {
        'id' : 10,
        'label' :'Doctor',
        'name' :'Doctor'
      
      },
      {
        'id' : 11,
        'label' :'Host & News Anchor',
        'name' :'Host & News Anchor'
      
      },

      {
        'id' : 12,
        'label' :'Traveller',
        'name' :'Traveller'
      
      },
      {
        'id' : 13,
        'label' :'Healthy Lifestyle',
        'name' :'Healthy Lifestyle'
      
      },
      {
        'id' : 14,
        'label' :'Content Creator',
        'name' :'Content Creator'
      
      },
      {
        'id' : 15,
        'label' :'Musician',
        'name' :'Musician'
      
      },
      {
        'id' : 16,
        'label' :'Athelete',
        'name' :'Athelete'
      
      },
      {
        'id' : 17,
        'label' :'Art',
        'name' :'Art'
      
      },
      {
        'id' : 18,
        'label' :'Automotive',
        'name' :'Automotive'
      
      },
      {
        'id' : 19,
        'label' :'Barista',
        'name' :'Barista'
      
      },


      {
        'id' : 20,
        'label' :'Chef',
        'name' :'Chef'
      
      },

      {
        'id' : 21,
        'label' :'Hijab Lifestyle',
        'name' :'Hijab Lifestyle'
      
      },

      {
        'id' : 22,
        'label' :'Gamers',
        'name' :'Gamers'
      
      },

      {
        'id' : 23,
        'label' :'Tech Enthusiast',
        'name' :'Tech Enthusiast'
      
      },

      {
        'id' : 24,
        'label' :'Family Lifestyle',
        'name' :'Family Lifestyle'
      
      },


      {
        'id' : 25,
        'label' :'White Collar',
        'name' :'White Collar'
      
      },

      {
        'id' : 26,
        'label' :'Entrepreneur',
        'name' :'Entrepreneur'
       
      },

      {
        'id' : 27,
        'label' :'Kids',
        'name' :'Kids'
      
      },


      
      
     
    ]
    this.navigation_menu = [
     
      {
        "id": "0",
        "menu_id": "0",
        "title": "Dashboard",
        "navigation": "/",
        "icon": "task",
        "global_nav": true,
        "has_child": false,
      },
      {
        "id": "1",
        "menu_id": "1",
        "title": "User Management",
        "navigation": "/user/user-account",
        "icon": "faUsers",
        "global_nav": true,
        "has_child": false,
        
      },
     
      {
        "id": "2",
        "menu_id": "2",
        "title": "Creator",
        "navigation": "/creator/",
        "icon": "faUserCircle",
        "has_child": true,
        "child": [
          {
            "id": "8",
            "menu_id": "8",
            "title": "Creator",
            "navigation": "/creator/creator",
            "icon": "title",
            "has_child": false
          },
          {
            "id": "6",
            "menu_id": "6",
            "title": "Campaign",
            "navigation": "/creator/campaign",
            "icon": "title",
            "has_child": false
          },
        
          
          {
            "id": "9",
            "menu_id": "9",
            "title": "Contract",
            "navigation": "/creator/contract",
            "icon": "title",
            "has_child": false
          }
        ]
      },
     
      {
        "id": "3",
        "menu_id": "3",
        "title": "Client",
        "navigation": "/client/",
        "icon": "faUserTie",
        "has_child": true,
        "child": [
         
          {
            "id": "13",
            "menu_id": "13",
            "title": "Client",
            "navigation": "/client/client",
            "icon": "title",
            "has_child": false
          },
          {
            "id": "11",
            "menu_id": "11",
            "title": "Acquisition",
            "navigation": "/client/aquisition",
            "icon": "title",
            "has_child": false
          },
          {
            "id": "14",
            "menu_id": "14",
            "title": "Newsfeed",
            "navigation": "/client/newsfeed",
            "icon": "title",
            "has_child": false
          }
        ]
      },
      {
        "id": "4",
        "menu_id": "4",
        "title": "Channel",
        "navigation": "/channel/",
        "icon": "faNetworkWired",
        "has_child": true,
        "child": [
          {
            "id": "16",
            "menu_id": "16",
            "title": "Vendor",
            "navigation": "/channel/vendor",
            "icon": "title",
            "has_child": false
          },
          {
            "id": "17",
            "menu_id": "17",
            "title": "Inventory",
            "navigation": "/channel/inventory",
            "icon": "title",
            "has_child": false
          },
          {
            "id": "19",
            "menu_id": "19",
            "title": "Employee",
            "navigation": "/channel/employee",
            "icon": "title",
            "has_child": false
          }
        ]
      },
      {
        "id": "5",
        "menu_id": "5",
        "title": "Community",
        "navigation": "/community/",
        "icon": "faBullhorn",
        "has_child": true,
        "child": [
          {
            "id": "21",
            "menu_id": "21",
            "title": "Member",
            "navigation": "/community/member",
            "icon": "title",
            "has_child": false
          },
      
          {
            "id": "22",
            "menu_id": "22",
            "title": "Campaign",
            "navigation": "/community/campaign",
            "icon": "title",
            "has_child": false
          },
         
          {
            "id": "28",
            "menu_id": "28",
            "title": "Points",
            "navigation": "/community/points",
            "icon": "title",
            "has_child": false
          },

          {
            "id": "23",
            "menu_id": "23",
            "title": "Rewards",
            "navigation": "/community/rewards",
            "icon": "title",
            "has_child": false
          },
          
          {
            "id": "24",
            "menu_id": "24",
            "title": "Inbox",
            "navigation": "/community/ticket",
            "icon": "title",
            "has_child": false
          }
        ]
      },
      {
        "id": "255",
        "menu_id": "255",
        "title": "Creative",
        "navigation": "/creative/",
        "icon": "faMagic",
        "has_child": true,
        "child": [
        
          {
            "id": "27",
            "menu_id": "27",
            "title": "Vendor",
            "navigation": "/creative/vendor",
            "icon": "title",
            "has_child": false
          }
        ]
      },

     
    ]
  }

  //Auth

  validateMenuAccess(PAGE_ID){
    var user_session = JSON.parse(localStorage.getItem("user_session"));
    if(user_session){
      return true
    }else{
      return false
    }
    
  }


  async showLoading(isBar?: boolean){
    this.loadingToastSubject.next(true)
  }

  async hideLoading(isBar?: boolean){
    this.loadingToastSubject.next(false)
  }

  

  openDialog(title,message:string, callbackButton?: string, callbackFn?: VoidFunction){
    let data = {
      'title' : title,
     'message' : message,
     'callbackButton' : callbackButton,
     'callbackFn' : callbackFn,
    }
   
    this.dialogEvent.emit(data)
    
  }

  
  openToast(message:string, callbackButton?: string, callbackFn?: VoidFunction){
    let data = {
     'message' : message,
     'callbackButton' : callbackButton,
     'callbackFn' : callbackFn,
    }
   
    this.toastEvent.emit(data)
    
  }













}
