import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ManualOrderService } from './manual-order/manual-order.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardClientService {
  client_session: any;

  constructor(
    private webService : ManualOrderService,

    private router: Router
  ) { }

  async canActivate() {
    this.client_session = this.webService.getClientSession()
    if(!this.client_session){
      this.router.navigate( ['client/login'] );
      return false
    }else{
    return true

    }
    


   
  }
}
