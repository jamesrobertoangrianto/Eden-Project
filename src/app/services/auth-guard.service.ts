import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ManualOrderService } from './manual-order/manual-order.service';



@Injectable({
  providedIn: 'root'
})


export class AuthGuard implements CanActivate {
  session_id: any;
  constructor(
    // private authService: AuthService,
    private webService : ManualOrderService,

    private router: Router
  ) {}
  async canActivate() {

    this.session_id = this.encript(navigator.userAgent)
    let response =  await this.webService.getUserSession( this.webService.getSessionId());
    
    if(response){
      let session_hash = this.encript(response.data.email.toLowerCase()) + this.session_id
      if(parseFloat(response.data.session_id) !== parseFloat(session_hash)){
        this.router.navigate( ['/login'] );
        return false
      }else{
        return true
      }
      
    }else{
       this.router.navigate( ['/login'] );
       return false

    }


   
  }




  encript(data){
    return data.split("").reduce(function(a, b) {
      a = ((a << 5) - a) + b.charCodeAt(0);
      return a & a;
    }, 0);
  }

  
}