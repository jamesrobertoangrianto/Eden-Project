import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';



import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BIDashboardComponent } from './component/BusinessIntelligence/dashboard/dashboard.component';


import { BISidebarComponent } from './component/BusinessIntelligence/dashboard/shareModule/bisidebar/bisidebar.component';
import { LoadingComponentComponent } from './component/BusinessIntelligence/dashboard/shareModule/loading-component/loading-component.component';
import { ModalComponent } from './component/BusinessIntelligence/dashboard/shareModule/modal/modal.component';
import { ProgressComponentComponent } from './component/BusinessIntelligence/dashboard/shareModule/progress-component/progress-component.component';
import { ToastComponentComponent } from './component/BusinessIntelligence/dashboard/shareModule/toast-component/toast-component.component';
import { TopNavigationComponent } from './component/BusinessIntelligence/dashboard/shareModule/top-navigation/top-navigation.component';
import { LoginComponent } from './component/BusinessIntelligence/dashboard/userModule/login/login.component';
import { ProfileComponent } from './component/BusinessIntelligence/dashboard/userModule/profile/profile.component';
import { UserAccountComponent } from './component/BusinessIntelligence/dashboard/userModule/user-account/user-account.component';
import { UserRoleComponent } from './component/BusinessIntelligence/dashboard/userModule/user-role/user-role.component';
import { BodyComponent } from './component/BusinessIntelligence/dashboard/shareModule/body/body.component';
import { BodyFullComponent } from './component/BusinessIntelligence/dashboard/shareModule/body-full/body-full.component';
import { PlaceholderComponent } from './component/BusinessIntelligence/dashboard/shareModule/placeholder/placeholder.component';
import { PaginationComponent } from './component/BusinessIntelligence/dashboard/shareModule/pagination/pagination.component';
import { TableActionComponent } from './component/BusinessIntelligence/dashboard/shareModule/table-action/table-action.component';
import { CampaignComponent } from './component/BusinessIntelligence/dashboard/creatorModule/campaign/campaign.component';
import { ContractComponent } from './component/BusinessIntelligence/dashboard/creatorModule/contract/contract.component';
import { CampaignViewComponent } from './component/BusinessIntelligence/dashboard/creatorModule/campaign/campaign-view/campaign-view.component';
import { ModalFullComponent } from './component/BusinessIntelligence/dashboard/shareModule/modal-full/modal-full.component';
import { ModalSheetsComponent } from './component/BusinessIntelligence/dashboard/shareModule/modal-sheets/modal-sheets.component';
import { StatusLabelComponent } from './component/BusinessIntelligence/dashboard/shareModule/status-label/status-label.component';
import { SalesPipelineComponent } from './component/BusinessIntelligence/dashboard/clientModule/sales-pipeline/sales-pipeline.component';
import { SalesPipelineViewComponent } from './component/BusinessIntelligence/dashboard/clientModule/sales-pipeline/sales-pipeline-view/sales-pipeline-view.component';
import { ClientComponent } from './component/BusinessIntelligence/dashboard/clientModule/client/client.component';
import { NewsfeedBackendComponent } from './component/BusinessIntelligence/dashboard/clientModule/newsfeed-backend/newsfeed-backend.component';
import { ChannelVendorComponent } from './component/BusinessIntelligence/dashboard/channelModule/channel-vendor/channel-vendor.component';
import { InventoryComponent } from './component/BusinessIntelligence/dashboard/channelModule/inventory/inventory.component';
import { EmployeeComponent } from './component/BusinessIntelligence/dashboard/channelModule/employee/employee.component';
import { CommunityCampaignComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-campaign/community-campaign.component';
import { CommunityRewardsComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-rewards/community-rewards.component';
import { CommunityTicketComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-ticket/community-ticket.component';
import { CommunityMemberComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-member/community-member.component';
import { TabMenuComponent } from './component/BusinessIntelligence/dashboard/shareModule/tab-menu/tab-menu.component';
import { AssetsComponent } from './component/BusinessIntelligence/dashboard/channelModule/assets/assets.component';
import { CommunityTicketViewComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-ticket/community-ticket-view/community-ticket-view.component';
import { CreativeAssignmentComponent } from './component/BusinessIntelligence/dashboard/creativeModule/creative-assignment/creative-assignment.component';
import { CreativeVendorComponent } from './component/BusinessIntelligence/dashboard/creativeModule/creative-vendor/creative-vendor.component';
import { CommunityPointsComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-points/community-points.component';
import { CommunityAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/community-account/community-account.component';
import { CommunityAccountRewardsComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/community-account/community-account-rewards/community-account-rewards.component';
import { CreatorAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/creator-account/creator-account.component';
import { ChannelAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/channel-account/channel-account.component';
import { CreativeAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/creative-account/creative-account.component';
import { NewsfeedBackendFilteringComponent } from './component/BusinessIntelligence/dashboard/clientModule/newsfeed-backend/newsfeed-backend-filtering/newsfeed-backend-filtering.component';
import { NewsfeedBackendCategoryComponent } from './component/BusinessIntelligence/dashboard/clientModule/newsfeed-backend/newsfeed-backend-category/newsfeed-backend-category.component';
import { NewsfeedBackendAccountComponent } from './component/BusinessIntelligence/dashboard/clientModule/newsfeed-backend/newsfeed-backend-account/newsfeed-backend-account.component';
import { GeneralContactComponent } from './component/BusinessIntelligence/dashboard/userModule/general-contact/general-contact.component';
import { TaskComponent } from './component/BusinessIntelligence/dashboard/shareModule/task/task.component';
import { FormInputComponent } from './component/BusinessIntelligence/dashboard/shareModule/form-input/form-input.component';
import { FormSelectComponent } from './component/BusinessIntelligence/dashboard/shareModule/form-select/form-select.component';
import { RemoveButtonComponent } from './component/BusinessIntelligence/dashboard/shareModule/remove-button/remove-button.component';
import { CreatorComponent } from './component/BusinessIntelligence/dashboard/creatorModule/creator/creator.component';
import { CreatorViewComponent } from './component/BusinessIntelligence/dashboard/creatorModule/creator/creator-view/creator-view.component';
import { FormSearchComponent } from './component/BusinessIntelligence/dashboard/shareModule/form-search/form-search.component';
import { StatusLabelPerformanceComponent } from './component/BusinessIntelligence/dashboard/shareModule/status-label-performance/status-label-performance.component';
import { EmployeeViewComponent } from './component/BusinessIntelligence/dashboard/channelModule/employee/employee-view/employee-view.component';
import { CommunityMemberViewComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-member/community-member-view/community-member-view.component';
import { CommunityCampaignViewComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-campaign/community-campaign-view/community-campaign-view.component';
import { TopNavigationClientComponent } from './component/BusinessIntelligence/dashboard/shareModule/top-navigation-client/top-navigation-client.component';
import { FormMultipleSelectComponent } from './component/BusinessIntelligence/dashboard/shareModule/form-multiple-select/form-multiple-select.component';
import { NoteComponent } from './component/BusinessIntelligence/dashboard/shareModule/note/note.component';
import { NewsfeedPostViewComponent } from './component/BusinessIntelligence/dashboard/clientModule/newsfeed-backend/newsfeed-post-view/newsfeed-post-view.component';
import { NewsHomepageComponent } from './component/BusinessIntelligence/dashboard/newsfeedModule/news-homepage/news-homepage.component';
import { NewsPostComponent } from './component/BusinessIntelligence/dashboard/newsfeedModule/news-post/news-post.component';
import { NewsCategoryComponent } from './component/BusinessIntelligence/dashboard/newsfeedModule/news-category/news-category.component';
import { TopNavigationNewsfeedComponent } from './component/BusinessIntelligence/dashboard/shareModule/top-navigation-newsfeed/top-navigation-newsfeed.component';
import { NewsFooterComponent } from './component/BusinessIntelligence/dashboard/newsfeedModule/news-footer/news-footer.component';
import { NewsLoginComponent } from './component/BusinessIntelligence/dashboard/newsfeedModule/news-login/news-login.component';
import { NewsfeedBackendContactComponent } from './component/BusinessIntelligence/dashboard/clientModule/newsfeed-backend/newsfeed-backend-contact/newsfeed-backend-contact.component';
import { NewsContactComponent } from './component/BusinessIntelligence/dashboard/newsfeedModule/news-contact/news-contact.component';
import { NewsGuidelinesComponent } from './component/BusinessIntelligence/dashboard/newsfeedModule/news-guidelines/news-guidelines.component';
import { NewsAboutComponent } from './component/BusinessIntelligence/dashboard/newsfeedModule/news-about/news-about.component';
import { NewsTermConditionsComponent } from './component/BusinessIntelligence/dashboard/newsfeedModule/news-term-conditions/news-term-conditions.component';
import { NewsPrivacyComponent } from './component/BusinessIntelligence/dashboard/newsfeedModule/news-privacy/news-privacy.component';
import { NewsfeedBackendPostComponent } from './component/BusinessIntelligence/dashboard/clientModule/newsfeed-backend/newsfeed-backend-post/newsfeed-backend-post.component';
import { ManagementAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/management-account/management-account.component';
import { TableSortComponent } from './component/BusinessIntelligence/dashboard/shareModule/table-sort/table-sort.component';
import { TooltipsComponent } from './component/BusinessIntelligence/dashboard/shareModule/tooltips/tooltips.component';
import { CardComponent } from './component/BusinessIntelligence/dashboard/shareModule/card/card.component';
import { LazyLoadDirective } from './component/BusinessIntelligence/dashboard/shareModule/lazy-load.directive';
import { CreatorDashboardComponent } from './component/BusinessIntelligence/dashboard/creatorModule/creator-dashboard/creator-dashboard.component';
import { ClientLoginComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/client-login/client-login.component';

@NgModule({
  declarations: [
    AppComponent,
    BIDashboardComponent,
    BISidebarComponent,
    TopNavigationComponent,
    ModalComponent,
    ProgressComponentComponent,
    ToastComponentComponent,
    LoadingComponentComponent,
    ProfileComponent,
    LoginComponent,
    UserAccountComponent,
    UserRoleComponent,
    BodyComponent,
    BodyFullComponent,
    PlaceholderComponent,
    PaginationComponent,
    TableActionComponent,
    CampaignComponent,
    ContractComponent,
    CampaignViewComponent,
    ModalFullComponent,
    ModalSheetsComponent,
    StatusLabelComponent,
    SalesPipelineComponent,
    SalesPipelineViewComponent,
    ClientComponent,
    NewsfeedBackendComponent,
    ChannelVendorComponent,
    InventoryComponent,
    EmployeeComponent,
    CommunityCampaignComponent,
    CommunityRewardsComponent,
    CommunityTicketComponent,
    CommunityMemberComponent,
    TabMenuComponent,
    AssetsComponent,
    CommunityTicketViewComponent,
    CreativeAssignmentComponent,
    CreativeVendorComponent,
    CommunityPointsComponent,
    CommunityAccountComponent,
    CommunityAccountRewardsComponent,
    CreatorAccountComponent,
    ChannelAccountComponent,
    CreativeAccountComponent,
    NewsfeedBackendFilteringComponent,
    NewsfeedBackendCategoryComponent,
    NewsfeedBackendAccountComponent,
    NewsfeedBackendComponent,
    GeneralContactComponent,
    TaskComponent,
    FormInputComponent,
    FormSelectComponent,
    RemoveButtonComponent,
    CreatorComponent,
    CreatorViewComponent,
    FormSearchComponent,
    StatusLabelPerformanceComponent,
    EmployeeViewComponent,
    CommunityMemberViewComponent,
    CommunityCampaignViewComponent,
    TopNavigationClientComponent,
    
    FormMultipleSelectComponent,
    NoteComponent,
    NewsfeedPostViewComponent,
    NewsHomepageComponent,
    NewsPostComponent,
    NewsCategoryComponent,
    TopNavigationNewsfeedComponent,
    NewsFooterComponent,
    NewsLoginComponent,
    NewsfeedBackendContactComponent,
    NewsContactComponent,
    NewsGuidelinesComponent,
    NewsAboutComponent,
    NewsTermConditionsComponent,
    NewsPrivacyComponent,
    NewsfeedBackendPostComponent,
    ManagementAccountComponent,
    TableSortComponent,
    TooltipsComponent,
    CardComponent,
    LazyLoadDirective,
    CreatorDashboardComponent,
    ClientLoginComponent
    
  

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,

    
  ],
  providers: [
   
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
